<?php

namespace Webit\Bundle\InvoiceBundle\Command;

use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;

class ListTypesCommand extends ContainerAwareCommand
{
    /** @var EntityRepository */
    private $typesRepository;

    protected function configure()
    {
        $this->setName(
            'webit-invoice:invoices:list-types'
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->typesRepository = $this->getContainer()->get('webit_invoice.type_repository');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $types = $this->typesRepository->findAll();

        /** @var Type $type */
        foreach ($types as $type) {
            $output->writeln(
                sprintf(
                    'ID: <info>%s</info>, Code: <info>%s</info>, Prefix: <info>%s</info>',
                    $type->getId(),
                    $type->getCode(),
                    $type->getPrefix()
                )
            );
        }
    }
}
