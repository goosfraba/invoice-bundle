<?php

namespace Webit\Bundle\InvoiceBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Webit\Bundle\InvoiceBundle\Integration\Push\InvoicePusher;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\Bundle\InvoiceBundle\Invoice\InvoiceRepository;
use Webit\Bundle\InvoiceBundle\Invoice\Month;
use Webit\WFirmaSDK\Entity\Exception\ValidationException;

class PushInvoicesToVendorCommand extends ContainerAwareCommand
{
    /** @var InvoiceRepository */
    private $invoicesRepository;

    /** @var InvoicePusher */
    private $pusher;

    protected function configure()
    {
        $this->setName(
            'webit-invoice:invoices:push-to-vendor'
        );

        $this->addArgument(
            'vendor',
            InputArgument::REQUIRED, 'Vendor invoices should be pushed to.'
        );

        $this->addArgument(
            'month',
            InputArgument::OPTIONAL,
            'Month for which invoices should be pushed (Y-m).'
        );

        $this->addOption(
            'repush',
            '',
            InputOption::VALUE_NONE,
            'Re-push already pushed invoices'
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->pusher = $this->getContainer()->get('webit_invoice.integration.invoice_pusher');
        $this->invoicesRepository = $this->getContainer()->get('webit_invoice.invoice_repository');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $vendor = $this->extractVendor($input->getArgument('vendor'));
        $month = $this->extractMonth($input->getArgument('month'));
        $repush = (bool)$input->getOption('repush');

        $invoices = $this->invoicesRepository->invoicesForVendorAndMonth($vendor, $month);
        foreach ($invoices as $i => $invoice) {
            if ($invoice->vendorId($vendor) && !$repush) {
                $output->writeln(
                    sprintf('Invoice <info>%s</info> is already pushed. Skipping...', $invoice->getNumber())
                );
                continue;
            }

            $output->writeln(sprintf('Pushing <info>%s</info>...', $invoice->getNumber()));
            $this->pusher->push($invoice, $vendor);
        }
    }

    /**
     * @param string $vendor
     * @return Vendor
     */
    private function extractVendor($vendor)
    {
        return Vendor::fromString($vendor);
    }

    /**
     * @param string $month
     * @return Month
     */
    private function extractMonth($month)
    {
        if ($month) {
            return Month::create(
                \DateTime::createFromFormat('Y-m', $month)
            );
        }

        $month = new \DateTime();
        try {
            $month = $month->sub(new \DateInterval('P1M'));
        } catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }

        return Month::create($month);
    }
}
