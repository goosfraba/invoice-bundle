<?php
namespace Webit\Bundle\InvoiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollection;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollection;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Webit\Bundle\InvoiceBundle\Printing\PrintManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;

class AdminController extends Controller
{
    public function indexAction()
    {
        return $this->render('WebitInvoiceBundle:Admin:index.html.twig', array());
    }

    public function staticDataAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $serializer = $this->getSerializer();
        $staticData = array();
        $staticData['WebitInvoice.data.Invoice.Unit'] = "[]"; //$serializer->serialize($em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Unit\Unit')->findAll(),'json'); 


        $taxResponse = $this->container->get('webit_invoice.tax_tax_combo_store')
            ->getModelList(
                array(),
                new FilterCollection(),
                new SorterCollection(),
                1,
                0,
                0
            );

        $ctx = SerializationContext::create();
        $ctx->setGroups($taxResponse->getSerializerGroups());
        $staticData['WebitInvoice.data.Invoice.Tax'] = "[]"; //$serializer->serialize($taxResponse->getData(),'json', $ctx);
        $staticData['WebitInvoice.data.Invoice.Type'] = $serializer->serialize(
            $em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Type')->findAll(),
            'json'
        );

        return $this->render('WebitInvoiceBundle:Admin:staticData.js.twig', array('staticData' => $staticData));
    }

    public function printInvoicesAction($mode = 'download')
    {
        $data = $this->getRequest()->get('invoices', '[]');
        $reprint = $this->getRequest()->get('reprint', false) == 'true';

        $serializer = $this->getSerializer();
        $invoices = new ArrayCollection(
            $serializer->deserialize(
                $data,
                'ArrayCollection<Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice>',
                'json'
            )
        );
        $invoices = $this->getInvoices($invoices);

        if (count($invoices) == 0) {
            throw $this->createNotFoundException('No invoices found');
        }

        $printManager = $this->getPrintManager();
        $print = $printManager->printInvoices($invoices, $reprint);
        $ext = $print->getExtension();

        $response = new Response();
        $response->setContent(file_get_contents($print->getPathname()));
        $response->headers->set('Content-Type', 'application/pdf; charset=binary', true);

        $now = new \DateTime();
        $filename = 'invoices-' . $now->format('YmdHis') . ($ext ? ('.' . $ext) : '');
        $response->headers->set(
            'Content-Disposition',
            ($mode == 'download' ? 'attachment' : 'inline') . '; filename=' . $filename,
            true
        );

        return $response;
    }

    public function printDuplicateAction($id, $mode = 'download')
    {
        $invoice = $this->container->get('doctrine.orm.entity_manager')->getRepository(
            'Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice'
        )->find($id);

        if (! $invoice) {
            throw $this->createNotFoundException('Invoice not found');
        }

        $printManager = $this->getPrintManager();
        $print = $printManager->printInvoice($invoice, false, true);
        $ext = $print->getExtension();

        $response = new Response();
        $response->setContent(file_get_contents($print->getPathname()));
        unlink($print->getPathname());

        $response->headers->set('Content-Type', 'application/pdf; charset=binary', true);

        $now = new \DateTime();
        $filename = 'invoice-' . $invoice->getId() . ($ext ? ('.' . $ext) : '');
        $response->headers->set(
            'Content-Disposition',
            ($mode == 'download' ? 'attachment' : 'inline') . '; filename=' . $filename,
            true
        );

        return $response;
    }

    private function getInvoices(ArrayCollection $invoices)
    {
        $arId = array();
        foreach ($invoices as $invoice) {
            $arId[] = $invoice->getId();
        }

        $arInvoices = $this->container->get('doctrine.orm.entity_manager')->getRepository(
            'Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice'
        )->findBy(array('id' => $arId));
        $invoices = new ArrayCollection($arInvoices);

        return $invoices;
    }

    /**
     *
     * @return Serializer
     */
    private function getSerializer()
    {
        $serializer = $this->container->get('serializer');

        return $serializer;
    }

    /**
     *
     * @return PrintManagerInterface
     */
    private function getPrintManager()
    {
        $printManager = $this->container->get('webit_invoice.print_manager');

        return $printManager;
    }
}
