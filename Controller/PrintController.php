<?php
namespace Webit\Bundle\InvoiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Printing\PrintManagerInterface;

class PrintController extends Controller
{
    /** @var PrintManagerInterface */
    private $printManager;

    public function __construct(PrintManagerInterface $printManager)
    {
        $this->printManager = $printManager;
    }

    public function printAction($id = null, $mode = 'download')
    {
        $invoice = $this->getInvoice($id);
        if (!$invoice) {
            $exception = $this->createNotFoundException('Invoice not found');
            throw $exception;
        }

        $print = $this->printManager->printInvoice($invoice);

        $em = $this->container->get('doctrine.orm.entity_manager');
        if ($em->contains($invoice)) {
            $this->container->get('doctrine.orm.entity_manager')->flush($invoice);
        }

        $ext = $print->getExtension();

        $response = new Response();
        $response->setContent(file_get_contents($print->getPathname()));
        if ($invoice->getType()->getPrintOnFly()) {
            @unlink($print->getPathname());
        }

        $response->headers->set('Content-Type', 'application/pdf; charset=binary', true);
        $filename = str_replace('/', '-', $invoice->getNumber()) . ($ext ? ('.' . $ext) : '');
        $response->headers->set(
            'Content-Disposition',
            ($mode == 'download' ? 'attachment' : 'inline') . '; filename=' . $filename,
            true
        );

        return $response;
    }

    private function getInvoice($invoiceId = null)
    {
        if ($invoiceId) {
            $repo = $this->container->get('doctrine.orm.entity_manager')->getRepository(
                'Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice'
            );
            $invoice = $repo->find($invoiceId);

            if ($invoice) {
                $token = $this->getRequest()->get('token');
                if ($invoice->getToken() == $token) {
                    return $invoice;
                }
            }
        } else {
            $invoice = $this->getRequest()->attributes->get('invoice');
            if ($invoice instanceof Invoice) {
                return $invoice;
            }
        }

        return null;
    }
}
