<?php

namespace Webit\Bundle\InvoiceBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webit\Common\CurrencyBundle\Entity\Currency;
use Webit\Common\CurrencyBundle\Entity\CurrencyEnabled;
use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryProviderInterface;

class LoadCurrency implements FixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function load(ObjectManager $manager)
    {
        /** @var DictionaryProviderInterface $dp */
        $dp = $this->container->get('webit_common_dictionary.dictionary_provider');


        foreach (self::$currencies as $symbol => $currency) {
            @list($enabled, $base) = $currency;

            $dictionary = $dp->getDictionary('currency');

            /** @var Currency $currency */
            $currency = $dictionary->createItem();

            $currency->setLabel('PLN');
            $currency->setCode('PLN');
            $currency->setSymbol('PLN');
            $dictionary->updateItem($currency);
            $dictionary->commitChanges();

            $dictionary = $dp->getDictionary('currency_enabled');

            if ($enabled || $base) {
                /** @var CurrencyEnabled $currencyEnabled */
                $currencyEnabled = $dictionary->createItem();
                $currencyEnabled->setCurrency($currency);
                $currencyEnabled->setBase($base);
                $currencyEnabled->setCode('PLN');

                $dictionary->updateItem($currencyEnabled);
                $dictionary->commitChanges();
            }
        }
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private static $currencies = [
        'PLN' => array(true, true)
    ];
}