<?php
namespace Webit\Bundle\InvoiceBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod;

class LoadPaymentMethod implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        foreach (self::$arMethods as $arMethod) {
            $method = new PaymentMethod();
            $method->setLabel($arMethod['label']);
            $method->setCode($arMethod['code']);
            $method->setDueDateOffset($arMethod['dueDateOffset']);

            $manager->persist($method);
        }

        $manager->flush();
    }

    static protected $arMethods = array(
        array('label' => 'Gotówka', 'code' => 'cash', 'dueDateOffset' => 0),
        array('label' => 'Płatność on-line', 'code' => 'on-line', 'dueDateOffset' => 0),
        array('label' => 'Przelew', 'code' => 'transfer', 'dueDateOffset' => 0),
        array('label' => 'Przelew (3 dni)', 'code' => 'transfer-3days', 'dueDateOffset' => 3),
        array('label' => 'Przelew (7 dni)', 'code' => 'transfer-7days', 'dueDateOffset' => 7),
        array('label' => 'Przelew (14 dni)', 'code' => 'transfer-14days', 'dueDateOffset' => 14),
        array('label' => 'Przelew (30 dni)', 'code' => 'transfer-30days', 'dueDateOffset' => 30)
    );
}
