<?php
namespace Webit\Bundle\InvoiceBundle\DataFixtures\ORM;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class LoadType implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        foreach (self::$_arType as $arType) {
            $type = new Type();
            $type->setLabel($arType['label']);
            $type->setSymbol($arType['symbol']);
            $type->setPrefix($arType['prefix']);
            $type->setCode($arType['code']);
            $type->setNumberLength($arType['number_length']);
            $type->setPrintOnFly(isset($arType['printOnFly']) ? $arType['printOnFly'] : false);

            $manager->persist($type);
        }

        $manager->flush();
    }

    static protected $_arType = array(
        array(
            'label' => 'VAT - Pozostałe',
            'symbol' => 'FA',
            'prefix' => 'FA/',
            'code' => 'vat',
            'number_length' => 0
        ),
        array(
            'label' => 'VAT - Sklep',
            'symbol' => 'FA',
            'prefix' => 'FA/I/',
            'code' => 'vat-ec',
            'number_length' => 0
        ),
        array(
            'label' => 'Proforma',
            'symbol' => 'PF',
            'prefix' => 'PF/',
            'code' => 'proforma',
            'printOnFly' => true,
            'number_length' => 0
        ),
        array(
            'label' => 'Proforma - Sklep',
            'symbol' => 'PF',
            'prefix' => null,
            'code' => 'proforma-ec',
            'printOnFly' => true,
            'number_length' => 0
        ),
        array(
            'label' => 'Korekta',
            'symbol' => 'KO',
            'prefix' => 'KO/',
            'code' => 'correct',
            'number_length' => 0
        )
    );
}