<?php

namespace Webit\Bundle\InvoiceBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webit\Accounting\CommonBundle\Entity\Vat\VatRate;
use Webit\Accounting\CommonBundle\Entity\Vat\VatValue;
use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryInterface;
use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryProviderInterface;

class LoadVatRates implements FixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function load(ObjectManager $manager)
    {
        $dictionary = $this->vatDictionary();

        foreach (self::$rates as $symbol => $percent) {
            $rate = new VatRate();
            $rate->setSymbol($symbol);
            $rate->setLabel('Stawka '.$symbol);

            $rate->getValues()->add(
                $value = new VatValue()
            );

            $value->setPercent($percent);
            $value->setVatRate($rate);

            $dictionary->updateItem($rate);

            $dictionary->commitChanges();
        }
    }

    static private $rates = array(
        'A' => 23,
        'B' => 8,
        'C' => 0
    );

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return DictionaryInterface
     */
    private function vatDictionary()
    {
        /** @var DictionaryProviderInterface $provider */
        $provider = $this->container->get('webit_common_dictionary.dictionary_provider');
        return $provider->getDictionary('vat');
    }
}