<?php

namespace Webit\Bundle\InvoiceBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * This compiler pass adds additional controller mappers
 * to the dynamic router.
 *
 * @author Daniel Leech <dan.t.leech@gmail.com>
 */
class InvoiceMapperPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $tagName = "webit_invoice.invoice_mapper";
        $arServices = $container->findTaggedServiceIds($tagName);
        $manager = $container->getDefinition('webit_invoice.invoice_creator');

        foreach ($arServices as $name => $tag) {
            $from = isset($tag[0]['from']) ? $tag[0]['from'] : null;
            $to = isset($tag[0]['to']) ? $tag[0]['to'] : null;

            if ($from == null || $to == null) {
                throw new \InvalidArgumentException(
                    'Tagged service "' . $name . '" must have "from" and "to" tag properties passed (for tag: "' . $tagName . "'"
                );
            }
            $def = $container->getDefinition($name);
            $manager->addMethodCall('registerMapper', array($from, $to, $def));
        }
    }
}

