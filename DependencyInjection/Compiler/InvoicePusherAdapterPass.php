<?php

namespace Webit\Bundle\InvoiceBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class InvoicePusherAdapterPass implements CompilerPassInterface
{
    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        $services = $container->findTaggedServiceIds($tagName = 'webit_invoice.invoice_pusher_adapter');

        $pusherAdapters = [];
        foreach ($services as $serviceId => $tags) {
            $tag = array_shift($tags);
            if (!isset($tag['vendor'])) {
                throw new \InvalidArgumentException(
                    sprintf('Missing required key "vendor" for tag "%s"', $tagName)
                );
            }

            $pusherAdapters[$tag['vendor']] = new Reference($serviceId);
        }

        $pusher = $container->findDefinition('webit_invoice.integration.invoice_pusher.adapter_selecting');
        $pusher->replaceArgument(0, $pusherAdapters);
    }
}