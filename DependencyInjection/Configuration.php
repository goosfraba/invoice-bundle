<?php

namespace Webit\Bundle\InvoiceBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('webit_invoice');
        $rootNode
            ->children()
                ->scalarNode('gs_path')->defaultValue('gs')->end()
                ->scalarNode('print_path')->defaultValue('%kernel.root_dir%/Resources/invoices')->end()
                ->arrayNode('wfirma')
                    ->canBeEnabled()
                    ->children()
                        ->scalarNode('username')->cannotBeEmpty()->end()
                        ->scalarNode('password')->cannotBeEmpty()->end()
                        ->scalarNode('company_id')->defaultNull()->end()
                        ->booleanNode('check_ssl_certificate')->defaultTrue()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
