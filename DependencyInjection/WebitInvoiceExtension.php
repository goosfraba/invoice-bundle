<?php

namespace Webit\Bundle\InvoiceBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class WebitInvoiceExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        foreach ($config as $key => $value) {
            $container->setParameter($this->getAlias() . '.' . $key, $value);
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('invoice_creator.yml');
        $loader->load('printing.yml');

        $container->setParameter('webit_invoice.ghost_script_binary', $config['gs_path']);
        $loader->load('phpgs.yml');

        $xmlLoader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $xmlLoader->load('integration.xml');

        if ($config['wfirma']['enabled']) {
            $this->loadWfirma($container, $xmlLoader, $config['wfirma']);
        }
    }

    private function loadWfirma(ContainerBuilder $container, Loader\XmlFileLoader $loader, array $config)
    {
        $container->setParameter('webit_invoice.wfirma.username', $config['username']);
        $container->setParameter('webit_invoice.wfirma.password', $config['password']);
        $container->setParameter('webit_invoice.wfirma.company_id', $config['company_id']);
        $loader->load('wfirma.xml');

        if ($config['check_ssl_certificate'] == false) {
            $buzzClientFactory = $container->findDefinition('webit_invoice.wfirma.entity_api_factory.buzz_client_factory');
            $buzzClientFactory->replaceArgument(1, [
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => false
            ]);
        }
    }
}
