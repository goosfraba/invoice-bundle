<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Contractor;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Webit\Bundle\InvoiceBundle\Util\Util;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_contractor_contractor",indexes={@ORM\Index(name="vat_idx", columns={"vat_no_canonical"})})
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="version", type="string", length=1)
 * @ORM\DiscriminatorMap({"v" = "Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorVersion","c"="Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent"})
 * @ORM\HasLifecycleCallbacks()
 */
abstract class AbstractContractorEntity
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer",name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     *
     * @var string
     * @ORM\Column(type="string",name="name")
     * @JMS\Type("string")
     */
    protected $name;

    /**
     *
     * @var string
     * @ORM\Column(type="string",name="vat_no",nullable=true)
     * @JMS\Type("string")
     */
    protected $vatNo;

    /**
     *
     * @var string @ORM\Column(type="string",name="vat_no_canonical",nullable=true)
     * @JMS\Type("string")
     */
    protected $vatNoCanonical;

    /**
     *
     * @var string @ORM\Column(type="string",name="address",nullable=true)
     * @JMS\Type("string")
     */
    protected $address;

    /**
     *
     * @var string @ORM\Column(type="string",name="address_post",nullable=true)
     * @JMS\Type("string")
     */
    protected $addressPost;

    /**
     *
     * @var string @ORM\Column(type="string",name="address_post_code",nullable=true)
     * @JMS\Type("string")
     */
    protected $addressPostCode;

    /**
     *
     * @var string @ORM\Column(type="string",name="address_country",nullable=true)
     * @JMS\Type("string")
     */
    protected $addressCountry;

    /**
     *
     * @var string @ORM\Column(type="text",name="information",nullable=true)
     * @JMS\Type("string")
     */
    protected $information;

    /**
     *
     * @var string @ORM\Column(type="string",name="bank_account",nullable=true)
     * @JMS\Type("string")
     */
    protected $bankAccount;

    /**
     *
     * @var string @ORM\Column(type="string",name="bank_account_canonical",nullable=true)
     * @JMS\Type("string")
     */
    protected $bankAccountCanonical;

    /**
     *
     * @var string @ORM\Column(type="string",name="bank_account_info",nullable=true)
     * @JMS\Type("string")
     */
    protected $bankAccountInfo;

    /**
     *
     * @var string @ORM\Column(type="string",name="email",nullable=true)
     * @JMS\Type("string")
     */
    protected $email;

    /**
     *
     * @var string
     * @ORM\Column(type="string",name="phone_no",nullable=true)
     * @JMS\Type("string")
     */
    protected $phoneNo;

    /**
     *
     * @var bool
     * @ORM\Column(name="supplier",type="boolean",nullable=false)
     * @JMS\Type("boolean")
     */
    protected $supplier = false;

    /**
     *
     * @var \DateTime @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at",type="datetime")
     * @JMS\Type("DateTime<'Y-m-d H:i:s'>")
     */
    protected $updatedAt;

    /**
     *
     * @var ArrayCollection|\Webit\Bundle\InvoiceBundle\Entity\Contractor\User[]
     * @ORM\OneToMany(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Contractor\User",mappedBy="contractor", cascade={"all"})
     * @JMS\Type("ArrayCollection<Webit\Bundle\InvoiceBundle\Entity\Contractor\User>")
     * @JMS\Groups({"users"})
     */
    protected $users;

    /**
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Contractor\User", cascade={"all"})
     * @ORM\JoinColumn(name="default_user_id")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Contractor\User")
     */
    protected $defaultUser;

    /**
     *
     * @var ArrayCollection|\Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice[]
     * @ORM\OneToMany(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice",mappedBy="client",cascade={"all"})
     * @JMS\Type("ArrayCollection<Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice>")
     * @JMS\Groups({"buyInvoices"})
     */
    protected $buyInvoices;

    /**
     *
     * @var ArrayCollection|\Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice[]
     * @ORM\OneToMany(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice",mappedBy="seller",cascade={"all"})
     * @JMS\Type("ArrayCollection<Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice>")
     * @JMS\Groups({"buyInvoices"})
     */
    protected $sellInvoices;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setVatNo($vatNo)
    {
        $this->vatNo = Util::prependCountryCode($vatNo, 'PL');
        $this->vatNoCanonical = Util::vatNoCanonize($this->vatNo, 'PL');
    }

    public function getVatNo()
    {
        return $this->vatNo;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddressPost($addressPost)
    {
        $this->addressPost = $addressPost;
    }

    public function getAddressPost()
    {
        return $this->addressPost;
    }

    public function setAddressPostCode($addressPostCode)
    {
        $this->addressPostCode = $addressPostCode;
    }

    public function getAddressPostCode()
    {
        return $this->addressPostCode;
    }

    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;
    }

    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    public function setInformation($information)
    {
        $this->information = $information;
    }

    public function getInformation()
    {
        return $this->information;
    }

    public function setBankAccount($bankAccount)
    {
        $this->bankAccount = Util::prependCountryCode($bankAccount, 'PL');
        $this->bankAccountCanonical = Util::vatNoCanonize($bankAccount, 'PL');
    }

    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getUsers()
    {
        if ($this->users == null) {
            $this->users = new ArrayCollection();
        }

        return $this->users;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function doStuffOnPrePersist()
    {
        $this->bankAccount = Util::prependCountryCode($this->bankAccount, 'PL');
        $this->bankAccountCanonical = Util::vatNoCanonize($this->bankAccount, 'PL');
    }

    /**
     *
     * @return the string
     */
    public function getVatNoCanonical()
    {
        return $this->vatNoCanonical;
    }

    /**
     *
     * @return bool
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     *
     * @param bool $supplier
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     *
     * @return string
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     *
     * @param string $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    public function isVersion()
    {
        return $this instanceof ContractorVersion;
    }

    /**
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     *
     * @return string
     */
    public function getBankAccountCanonical()
    {
        return $this->bankAccountCanonical;
    }

    /**
     *
     * @return string
     */
    public function getBankAccountInfo()
    {
        return $this->bankAccountInfo;
    }

    /**
     *
     * @param string $bankAccountInfo
     */
    public function setBankAccountInfo($bankAccountInfo)
    {
        $this->bankAccountInfo = $bankAccountInfo;
    }

    /**
     *
     * @return User
     */
    public function getDefaultUser()
    {
        return $this->defaultUser;
    }

    /**
     *
     * @param User $defaultUser
     */
    public function setDefaultUser(User $defaultUser = null)
    {
        $this->defaultUser = $defaultUser;
    }
}
