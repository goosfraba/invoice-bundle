<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Contractor;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 */
class ContractorCurrent extends AbstractContractorEntity
{

    /**
     *
     * @var ArrayCollection|ContractorVersion[]
     * @ORM\OneToMany(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorVersion",mappedBy="contractorCurrent")
     * @JMS\Type("ArrayCollection<Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorVersion>")
     * @JMS\Groups({"versions"})
     */
    protected $contractorVersions;

    /**
     *
     * @return ArrayCollection|ContractorVersion[]
     */
    public function getContractorVersions()
    {
        if ($this->contractorVersions == null) {
            $this->contractorVersions = new ArrayCollection();
        }

        return $this->contractorVersions;
    }
}
