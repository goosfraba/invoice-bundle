<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Contractor;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 */
class ContractorVersion extends AbstractContractorEntity
{

    /**
     *
     * @var AbstractContractorEntity @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent",inversedBy="contractorVersions")
     * @ORM\JoinColumn(name="contractor_current_id")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent")
     * @JMS\Groups({"current"})
     *
     */
    protected $contractorCurrent;

    /**
     *
     * @return the AbstractContractorEntity
     */
    public function getContractorCurrent()
    {
        return $this->contractorCurrent;
    }

    /**
     *
     * @param AbstractContractorEntity $contractorCurrent
     */
    public function setContractorCurrent(AbstractContractorEntity $contractorCurrent)
    {
        $this->contractorCurrent = $contractorCurrent;
    }
}
