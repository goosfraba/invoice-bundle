<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Contractor;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_contractor_user")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer",name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     *
     * @var string
     * @ORM\Column(type="string",name="name")
     * @JMS\Type("string")
     */
    protected $name;

    /**
     *
     * @var string
     * @ORM\Column(type="string",name="lastname")
     * @JMS\Type("string")
     */
    protected $lastname;

    /**
     *
     * @var string
     * @ORM\Column(type="string",name="state",nullable=true)
     * @JMS\Type("string")
     */
    protected $state;

    /**
     *
     * @var string
     * @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity",inversedBy="users")
     * @ORM\JoinColumn(name="contractor_id",referencedColumnName="id")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity")
     * @JMS\Groups({"contractor"})
     */
    protected $contractor;

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setLastame($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function getFullname()
    {
        return trim($this->name . ' ' . $this->lastname);
    }

    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setContractor(AbstractContractorEntity $contractor)
    {
        $this->contractor = $contractor;
    }

    public function getContractor()
    {
        return $this->contractor;
    }
}
