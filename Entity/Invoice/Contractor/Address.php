<?php
/**
 * File Address.php
 * Created at: 2016-09-04 09-41
 *
 * @author Daniel Bojdo <daniel.bojdo@web-it.eu>
 */

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Address
 * @ORM\Embeddable
 */
class Address
{
    /**
     * @var string
     * @JMS\Type("string")
     * @ORM\Column(type = "string", name="address", nullable=true, length=255)
     */
    protected $address;

    /**
     * @var string
     * @JMS\Type("string")
     * @ORM\Column(type = "string", name="post", nullable=true, length=64)
     */
    protected $post;

    /**
     * @var string
     * @JMS\Type("string")
     * @ORM\Column(type = "string", name="post_code", nullable=true, length=16)
     */
    protected $postCode;

    /**
     * @var string
     * @JMS\Type("string")
     * @ORM\Column(type = "string", name="country", nullable=true, length=64)
     */
    protected $country;

    /**
     * @param string $address
     * @param string $post
     * @param string $postCode
     * @param string $country
     */
    public function __construct($address, $post, $postCode, $country)
    {
        $this->address = $address;
        $this->post = $post;
        $this->postCode = $postCode;
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function address()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function post()
    {
        return $this->post;
    }

    /**
     * @return string
     */
    public function postCode()
    {
        return $this->postCode;
    }

    /**
     * @return string
     */
    public function country()
    {
        return $this->country;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return sprintf('%s, %s %s, %s', $this->address(), $this->postCode(), $this->post(), $this->country());
    }
}
