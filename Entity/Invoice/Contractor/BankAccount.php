<?php
/**
 * File BankAccount.php
 * Created at: 2016-09-04 09-39
 *
 * @author Daniel Bojdo <daniel.bojdo@web-it.eu>
 */

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class BankAccount
 * @ORM\Embeddable
 */
class BankAccount
{
    /**
     * @var string
     * @ORM\Column(type="string", name="number", length=64, nullable=true)
     * @JMS\Type("string")
     */
    protected $number;

    /**
     * @var string
     * @ORM\Column(type="string", name="info", nullable=true)
     * @JMS\Type("string")
     */
    protected $info;

    /**
     * BankAccount constructor.
     * @param string $number
     * @param string $info
     */
    public function __construct($number, $info)
    {
        $this->number = $number;
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function number()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function info()
    {
        return $this->info;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return (string) $this->number();
    }
}
