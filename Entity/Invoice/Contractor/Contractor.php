<?php
/**
 * File Contractor.php
 * Created at: 2016-09-04 09-41
 *
 * @author Daniel Bojdo <daniel.bojdo@web-it.eu>
 */

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Contractor
 * @ORM\Embeddable
 */
abstract class Contractor
{
    /**
     *
     * @var string
     * @ORM\Column(type="string", name="name", nullable=true)
     * @JMS\Type("string")
     */
    protected $name;

    /**
     *
     * @var VatNo
     * @ORM\Embedded(class="Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\VatNo", columnPrefix=false)
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\VatNo")
     */
    protected $vatNo;

    /**
     * @var Address
     * @ORM\Embedded(class="Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Address", columnPrefix=false)
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Address")
     */
    protected $address;

    /**
     * Contractor constructor.
     * @param string $name
     * @param VatNo $vatNo
     * @param Address $address
     */
    public function __construct($name, VatNo $vatNo = null, Address $address = null)
    {
        $this->name = $name;
        $this->vatNo = $vatNo;
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return VatNo
     */
    public function vatNo()
    {
        return $this->vatNo;
    }

    /**
     * @return Address
     */
    public function address()
    {
        return $this->address;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return (string) $this->name();
    }

    // BC \Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity methods
    public function getName()
    {
        return $this->name();
    }

    public function getVatNo()
    {
        return $this->vatNo() ? $this->vatNo()->canonical() : null;
    }

    public function getAddress()
    {
        return $this->address() ? $this->address()->address() : null;
    }


    public function getAddressPost()
    {
        return $this->address() ? $this->address()->post() : null;
    }

    public function getAddressPostCode()
    {
        return $this->address() ? $this->address()->postCode() : null;
    }

    public function getAddressCountry()
    {
        return $this->address() ? $this->address()->country() : null;
    }

    /**
     * @return null
     */
    public function getBankAccount()
    {
        return null;
    }

    /**
     *
     * @return string
     */
    public function getBankAccountInfo()
    {
        return null;
    }
}
