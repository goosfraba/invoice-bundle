<?php
/**
 * File Customer.php
 * Created at: 2016-09-04 09-43
 *
 * @author Daniel Bojdo <daniel.bojdo@web-it.eu>
 */

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Customer
 * @ORM\Embeddable
 */
class Customer extends Contractor
{

}
