<?php
/**
 * File Seller.php
 * Created at: 2016-09-04 09-43
 *
 * @author Daniel Bojdo <daniel.bojdo@web-it.eu>
 */

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Seller
 * @ORM\Embeddable
 */
class Seller extends Contractor
{
    /**
     * @var BankAccount
     * @ORM\Embedded(class="Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\BankAccount", columnPrefix="bank_account_")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\BankAccount")
     */
    protected $bankAccount;

    /**
     * Seller constructor.
     * @param string $name
     * @param VatNo|null $vatNo
     * @param Address|null $address
     * @param BankAccount|null $bankAccount
     */
    public function __construct($name, VatNo $vatNo = null, Address $address = null, BankAccount $bankAccount = null)
    {
        parent::__construct($name, $vatNo, $address);

        $this->bankAccount = $bankAccount;
    }

    /**
     * @return BankAccount
     */
    public function bankAccount()
    {
        return $this->bankAccount;
    }

    // BC \Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity methods

    /**
     * @return string
     */
    public function getBankAccount()
    {
        return $this->bankAccount() ? $this->bankAccount()->number() : null;
    }

    /**
     *
     * @return string
     */
    public function getBankAccountInfo()
    {
        return $this->bankAccount() ? $this->bankAccount()->info() : null;
    }
}
