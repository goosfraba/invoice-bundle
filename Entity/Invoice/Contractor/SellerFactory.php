<?php
/**
 * File SellerFactory.php
 * Created at: 2016-09-04 10-30
 *
 * @author Daniel Bojdo <daniel.bojdo@web-it.eu>
 */

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor;

use Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity;

class SellerFactory
{
    /**
     * @param AbstractContractorEntity $contractor
     * @return Seller
     */
    public function create(AbstractContractorEntity $contractor)
    {
        return new Seller(
            $contractor->getName(),
            $this->vatNoOfContractor($contractor),
            new Address(
                $contractor->getAddress(),
                $contractor->getAddressPost(),
                $contractor->getAddressPostCode(),
                $contractor->getAddressCountry()
            ),
            $this->bankAccountOfContractor($contractor)
        );
    }

    /**
     * @param AbstractContractorEntity $contractor
     * @return null|VatNo
     */
    private function vatNoOfContractor(AbstractContractorEntity $contractor)
    {
        if ($vatCanonical = $contractor->getVatNoCanonical()) {
            return VatNo::fromCanonical($contractor->getVatNoCanonical());
        }

        return null;
    }

    /**
     * @param AbstractContractorEntity $contractor
     * @return null|BankAccount
     */
    private function bankAccountOfContractor(AbstractContractorEntity $contractor)
    {
        if ($canonical = $contractor->getBankAccountCanonical()) {
            return new BankAccount(
                $contractor->getBankAccountCanonical(),
                $contractor->getBankAccountInfo()
            );
        }

        return null;
    }
}
