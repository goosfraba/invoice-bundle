<?php
/**
 * File VatNo.php
 * Created at: 2016-09-03 15-24
 *
 * @author Daniel Bojdo <daniel.bojdo@web-it.eu>
 */

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class VatNo
 * @ORM\Embeddable
 */
class VatNo
{
    /**
     * @var string
     * @ORM\Column(type = "string", length=16, name="vat_no", nullable=true)
     */
    private $canonical;

    /**
     * VatNo constructor.
     * @param string $number
     * @param string|null $countryCode
     */
    public function __construct($number, $countryCode = null)
    {
        $this->canonical = sprintf(
            '%s%s',
            (string)$countryCode,
            preg_replace('/[^A-Z0-9]/', '', strtoupper($number))
        );
    }

    /**
     * @param string $canonical
     * @return VatNo
     */
    public static function fromCanonical($canonical)
    {
        $countryCode = substr($canonical, 0, 2);
        if (self::isCountryCode($countryCode)) {
            return new VatNo(substr($canonical, 2), $countryCode);
        }

        return new VatNo($canonical);
    }

    /**
     * @return string
     */
    public function countryCode()
    {
        $countryCode = substr($this->canonical(), 0, 2);

        return self::isCountryCode($countryCode) ? $countryCode : null;
    }

    /**
     * @return string
     */
    public function number()
    {
        if ($this->countryCode()) {
            return substr($this->canonical(), 2);
        }

        return $this->canonical();
    }

    public function canonical()
    {
        return $this->canonical;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->canonical();
    }

    /**
     * @param string $countryCode
     * @return int
     */
    private static function isCountryCode($countryCode)
    {
        return (bool) preg_match('/^[A-Z]{2}$/', $countryCode);
    }
}
