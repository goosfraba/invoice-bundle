<?php

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Payment\CoreBundle\Entity\FinancialTransaction;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use JMS\Payment\CoreBundle\Model\FinancialTransactionInterface;
use JMS\Payment\CoreBundle\Model\PaymentInterface;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Customer;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Seller;
use Webit\Bundle\InvoiceBundle\Integration\InvoiceId;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\Bundle\InvoiceBundle\Integration\WFirma\WFirma;
use Webit\Bundle\InvoiceBundle\Util\PaymentDateHelper;
use Webit\Common\CurrencyBundle\Model\CurrencyInterface;

/**
 * @ORM\Entity(repositoryClass="Webit\Bundle\InvoiceBundle\Invoice\Infrastructure\InvoiceEntityRepository")
 * @ORM\Table(
 *     name="webit_invoice_invoice_invoice",
 *     indexes={
 *         @ORM\Index(name="issue_date_idx", columns={"issue_date"})
 *     }
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Invoice
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer",name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var Type
     * @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\Type", inversedBy="invoices")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Invoice\Type")
     * @JMS\MaxDepth(0)
     */
    protected $type;

    /**
     * @var InvoicePrice
     * @ORM\OneToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoicePrice",cascade="all")
     * @ORM\JoinColumn(name="value_id")
     */
    protected $value;

    /**
     * @var InvoicePrice
     * @ORM\OneToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoicePrice",cascade="all")
     * @ORM\JoinColumn(name="taxed_value_id")
     */
    protected $taxedValue;

    /**
     * @var string
     * @ORM\Column(type="string",name="number")
     * @JMS\Type("string")
     */
    protected $number;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",name="issue_date")
     * @JMS\Type("DateTime")
     */
    protected $issueDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",name="sale_date")
     * @JMS\Type("DateTime")
     */
    protected $saleDate;

    /**
     * @var PaymentMethod
     * @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod")
     * @ORM\JoinColumn(name="payment_method_id")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod")
     */
    protected $paymentMethod;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",name="payment_date")
     * @JMS\Type("DateTime")
     */
    protected $dueDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",name="paid_at",nullable=true)
     * @JMS\Type("DateTime")
     */
    protected $paidAt;

    /**
     * @var Seller
     * @ORM\Embedded(class="Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Seller", columnPrefix="seller_")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Seller")
     * @JMS\Groups({"seller"})
     */
    protected $seller;

    /**
     * @var Customer
     * @ORM\Embedded(class="Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Customer", columnPrefix="customer_")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Customer")
     * @JMS\Groups({"client"})
     */
    protected $client;

    /**
     * @ORM\Column(type="integer",name="seller_id",nullable=true)
     */
    protected $sellerId;

    /**
     * @ORM\Column(type="integer",name="client_id",nullable=true)
     */
    protected $clientId;

    /**
     * @var string
     * @ORM\Column(type="string", name="issuer", nullable=true)
     * @JMS\Type("string")
     */
    protected $issuer;

    /**
     * @var string
     * @ORM\Column(type="string",name="reciver",nullable=true)
     * @JMS\Type("string")
     */
    protected $reciver;

    /**
     * @var Invoice
     * @ORM\OneToMany(
     *     targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\JoinedInvoice",
     *     mappedBy="masterInvoice",
     *     cascade={"all"}
     * )
     * @JMS\Type("ArrayCollection<Webit\Bundle\InvoiceBundle\Entity\Invoice\JoinedInvoice>")
     * @JMS\MaxDepth(1)
     * @JMS\Groups({"joined"})
     */
    protected $joinedInvoices;

    /**
     * @var string @ORM\Column(type="text",name="note",nullable=true)
     * @JMS\Type("string")
     */
    protected $note;

    /**
     * @var string @ORM\Column(type="text",name="internal_note",nullable=true)
     * @JMS\Type("string")
     */
    protected $internalNote;

    /**
     * @var string
     * @ORM\Column(type="string",name="token",nullable=false,length=8)
     * @JMS\Type("string")
     */
    protected $token;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem",
     *     mappedBy="invoice",
     *     cascade={"all"},
     *     orphanRemoval=true
     * )
     * @JMS\Type("ArrayCollection<Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem>")
     * @JMS\Groups({"items"})
     */
    protected $items;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="JMS\Payment\CoreBundle\Entity\PaymentInstruction",cascade={"all"})
     * @ORM\JoinTable(
     *     name="webit_invoice_invoice_payment_instruction_xref",
     *     joinColumns={
     *         @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="payment_instruction_id", referencedColumnName="id",unique=true)
     *     }
     * )
     * @JMS\Type("ArrayCollection<JMS\Payment\CoreBundle\Entity\PaymentInstruction>")
     * @JMS\Groups({"paymentInstructions"})
     */
    protected $paymentInstructions;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime",name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime",name="updated_at")
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update",field={"printFilename"})
     * @ORM\Column(type="datetime",name="printed_at",nullable=true)
     */
    protected $printedAt;

    /**
     * @var string
     * @ORM\Column(type="string",name="print_filename",length=255,nullable=true)
     */
    protected $printFilename;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceVendorId",
     *     mappedBy="invoice",
     *     cascade={"all"},
     *     orphanRemoval=true,
     *     fetch="EAGER"
     * )
     */
    protected $vendorIds;

    public function __construct()
    {
        $this->vendorIds = new ArrayCollection();
        $this->items = new ArrayCollection();
    }

    /**
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param Type $type
     */
    public function setType(Type $type)
    {
        $this->type = $type;
    }

    /**
     *
     * @return \Webit\Bundle\InvoiceBundle\Entity\Invoice\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     *
     * @return InvoicePrice
     */
    public function getValue()
    {
        if ($this->value == null) {
            $this->value = new InvoicePrice();
        }

        return $this->value;
    }

    /**
     *
     * @param InvoicePrice $value
     */
    public function setValue(InvoicePrice $value)
    {
        $this->value = $value;
    }

    /**
     *
     * @return InvoicePrice
     */
    public function getTaxedValue()
    {
        if ($this->taxedValue == null) {
            $this->taxedValue = new InvoicePrice();
        }

        return $this->taxedValue;
    }

    /**
     *
     * @param InvoicePrice $taxedValue
     */
    public function setTaxedValue(InvoicePrice $taxedValue)
    {
        $this->taxedValue = $taxedValue;
    }

    /**
     *
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     *
     * @param \DateTime $issueDate
     */
    public function setIssueDate(\DateTime $issueDate = null)
    {
        $this->issueDate = $issueDate;
    }

    /**
     *
     * @return \DateTime
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     *
     * @param \DateTime $saleDate
     */
    public function setSaleDate(\DateTime $saleDate = null)
    {
        $this->saleDate = $saleDate;
    }

    /**
     *
     * @return \DateTime
     */
    public function getSaleDate()
    {
        return $this->saleDate;
    }

    /**
     *
     * @param PaymentMethod $paymentMethod
     */
    public function setPaymentMethod(PaymentMethod $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     *
     * @return PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     *
     * @param \DateTime $dueDate
     */
    public function setDueDate(\DateTime $dueDate = null)
    {
        $this->dueDate = $dueDate;
    }

    /**
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     *
     * @param \DateTime $paidAt
     */
    public function setPaidAt(\DateTime $paidAt = null)
    {
        $this->paidAt = $paidAt;
    }

    /**
     *
     * @return \DateTime
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     *
     * @param string $issuer
     */
    public function setIssuer($issuer)
    {
        $this->issuer = $issuer;
    }

    /**
     *
     * @return string
     */
    public function getIssuer()
    {
        return $this->issuer;
    }

    /**
     *
     * @param string $receiver
     * @deprecated
     */
    public function setReciver($receiver)
    {
        $this->setReceiver($receiver);
    }

    /**
     * @param string $receiver
     */
    public function setReceiver($receiver)
    {
        $this->reciver = $receiver;
    }

    /**
     *
     * @return string
     * @deprecated
     */
    public function getReciver()
    {
        return $this->getReceiver();
    }

    /**
     *
     * @return string
     */
    public function getReceiver()
    {
        return $this->reciver;
    }

    /**
     *
     * @return Invoice
     */
    public function getJoinedInvoices()
    {
        if ($this->joinedInvoices == null) {
            $this->joinedInvoices = new ArrayCollection();
        }

        return $this->joinedInvoices;
    }

    /**
     *
     * @param string $type
     * @return ArrayCollection
     */
    public function getJoinedInvoicesByType($type)
    {
        $filtered = $this->getJoinedInvoices()->filter(
            function (JoinedInvoice $invoice) use ($type) {
                return $invoice->getType() == $type;
            }
        );

        return $filtered;
    }

    /**
     *
     * @param string $type
     * @return JoinedInvoice
     */
    public function getJoinedInvoiceByType($type)
    {
        $filtered = $this->getJoinedInvoicesByType($type);
        if ($filtered->count() > 0) {
            return $filtered->first();
        }

        return null;
    }

    /**
     *
     * @param Seller $seller
     */
    public function setSeller(Seller $seller)
    {
        $this->seller = $seller;
    }

    /**
     *
     * @return \Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Seller
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     *
     * @param Customer $client
     */
    public function setClient(Customer $client)
    {
        $this->client = $client;
    }

    /**
     *
     * @return Customer
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     *
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getItems()
    {
        if ($this->items == null) {
            $this->items = new ArrayCollection();
        }

        return $this->items;
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection|PaymentInstruction[]
     */
    public function getPaymentInstructions()
    {
        if ($this->paymentInstructions == null) {
            $this->paymentInstructions = new ArrayCollection();
        }

        return $this->paymentInstructions;
    }

    /**
     *
     * @param ArrayCollection $paymentInstructions
     */
    public function setPaymentInstructions(ArrayCollection $paymentInstructions)
    {
        $this->paymentInstructions = $paymentInstructions;
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection|FinancialTransaction[]
     */
    public function getPrePaids()
    {
        $coll = new ArrayCollection();
        foreach ($this->getPaymentInstructions() as $paymentInstruction) {
            foreach ($paymentInstruction->getPayments() as $payment) {
                if ($payment->getState() != PaymentInterface::STATE_DEPOSITED) {
                    continue;
                }

                /** @var FinancialTransaction $transaction */
                foreach ($payment->getTransactions() as $transaction) {
                    $comparisionDate = PaymentDateHelper::getPaymentDate($transaction);
                    if ($comparisionDate <= $this->getIssueDate() && $transaction->getState(
                        ) == FinancialTransactionInterface::STATE_SUCCESS
                    ) {
                        $coll->add($transaction);
                    }
                }
            }
        }

        return $coll;
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection|FinancialTransaction[]
     */
    public function getPostPaids()
    {
        $coll = new ArrayCollection();
        foreach ($this->getPaymentInstructions() as $paymentInstruction) {
            foreach ($paymentInstruction->getPayments() as $payment) {
                if ($payment->getState() != PaymentInterface::STATE_DEPOSITED) {
                    continue;
                }

                foreach ($payment->getTransactions() as $transaction) {
                    if ($transaction->getCreatedAt() > $this->getIssueDate() && $transaction->getState(
                        ) == FinancialTransactionInterface::STATE_SUCCESS
                    ) {
                        $coll->add($transaction);
                    }
                }
            }
        }

        return $coll;
    }

    /**
     * @return CurrencyInterface|null
     */
    private function currency()
    {
        if ($this->taxedValue) {
            return $this->taxedValue->getCurrency();
        }

        $item = $this->getItems()->first();

        return $item ? $item->getPrice()->getCurrency() : null;
    }

    /**
     *
     */
    public function updatePrices()
    {
        $net = 0;
        $gross = 0;
        $currency = $this->currency();

        foreach ($this->items as $item) {
            $net += $item->getValueNet();
            $gross += $item->getValueGross();
        }

        $this->getValue()->setCurrency($currency);
        $this->getValue()->setValue($net);

        $this->getTaxedValue()->setCurrency($currency);
        $this->getTaxedValue()->setValue($gross);
    }

    public function getValueNetByTaxRate($rate)
    {
        $value = 0;
        foreach ($this->getItems() as $item) {
            if ($item->getDiscountPrice()->getVatValue() == $rate) {
                $value += $item->getValueNet();
            }
        }

        return $value;
    }

    public function getValueGrossByTaxRate($rate)
    {
        $value = 0;
        foreach ($this->getItems() as $item) {
            if ($item->getDiscountPrice()->getVatValue() == $rate) {
                $value += $item->getValueGross();
            }
        }

        return $value;
    }

    /**
     *
     * @param string $rate
     * @return float
     */
    public function getTaxValue($rate = null)
    {
        $vatValue = 0;
        foreach ($this->getItems() as $item) {
            if ($rate) {
                if ($rate == $item->getDiscountPrice()->getVatValue()) {
                    $vatValue += $item->getTaxValue();
                }
            } else {
                $vatValue += $item->getTaxValue();
            }
        }

        return $vatValue;
    }

    /**
     *
     * @return array
     */
    public function getTaxRates()
    {
        $rates = array();
        foreach ($this->getItems() as $item) {
            $rate = $item->getDiscountPrice()->getVatValue();
            if (in_array($rate, $rates) == false) {
                $rates[] = $rate;
            }
        }
        rsort($rates);

        return $rates;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        // token do pobrania wydruku
        if ($this->token == null) {
            $token = substr(md5(microtime() . mt_rand(0, 10000)), 3, 8);
            $this->setToken($token);
        }

        // data wystawienia
        if ($this->issueDate == null) {
            $this->issueDate = new \DateTime();
        }

        // data sprzedaży
        if ($this->saleDate == null) {
            $this->saleDate = clone($this->issueDate);
        }

        // termin płatności
        if ($this->dueDate == null) {
            $dueDate = clone($this->getIssueDate());
            $offset = $this->getPaymentMethod() ? $this->getPaymentMethod()->getDueDateOffset() : 0;
            if ((int)$offset > 0) {
                $dueDate->add(new \DateInterval('P' . $offset . 'D'));
            }
            $this->setDueDate($dueDate);
        }
        $this->updatePrices();
    }

    /**
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     *
     * @return \DateTime
     */
    public function getPrintedAt()
    {
        return $this->printedAt;
    }

    /**
     *
     * @return string
     */
    public function getPrintFilename()
    {
        return $this->printFilename;
    }

    /**
     *
     * @param string $printFilename
     */
    public function setPrintFilename($printFilename)
    {
        $this->printFilename = $printFilename;
        if ($this->printFilename === null) {
            $this->printedAt = null;
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return string
     */
    public function getInternalNote()
    {
        return $this->internalNote;
    }

    /**
     * @param string $internalNote
     */
    public function setInternalNote($internalNote)
    {
        $this->internalNote = $internalNote;
    }

    /**
     *
     * @return boolean
     */
    public function isInternationalInvoice()
    {
        $sellerVatNo = $this->getSeller() ? $this->getSeller()->vatNo() : null;
        $customerVatNo = $this->getClient() ? $this->getClient()->vatNo() : null;

        if (!$sellerVatNo || !$customerVatNo) {
            return false;
        }

        $sellerCountryCode = $sellerVatNo->countryCode();
        $customerVatNo = $customerVatNo->countryCode();

        if (!$sellerCountryCode || !$customerVatNo) {
            return false;
        }

        return $sellerCountryCode != $customerVatNo;
    }

    /**
     * @param Vendor $vendor
     * @return InvoiceId|null
     */
    public function vendorId(Vendor $vendor)
    {
        $invoiceVendorId = $this->findInvoiceVendorIdOfVendor($vendor);

        return $invoiceVendorId ? $invoiceVendorId->invoiceId() : null;
    }

    /**
     * @param Vendor $vendor
     * @return null|InvoiceVendorId
     */
    private function findInvoiceVendorIdOfVendor(Vendor $vendor)
    {
        $vendorIds = $this->vendorIds ?: new ArrayCollection();
        /** @var InvoiceVendorId $vendorId */
        foreach ($vendorIds as $vendorId) {
            if ($vendorId->invoiceId()->vendor() == $vendor) {
                return $vendorId;
            }
        }

        return null;
    }

    /**
     * @param InvoiceId $vendorInvoiceId
     */
    public function setVendorId(InvoiceId $vendorInvoiceId)
    {
        $vendorIdEntity = $this->findInvoiceVendorIdOfVendor($vendorInvoiceId->vendor());
        if ($vendorIdEntity) {
            $vendorIdEntity->updateFromVendorInvoiceId($vendorInvoiceId);
            return;
        }

        $vendorIdEntity = new InvoiceVendorId($this, $vendorInvoiceId);
        $this->vendorIds->add($vendorIdEntity);
    }

    /**
     * @param Vendor $vendor
     */
    public function unsetVendorId(Vendor $vendor)
    {
        $invoiceVendorId = $this->findInvoiceVendorIdOfVendor($vendor);
        if ($invoiceVendorId) {
            $this->vendorIds->removeElement($invoiceVendorId);
        }
    }

    /**
     * @return float
     */
    public function prePaidAmount()
    {
        $paid = 0;
        foreach ($this->getPrePaids() as $transaction) {
            $paid += $transaction->getProcessedAmount();
        }

        return $paid;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("wfirma_id")
     */
    public function wFirmaId()
    {
        $wFirmaId =  $this->vendorId(WFirma::vendor());
        return $wFirmaId ? $wFirmaId->id() : null;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastPaymentDate()
    {
        $lastPaymentDate = null;
        foreach ($this->getPaymentInstructions() as $paymentInstruction) {
            foreach ($paymentInstruction->getPayments() as $payment) {
                if ($payment->getState() != PaymentInterface::STATE_DEPOSITED) {
                    continue;
                }

                foreach ($payment->getTransactions() as $transaction) {
                    if ($transaction->getState() != FinancialTransaction::STATE_SUCCESS) {
                        continue;
                    }
                    $transactionDate = $transaction->getUpdatedAt() ?: $transaction->getCreatedAt();
                    $lastPaymentDate = !$lastPaymentDate || $transactionDate > $lastPaymentDate ? $transactionDate : $lastPaymentDate;
                }
            }
        }

        return $lastPaymentDate;
    }
}
