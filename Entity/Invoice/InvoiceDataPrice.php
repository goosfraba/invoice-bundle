<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Invoice;

use Webit\Accounting\CommonBundle\Entity\Price\TaxedPrice;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_data_price")
 * @ORM\HasLifecycleCallbacks()
 */
class InvoiceDataPrice extends TaxedPrice
{
    /**
     * @var float
     * @ORM\Column(name="vat_value", type="decimal", precision=18, scale=10, nullable=false)
     */
    protected $vatValue;

    public function getVatValue()
    {
        return $this->vatValue;
    }

    public function setVatRate(VatRateInterface $vatRate)
    {
        parent::setVatRate($vatRate);
        $vatValue = $vatRate->getValue(new \DateTime());
        $vatValue = $vatValue ? $vatValue->getValue() : null;
        if ($vatValue) {
            $this->setVatValue($vatValue);
        }
    }

    public function setVatValue($vatValue)
    {
        $this->vatValue = $vatValue;
    }

    /**
     * @param \DateTime $date
     * @return float
     */
    public function getTaxedValue(\DateTime $date = null)
    {
        if ($this->net) {
            return $this->value * (1 + $this->vatValue);
        }

        return $this->value / (1 + $this->vatValue);
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if ($vr = $this->getVatRate()) {
            $v = $vr->getValue(new \DateTime());
            $this->vatValue = $v ? $v->getValue() : null;
        }
    }

    public function __clone()
    {
        $this->id = null;
    }
}
