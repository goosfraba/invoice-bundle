<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Invoice;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Webit\Common\UnitBundle\Model\UnitInterface;
use Webit\Common\DictionaryBundle\Annotation as Dict;
use Webit\Common\DictionaryBundle\Model\DictionaryItem\DictionaryItemAwareInterface;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_invoice_invoice_item")
 * @ORM\HasLifecycleCallbacks()
 */
class InvoiceItem implements DictionaryItemAwareInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     *
     * @var string @ORM\Column(name="name",type="string")
     * @JMS\Type("string")
     */
    protected $name;

    /**
     *
     * @var string @ORM\Column(name="description",type="string",nullable=true)
     * @JMS\Type("string")
     */
    protected $description;

    /**
     *
     * @var string @Dict\ItemCode(dictionaryName="unit",itemProperty="unit")
     * @ORM\Column(name="unit",type="string",length=64,nullable=true)
     */
    protected $unitId;

    /**
     *
     * @var UnitInterface
     */
    protected $unit;

    /**
     *
     * @var InvoiceDataPrice @ORM\OneToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceDataPrice",cascade="all")
     * @ORM\JoinColumn(name="price_id")
     */
    protected $price;

    /**
     *
     * @var InvoiceDataPrice @ORM\OneToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceDataPrice",cascade="all")
     * @ORM\JoinColumn(name="discount_price_id")
     *
     */
    protected $discountPrice;

    /**
     *
     * @var float @ORM\Column(type="decimal",name="quantity",scale=8)
     * @JMS\Type("double")
     */
    protected $quantity;

    /**
     *
     * @var Invoice @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice",inversedBy="items")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice")
     */
    protected $invoice;

    /**
     *
     * @var string @ORM\Column(type="string",name="source_item_id",length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $sourceItemId;

    /**
     *
     * @var string @ORM\Column(type="string",name="source_item_provider",length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $sourceItemProvider;

    /**
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     *
     * @param string $description
     */
    public function setDescription($description = null)
    {
        $this->description = $description;
    }

    public function setUnit(UnitInterface $unit = null)
    {
        $this->unit = $unit;
        $this->unitId = $unit->getCode();
    }

    public function getUnit()
    {
        return $this->unit;
    }

    /**
     *
     * @param InvoiceDataPrice $price
     */
    public function setPrice(InvoiceDataPrice $price)
    {
        $this->price = $price;
    }

    /**
     *
     * @return \Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceDataPrice
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     *
     * @param InvoiceDataPrice $discountPrice
     */
    public function setDiscountPrice(InvoiceDataPrice $discountPrice)
    {
        $this->discountPrice = $discountPrice;
    }

    /**
     *
     * @return \Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceDataPrice
     */
    public function getDiscountPrice()
    {
        return $this->discountPrice ?: $this->getPrice();
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setInvoice(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     *
     * @return string
     */
    public function getSourceItemId()
    {
        return $this->sourceItemId;
    }

    /**
     *
     * @param string $sourceItemId
     */
    public function setSourceItemId($sourceItemId = null)
    {
        $this->sourceItemId = $sourceItemId;
    }

    /**
     *
     * @return string
     */
    public function getSourceItemProvider()
    {
        return $this->sourceItemProvider;
    }

    /**
     *
     * @param string $sourceItemProvider
     */
    public function setSourceItemProvider($sourceItemProvider = null)
    {
        $this->sourceItemProvider = $sourceItemProvider;
    }

    /**
     *
     * @return number
     */
    public function getValueNet()
    {
        return $this->getDiscountPrice()->getValue() * $this->getQuantity();
    }

    /**
     *
     * @return number
     */
    public function getValueGross()
    {
        return $this->getValueNet() * ($this->getDiscountPrice()->getVatValue() + 1);
    }

    /**
     *
     * @return number
     */
    public function getTaxValue()
    {
        return $this->getValueNet() * $this->getDiscountPrice()->getVatValue();
    }
}
