<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Invoice;

use Webit\Accounting\CommonBundle\Entity\Price\Price;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_price")
 */
class InvoicePrice extends Price
{
    public function __clone()
    {
        $this->id = null;
    }
}
