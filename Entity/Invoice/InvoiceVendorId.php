<?php

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice;

use Doctrine\ORM\Mapping as ORM;
use Webit\Bundle\InvoiceBundle\Integration\InvoiceId;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;

/**
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_vendor_ids")
 */
class InvoiceVendorId
{
    /**
     * @var Invoice
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice", inversedBy="vendorIds")
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     */
    private $invoice;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string",name="vendor",length=32,nullable=false)
     */
    private $vendor;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string",name="id",length=64,nullable=false)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="datetime",name="pushed_at",nullable=false)
     */
    private $pushedAt;

    /**
     * @param Invoice $invoice
     * @param InvoiceId $invoiceId
     */
    public function __construct(Invoice $invoice, InvoiceId $invoiceId)
    {
        $this->invoice = $invoice;
        $this->vendor = (string)$invoiceId->vendor();
        $this->id = $invoiceId->id();
        $this->pushedAt = new \DateTime();
    }

    /**
     * @return Invoice
     */
    public function invoice()
    {
        return $this->invoice;
    }

    /**
     * @param InvoiceId $invoiceId
     */
    public function updateFromVendorInvoiceId(InvoiceId $invoiceId)
    {
        $this->vendor = (string)$invoiceId->vendor();
        $this->id = $invoiceId->id();
        $this->pushedAt = $invoiceId->pushedAt();
    }

    /**
     * @return InvoiceId
     */
    public function invoiceId()
    {
        return new InvoiceId(Vendor::fromString($this->vendor), $this->id);
    }

    /**
     * @return \DateTime
     */
    public function pushedAt()
    {
        return $this->pushedAt;
    }
}