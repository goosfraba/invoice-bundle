<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Invoice;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 * @author dbojdo
 * @ORM\Entity()
 * @ORM\Table(name="webit_invoice_joined_invoice")
 */
class JoinedInvoice
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer",name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    private $id;

    /**
     *
     * @var Invoice @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice",inversedBy="joinedInvoices",cascade={"all"})
     * @ORM\JoinColumn(name="source_invoice_id")
     */
    private $masterInvoice;

    /**
     *
     * @var Invoice @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice",cascade={"all"})
     * @ORM\JoinColumn(name="dest_invoice_id")
     */
    private $destInvoice;

    /**
     *
     * @var string @ORM\Column(name="type",type="string",length=16,nullable=false)
     */
    private $type;

    /**
     *
     * @return the unknown_type
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return Invoice
     */
    public function getMasterInvoice()
    {
        return $this->masterInvoice;
    }

    /**
     *
     * @param Invoice $masterInvoice
     */
    public function setMasterInvoice(Invoice $masterInvoice)
    {
        $this->masterInvoice = $masterInvoice;
    }

    /**
     *
     * @return Invoice
     */
    public function getDestInvoice()
    {
        return $this->destInvoice;
    }

    /**
     *
     * @param Invoice $destInvoice
     */
    public function setDestInvoice(Invoice $destInvoice)
    {
        $this->destInvoice = $destInvoice;
    }

    /**
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}