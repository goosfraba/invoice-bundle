<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Invoice;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_payment_method")
 */
class PaymentMethod
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer",name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    private $id;

    /**
     *
     * @var string
     * @ORM\Column(type="string",name="code",length=255)
     */
    private $code;

    /**
     *
     * @var string
     * @ORM\Column(type="string",name="string",length=255)
     */
    private $label;

    /**
     *
     * @var int
     * @ORM\Column(type="integer",name="dueDateOffset")
     */
    private $dueDateOffset = 0;

    /**
     *
     * @return int unknown_type
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     *
     * @param string $name
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     *
     * @return int
     */
    public function getDueDateOffset()
    {
        return $this->dueDateOffset;
    }

    /**
     *
     * @param int $dueDateOffset
     */
    public function setDueDateOffset($dueDateOffset)
    {
        $this->dueDateOffset = $dueDateOffset;
    }
}