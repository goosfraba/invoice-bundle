<?php

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Webit\Bundle\InvoiceBundle\Integration\TypeId;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;

/**
 * @ORM\Entity(repositoryClass="Webit\Bundle\InvoiceBundle\Invoice\Infrastructure\TypeEntityRepository")
 * @ORM\Table(name="webit_invoice_invoice_type")
 */
class Type
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer",name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string",name="label")
     * @JMS\Type("string")
     */
    protected $label;

    /**
     * @var string
     * @ORM\Column(type="string",name="code",nullable=false)
     * @JMS\Type("string")
     */
    protected $code;

    /**
     * @var string
     * @ORM\Column(type="string",name="symbol")
     * @JMS\Type("string")
     */
    protected $symbol;

    /**
     * @var string
     * @ORM\Column(type="string",name="prefix",nullable=true)
     * @JMS\Type("string")
     */
    protected $prefix;

    /**
     * @ORM\Column(type="integer",name="number_length")
     * @JMS\Type("integer")
     */
    protected $numberLength = 5;

    /**
     * @var bool
     * @ORM\Column(type="boolean",name="print_on_fly",nullable=false)
     * @JMS\Type("boolean")
     */
    protected $printOnFly = false;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice",mappedBy="type")
     * @JMS\Type("ArrayCollection<Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice>")
     * @JMS\Exclude
     */
    protected $invoices;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\TypeVendorId",
     *     mappedBy="type",
     *     cascade={"all"},
     *     orphanRemoval=true,
     *     fetch="EAGER"
     * )
     */
    protected $vendorIds;

    public function __construct()
    {
        $this->vendorIds = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param int $numberLength
     */
    public function setNumberLength($numberLength)
    {
        $this->numberLength = $numberLength;
    }

    /**
     * @return int
     */
    public function getNumberLength()
    {
        return $this->numberLength;
    }

    /**
     * @return ArrayCollection
     */
    public function getInvoices()
    {
        return $this->invoices ?: new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return bool
     */
    public function getPrintOnFly()
    {
        return $this->printOnFly;
    }

    /**
     * @param bool $printOnFly
     */
    public function setPrintOnFly($printOnFly)
    {
        $this->printOnFly = $printOnFly;
    }

    /**
     * @return string
     */
    public function getMasterType()
    {
        $arType = explode('-', $this->code);

        return $arType[0];
    }

    /**
     * @param Vendor $vendor
     * @return TypeId|null
     */
    public function vendorId(Vendor $vendor)
    {
        $vendorTypeId = $this->findVendorTypeIdOfVendor($vendor);

        return $vendorTypeId ? $vendorTypeId->typeId() : null;
    }

    /**
     * @param Vendor $vendor
     * @return null|TypeVendorId
     */
    private function findVendorTypeIdOfVendor(Vendor $vendor)
    {
        $vendorIds = $this->vendorIds ?: new ArrayCollection();
        /** @var TypeVendorId $vendorId */
        foreach ($vendorIds as $vendorId) {
            if ($vendorId->typeId()->vendor() == $vendor) {
                return $vendorId;
            }
        }

        return null;
    }

    /**
     * @param TypeId $vendorTypeId
     */
    public function setVendorId(TypeId $vendorTypeId)
    {
        $vendorIdEntity = $this->findVendorTypeIdOfVendor($vendorTypeId->vendor());
        if ($vendorIdEntity) {
            $vendorIdEntity->updateFromVendorTypeId($vendorTypeId);
            return;
        }

        $vendorIdEntity = new TypeVendorId($this, $vendorTypeId);
        $this->vendorIds->add($vendorIdEntity);
    }

    /**
     * @param Vendor $vendor
     */
    public function unsetVendorId(Vendor $vendor)
    {
        $vendorTypeId = $this->findVendorTypeIdOfVendor($vendor);
        if ($vendorTypeId) {
            $this->vendorIds->removeElement($vendorTypeId);
        }
    }
}
