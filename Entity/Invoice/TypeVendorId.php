<?php

namespace Webit\Bundle\InvoiceBundle\Entity\Invoice;

use Doctrine\ORM\Mapping as ORM;
use Webit\Bundle\InvoiceBundle\Integration\TypeId;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;

/**
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_type_vendor_ids")
 */
class TypeVendorId
{
    /**
     * @var Type
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Invoice\Type", inversedBy="vendorIds")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string",name="vendor",length=32,nullable=false)
     */
    private $vendor;

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string",name="id",length=64,nullable=false)
     */
    private $id;

    /**
     * @param Type $type
     * @param TypeId $typeId
     */
    public function __construct(Type $type, TypeId $typeId)
    {
        $this->type = $type;
        $this->vendor = (string)$typeId->vendor();
        $this->id = $typeId->id();
    }

    /**
     * @return TypeId
     */
    public function typeId()
    {
        return new TypeId(
            Vendor::fromString($this->vendor),
            $this->id
        );
    }

    /**
     * @param TypeId $typeId
     */
    public function updateFromVendorTypeId(TypeId $typeId)
    {
        $this->vendor = (string)$typeId->vendor();
        $this->id = $typeId->id();
    }
}
