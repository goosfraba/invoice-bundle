<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Webit\Common\UnitBundle\Model\UnitAwareInterface;
use Webit\Common\UnitBundle\Model\UnitInterface;
use Webit\Common\DictionaryBundle\Annotation as Dict;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_product")
 */
class Product implements UnitAwareInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     *
     * @var string
     * @ORM\Column(type="string")
     * @JMS\Type("string")
     */
    protected $name;

    /**
     * @Dict\ItemCode(dictionaryName="unit",itemProperty="unit")
     */
    protected $unitId;

    /**
     * @var UnitInterface
     */
    protected $unit;

    /**
     *
     * @var ProductPrice
     * @ORM\OneToOne(targetEntity="Webit\Bundle\InvoiceBundle\Entity\Product\ProductPrice", cascade="all")
     * @ORM\JoinColumn(name="price_id")
     * @JMS\Type("Webit\Bundle\InvoiceBundle\Entity\Product\ProductPrice")
     */
    protected $price;

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setUnit(UnitInterface $unit = null)
    {
        $this->unit = $unit;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function getUnitId()
    {
        return $this->unitId;
    }

    public function setPrice(ProductPrice $price)
    {
        $this->price = $price;
    }

    /**
     *
     * @return ProductPrice
     */
    public function getPrice()
    {
        return $this->price;
    }
}
