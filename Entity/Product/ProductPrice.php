<?php
namespace Webit\Bundle\InvoiceBundle\Entity\Product;

use Webit\Accounting\CommonBundle\Entity\Price\TaxedPrice;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 * @author dbojdo
 * @ORM\Entity
 * @ORM\Table(name="webit_invoice_product_price")
 * @ORM\HasLifecycleCallbacks()
 */
class ProductPrice extends TaxedPrice
{
    /**
     * @var float
     * @ORM\Column(name="vat_value", type="decimal", precision=18, scale=10, nullable=false)
     */
    protected $vatValue;

    public function getVatValue()
    {
        return $this->vatValue;
    }

    public function setVatValue($vatValue)
    {
        $this->vatValue = $vatValue;
    }

    /**
     * @param \DateTime $date
     * @return float
     */
    public function getTaxedValue(\DateTime $date = null)
    {
        if ($this->net) {
            return $this->value * (1 + $this->vatValue);
        }

        return $this->value / (1 + $this->vatValue);
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if ($vr = $this->getVatRate()) {
            $v = $vr->getValue(new \DateTime());
            $this->vatValue = $v ? $v->getValue() : null;
        }
    }
}
