<?php
namespace Webit\Bundle\InvoiceBundle\ExtJsStore\Contractor;

use Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity;
use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;
use Doctrine\ORM\EntityManager;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\ORM\QueryBuilderDecorator;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreAbstract;

class ContractorStore extends ExtJsStoreAbstract
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(array $options = array(), EntityManager $em)
    {
        parent::__construct($options);
        $this->em = $em;
    }

    public function getModelList(
        $queryParams,
        FilterCollectionInterface $filters,
        SorterCollectionInterface $sorters,
        $page = null,
        $limit = null,
        $offset = null
    ) {
        $qb = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Contractor\Contractor')->createQueryBuilder(
            'c'
        );

        $qbDecorator = new QueryBuilderDecorator($qb);
        $qbDecorator->applySorters($sorters);
        $qbDecorator->applyFilters($filters);
        $qbDecorator->applyLimit($limit);
        $qbDecorator->applyOffset($offset);

        $collContractor = $qb->getQuery()->execute();

        $response = new ExtJsJson();
        $response->setData($collContractor);

        return $response;
    }

    public function loadModel($id, $queryParams)
    {
        $eContractor = $this->findContractor($id);

        $response = new ExtJsJson();
        $response->setData($eContractor);

        return $response;
    }

    public function createModels(\Traversable $modelListData)
    {
        $response = new ExtJsJson();

        foreach ($modelListData as &$contractorData) {
            $this->em->persist($contractorData);
        }
        $this->em->flush();

        $response->setData($modelListData);

        return $response;
    }

    public function updateModels(\Traversable $modelListData)
    {
        $response = new ExtJsJson();
        $arData = array();

        foreach ($modelListData as &$contractorData) {
            $this->em->merge($contractorData);
        }
        $this->em->flush();

        $response->setData($modelListData);

        return $response;
    }

    public function deleteModel($id)
    {
        $coll = $id;
        $arData = array();

        foreach ($coll as $contractorData) {
            $eContractor = $this->findContractor($contractorData->getId());
            $this->em->remove($eContractor);
            $arData[] = $contractorData->getId();
        }
        $this->em->flush();

        $response = new ExtJsJson();;
        $response->setData($arData);

        return $response;
    }

    /**
     *
     * @param int $id
     * @return AbstractContractorEntity
     */
    private function findContractor($id)
    {
        $eContractor = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Contractor\Contractor')->find($id);

        return $eContractor;
    }

    public function getDataClass()
    {
        return 'Webit\Bundle\InvoiceBundle\Entity\Contractor\Contractor';
    }
}
