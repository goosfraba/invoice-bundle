<?php
namespace Webit\Bundle\InvoiceBundle\ExtJsStore\Invoice;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

use Doctrine\ORM\Query;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\DependencyInjection\ContainerAware;

use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;

use Doctrine\ORM\EntityManager;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\ORM\QueryBuilderDecorator;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreAbstract;

class InvoiceItemStore extends ExtJsStoreAbstract
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(array $options = array(), EntityManager $em)
    {
        parent::__construct($options);

        $this->em = $em;
    }

    public function getModelList(
        $queryParams,
        FilterCollectionInterface $filters,
        SorterCollectionInterface $sorters,
        $page = null,
        $limit = null,
        $offset = null
    ) {
        $qb = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem')->createQueryBuilder(
            'ii'
        );

        $filter = $filters->getFilter('invoice_id');
        if (empty($filter)) {
            throw new \InvalidArgumentException('Required filter (invoice_id) not found.');
        }

        $qbDecorator = new QueryBuilderDecorator($qb);
        $qbDecorator->applySorters($sorters);
        $qbDecorator->applyFilters($filters);
        $qbDecorator->applyLimit($limit);
        $qbDecorator->applyOffset($offset);

        $collInvoice = $qb->getQuery()->execute();

        $response = new ExtJsJson();
        $response->setData($collInvoice);

        return $response;
    }

    public function loadModel($id, $queryParams)
    {
        $invoiceItem = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem')->find($id);

        $response = new ExtJsJson();
        $response->setData($invoiceItem);

        return $response;
    }

    public function createModels(\Traversable $modelListData)
    {
        $response = new ExtJsJson();

        foreach ($modelListData as &$invoiceItemData) {
            $invoice = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice')->find(
                $invoiceItemData->getInvoiceId()
            );
            if (!$invoice) {
                throw new \InvalidArgumentException('Required invoice not found.');
            }
            $invoiceItemData->setInvoice($invoice);

            $taxRate = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Tax\TaxRate')->find(
                $invoiceItemData->getTaxRateId()
            );
            if (!$taxRate) {
                throw new \InvalidArgumentException('Required Tax Rate not found.');
            }
            $invoiceItemData->setTaxRate($taxRate);

            $unit = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Unit\Unit')->find(
                $invoiceItemData->getUnitId()
            );
            if (!$unit) {
                throw new \InvalidArgumentException('Required Unit not found.');
            }
            $invoiceItemData->setUnit($unit);

            $this->em->persist($invoiceItemData);

        }
        $this->em->flush();

        $invoice->updatePrices();

        $this->em->flush($invoice);

        $response->setData($modelListData);
        $response->setMiscData(array('invoice' => $invoice));

        return $response;
    }

    public function updateModels(\Traversable $modelListData)
    {
        $response = new ExtJsJson();

        foreach ($modelListData as &$invoiceItemData) {
            $invoice = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice')->find(
                $invoiceItemData->getInvoiceId()
            );
            if (!$invoice) {
                throw new \InvalidArgumentException('Required invoice not found.');
            }
            $invoiceItemData->setInvoice($invoice);

            $taxRate = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Tax\TaxRate')->find(
                $invoiceItemData->getTaxRateId()
            );
            if (!$taxRate) {
                throw new \InvalidArgumentException('Required Tax Rate not found.');
            }
            $invoiceItemData->setTaxRate($taxRate);

            $unit = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Unit\Unit')->find(
                $invoiceItemData->getUnitId()
            );
            if (!$unit) {
                throw new \InvalidArgumentException('Required Unit not found.');
            }
            $invoiceItemData->setUnit($unit);

            $this->em->merge($invoiceItemData);
        }
        $this->em->flush();

        $invoiceItemData->getInvoice()->updatePrices();
        $this->em->flush($invoiceItemData->getInvoice());

        $response->setData($modelListData);
        $response->setMiscData(array('invoice' => $invoiceItemData->getInvoice()));

        return $response;
    }

    public function deleteModel($id)
    {
        $coll = $id;

        foreach ($coll as &$invoiceItemData) {
            $invoiceItem = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem')->find(
                $invoiceItemData->getId()
            );
            if ($invoiceItem) {
                $this->em->remove($invoiceItem);
            }
        }
        $this->em->flush();

        $invoiceItem->getInvoice()->updatePrices();
        $this->em->flush($invoiceItem->getInvoice());

        $response = new ExtJsJson();;
        $response->setData($coll);
        $response->setMiscData(array('invoice' => $invoiceItem->getInvoice()));

        return $response;
    }

    public function getDataClass()
    {
        return 'Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem';
    }
}
