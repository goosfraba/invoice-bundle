<?php
namespace Webit\Bundle\InvoiceBundle\ExtJsStore\Invoice;

use Webit\Bundle\ExtJsBundle\Store\Filter\Filter;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollection;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterParams;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

use Doctrine\ORM\Query;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\DependencyInjection\ContainerAware;

use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;

use Doctrine\ORM\EntityManager;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\ORM\QueryBuilderDecorator;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreAbstract;

class InvoiceStore extends ExtJsStoreAbstract
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(array $options = array(), EntityManager $em)
    {
        parent::__construct($options);

        $this->em = $em;
    }

    public function getModelList(
        $queryParams,
        FilterCollectionInterface $filters,
        SorterCollectionInterface $sorters,
        $page = null,
        $limit = null,
        $offset = null
    ) {
        $qb = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice')->createQueryBuilder('i');
        $qb->leftJoin('i.value', 'v');
        $qb->leftJoin('i.taxedValue', 'tv');
        $qb->leftJoin('i.type', 't');

        $filters = $this->fixFilters($filters);

        $qbDecorator = new QueryBuilderDecorator($qb);
        $qbDecorator->applySorters($sorters);
        $qbDecorator->applyFilters($filters);

        $total = $qb->select('COUNT(DISTINCT i.id)')->getQuery()->execute(array(), Query::HYDRATE_SINGLE_SCALAR);

        $qb->select('i, t, v, tv');
        $qbDecorator->applyLimit($limit);
        $qbDecorator->applyOffset($offset);

        $collInvoice = $qb->getQuery()->execute();

        $response = new ExtJsJson();
        $response->setData($collInvoice);
        $response->setSerializerGroups(array('Default', 'client', 'seller'));
        $response->setTotal($total);

        return $response;
    }

    public function loadModel($id, $queryParams)
    {
        $invoice = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice')->find($id);

        $response = new ExtJsJson();
        $response->setData($invoice);

        return $response;
    }

    public function createModels(\Traversable $modelListData)
    {
        $response = new ExtJsJson();

        foreach ($modelListData as &$invoiceData) {
            $type = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Type')->find(
                $invoiceData->getType()->getId()
            );
            $invoiceData->setType($type);

            $this->em->persist($invoiceData);

        }
        $this->em->flush();

        $response->setData($modelListData);

        return $response;
    }

    public function updateModels(\Traversable $modelListData)
    {
        $response = new ExtJsJson();

        foreach ($modelListData as &$invoiceData) {
            $type = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Type')->find(
                $invoiceData->getType()->getId()
            );
            $invoiceData->setType($type);

            $invoiceData = $this->em->merge($invoiceData);
        }

        $invoiceData->updatePrices();
        $this->em->flush();

        $response->setData($modelListData);

        return $response;
    }

    public function deleteModel($id)
    {
        $coll = $id;

        foreach ($coll as &$invoiceData) {
            $invoice = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice')->find(
                $invoiceData->getId()
            );
            if ($invoice) {
                $this->em->remove($invoice);
            }
        }
        $this->em->flush();

        $response = new ExtJsJson();;
        $response->setData($coll);

        return $response;
    }

    public function getDataClass()
    {
        return 'Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice';
    }

    /**
     * @param FilterCollectionInterface $filters
     * @return FilterCollection
     */
    private function fixFilters(FilterCollectionInterface $filters)
    {
        $fixed = array();
        /** @var Filter $filter */
        foreach ($filters as $filter) {
            $params = $filter->getParams();

            $fixedFilter = new Filter(
                $filter->getProperty(),
                $filter->getValue(),
                $params
            );

            $property = $filter->getProperty();
            $value = $filter->getValue();
            $type = $filter->getType();
            $comparison = $filter->getComparision();

            switch ($filter->getProperty()) {
                case 'client':
                    $property = 'client.name';
                    $params = new FilterParams(array(
                        'case_sensitive' => false,
                        'like_wildcard' => FilterParams::LIKE_WILDCARD_BOTH,
                        'negation' => false
                    ));
                    break;
                case 'number':
                    $params = new FilterParams(array(
                        'case_sensitive' => false,
                        'like_wildcard' => FilterParams::LIKE_WILDCARD_BOTH,
                        'negation' => false
                    ));
                    break;
            }

            $fixedFilter = new Filter(
                $property,
                $value,
                $params
            );

            $fixedFilter->setType($type);
            $fixedFilter->setComparision($comparison);

            $fixed[] = $fixedFilter;
        }

        return new FilterCollection($fixed);
    }
}
