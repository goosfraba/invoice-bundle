<?php
namespace Webit\Bundle\InvoiceBundle\ExtJsStore\Product;

use Doctrine\ORM\Query;

use Doctrine\Common\Collections\ArrayCollection;

use Webit\Bundle\InvoiceBundle\ExtJsStore\Unit\UnitComboData;

use Webit\Bundle\InvoiceBundle\ExtJsStore\Tax\TaxComboData;

use Symfony\Component\DependencyInjection\ContainerAware;

use Webit\Bundle\InvoiceBundle\Entity\Product\Product;

use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Doctrine\ORM\EntityManager;
use Webit\Bundle\ExtJsBundle\Store\ORM\QueryBuilderDecorator;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreAbstract;

class ProductStore extends ExtJsStoreAbstract
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(array $options = array(), EntityManager $em)
    {
        parent::__construct($options);
        $this->em = $em;
    }

    public function getModelList(
        $queryParams,
        FilterCollectionInterface $filters,
        SorterCollectionInterface $sorters,
        $page = null,
        $limit = null,
        $offset = null
    ) {
        $qb = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Product\Product')->createQueryBuilder('p');
        $qb->leftJoin('p.taxRate', 't')->leftJoin('p.unit', 'u');

        $qbDecorator = new QueryBuilderDecorator($qb);
        $qbDecorator->applySorters($sorters);
        $qbDecorator->applyFilters($filters);
        $qbDecorator->applyLimit($limit);
        $qbDecorator->applyOffset($offset);

        $collProduct = $qb->getQuery()->execute();

        $response = new ExtJsJson();
        $response->setData($collProduct);

        return $response;
    }

    private function createData(Product $product)
    {
        $productData = new ProductData($product);

        $taxData = new TaxComboData($product->getTaxRate());
        $taxValue = $product->getTaxRate()->getCurrentValue();
        if ($taxValue) {
            $taxData->setValue($taxValue->getValue());
            $taxData->setRatio(($taxValue->getValue() + 100) / 100);
            $taxData->setLabel($product->getTaxRate()->getName() . ' (' . $taxValue->getValue() . '%)');
        }
        $productData->setTaxRate($taxData);

        $unitData = new UnitComboData($product->getUnit());
        $productData->setUnit($unitData);

        return $productData;
    }

    private function updateEntity($product, ProductData $productData)
    {
        $productData->fillEntity($product);

        $taxRate = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Tax\TaxRate')->find(
            $productData->getTaxRateId()
        );
        $product->setTaxRate($taxRate);

        $unit = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Unit\Unit')->find(
            $productData->getUnitId()
        );
        $product->setUnit($unit);
    }

    public function loadModel($id, $queryParams)
    {
        $product = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Product\Product')->find($id);

        $response = new ExtJsJson();
        $response->setData($product);

        return $response;
    }

    public function createModels(\Traversable $modelListData)
    {
        $response = new ExtJsJson();

        foreach ($modelListData as &$productData) {
            $rate = $this->em->find('Webit\Bundle\InvoiceBundle\Entity\Tax\TaxRate', $productData->getTaxRateId());
            $productData->setTaxRate($rate);

            $unit = $this->em->find('Webit\Bundle\InvoiceBundle\Entity\Unit\Unit', $productData->getUnitId());
            $productData->setUnit($unit);

            $this->em->persist($productData);
            $this->em->flush();
        }

        $response->setData($modelListData);

        return $response;
    }

    public function updateModels(\Traversable $modelListData)
    {
        $response = new ExtJsJson();

        foreach ($modelListData as &$productData) {
            $rate = $this->em->find('Webit\Bundle\InvoiceBundle\Entity\Tax\TaxRate', $productData->getTaxRateId());
            $productData->setTaxRate($rate);

            $unit = $this->em->find('Webit\Bundle\InvoiceBundle\Entity\Unit\Unit', $productData->getUnitId());
            $productData->setUnit($unit);

            $this->em->merge($productData);
        }
        $this->em->flush();

        $response->setData($modelListData);

        return $response;
    }

    public function deleteModel($id)
    {
        $coll = $id;
        $arData = array();
        foreach ($coll as $productData) {
            $arData[] = $evidence->deleteProduct($productData);
        }

        $response = new ExtJsJson();;
        $response->setData($arData);

        return $response;
    }

    public function getDataClass()
    {
        return 'Webit\Bundle\InvoiceBundle\Entity\Product\Product';
    }
}
