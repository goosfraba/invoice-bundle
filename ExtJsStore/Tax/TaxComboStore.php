<?php
namespace Webit\Bundle\InvoiceBundle\ExtJsStore\Tax;

use Doctrine\ORM\Query;

use Symfony\Component\DependencyInjection\ContainerAware;

use Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity;

use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;

use Doctrine\ORM\EntityManager;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreAbstract;

class TaxComboStore extends ExtJsStoreAbstract
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(array $options = array(), EntityManager $em)
    {
        parent::__construct($options);

        $this->em = $em;
    }

    public function getModelList(
        $queryParams,
        FilterCollectionInterface $filters,
        SorterCollectionInterface $sorters,
        $page = 1,
        $limit = 25,
        $offset = 0
    ) {
        $qb = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Tax\TaxRate')->createQueryBuilder('r');
        $qb->innerJoin('r.values', 'v')->orderBy('r.name', 'ASC');

        $arResults = $qb->getQuery()->execute();

        $response = new ExtJsJson();
        $response->setData($arResults);
        $response->setSerializerGroups(array('taxRateCombo'));

        return $response;
    }

    public function loadModel($id, $queryParams)
    {
        throw new \RuntimeException('Loading not allowed');
    }

    public function createModels(\Traversable $modelListData)
    {
        throw new \RuntimeException('Create not allowed');
    }

    public function updateModels(\Traversable $modelListData)
    {
        throw new \RuntimeException('Update not allowed');
    }

    public function deleteModel($id)
    {
        throw new \RuntimeException('Delete not allowed');
    }

    public function getDataClass()
    {
        return 'Webit\Bundle\InvoiceBundle\Entity\Tax\TaxRate';
    }

}
