<?php

namespace Webit\Bundle\InvoiceBundle\Integration;

abstract class AbstractVendorId
{
    /** @var Vendor */
    private $vendor;

    /** @var string */
    private $id;

    /**
     * @param Vendor $vendor
     * @param string $id
     */
    public function __construct(Vendor $vendor, $id)
    {
        $this->vendor = $vendor;
        $this->id = $id;
    }

    /**
     * @return Vendor
     */
    public function vendor()
    {
        return $this->vendor;
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return sprintf('%s:%s', $this->vendor(), $this->id());
    }
}