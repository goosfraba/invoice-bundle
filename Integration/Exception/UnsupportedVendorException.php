<?php

namespace Webit\Bundle\InvoiceBundle\Integration\Exception;

use Webit\Bundle\InvoiceBundle\Integration\Vendor;

class UnsupportedVendorException extends \RuntimeException
{
    /** @var Vendor */
    private $vendor;

    /**
     * @param Vendor $vendor
     * @return UnsupportedVendorException
     */
    public static function create(Vendor $vendor)
    {
        $e = new self(sprintf('Vendor "%s" is not supported.', $vendor));
        $e->vendor = $vendor;

        return $e;
    }

    /**
     * @return Vendor
     */
    public function vendor()
    {
        return $this->vendor;
    }
}