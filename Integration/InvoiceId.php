<?php

namespace Webit\Bundle\InvoiceBundle\Integration;

final class InvoiceId extends AbstractVendorId
{
    /** @var \DateTime */
    private $pushedAt;

    public function __construct(Vendor $vendor, $id)
    {
        parent::__construct($vendor, $id);
        $this->pushedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function pushedAt()
    {
        return $this->pushedAt;
    }
}
