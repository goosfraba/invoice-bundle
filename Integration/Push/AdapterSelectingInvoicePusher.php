<?php

namespace Webit\Bundle\InvoiceBundle\Integration\Push;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Integration\Exception\UnsupportedVendorException;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;

final class AdapterSelectingInvoicePusher implements InvoicePusher
{
    /** @var InvoicePusherAdapter[] */
    private $adapters;

    /**
     * @param InvoicePusherAdapter[] $adapters
     */
    public function __construct(array $adapters)
    {
        $this->adapters = $adapters;
    }

    /**
     * @inheritdoc
     */
    public function push(Invoice $invoice, Vendor $vendor)
    {
        if (isset($this->adapters[(string)$vendor])) {
            return $this->adapters[(string)$vendor]->push($invoice);
        }

        throw UnsupportedVendorException::create($vendor);
    }
}
