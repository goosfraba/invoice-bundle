<?php

namespace Webit\Bundle\InvoiceBundle\Integration\Push;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\Bundle\InvoiceBundle\Integration\InvoiceId;

interface InvoicePusher
{
    /**
     * @param Invoice $invoice
     * @param Vendor $vendor
     * @return InvoiceId
     */
    public function push(Invoice $invoice, Vendor $vendor);
}
