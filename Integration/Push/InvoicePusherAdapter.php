<?php

namespace Webit\Bundle\InvoiceBundle\Integration\Push;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Integration\InvoiceId;

interface InvoicePusherAdapter
{
    /**
     * @param Invoice $invoice
     * @return InvoiceId
     */
    public function push(Invoice $invoice);
}
