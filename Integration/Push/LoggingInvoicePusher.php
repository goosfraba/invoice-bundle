<?php

namespace Webit\Bundle\InvoiceBundle\Integration\Push;

use Psr\Log\LoggerInterface;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;

final class LoggingInvoicePusher implements InvoicePusher
{
    /** @var InvoicePusher */
    private $pusher;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(InvoicePusher $pusher, LoggerInterface $logger)
    {
        $this->pusher = $pusher;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function push(Invoice $invoice, Vendor $vendor)
    {
        $this->logger->info(
            sprintf(
                '[INVOICE PUSH] Pushing invoice of ID "%s" to the "%s" vendor.',
                $invoice->getId(),
                $vendor
            ),
            [
                'id' => $invoice->getId(),
                'vendor' => (string)$vendor
            ]
        );

        try {
            $vendorId = $this->pusher->push($invoice, $vendor);
        } catch (\Exception $e) {
            $this->logger->error(
                sprintf(
                    '[INVOICE PUSH] Error during pushing the invoice of ID "%s" to the "%s" vendor.',
                    $invoice->getId(),
                    $vendor
                ),
                [
                    'id' => $invoice->getId(),
                    'vendor' => (string)$vendor,
                    'class' => get_class($e),
                    'msg' => $e->getMessage()
                ]
            );
            throw $e;
        }

        return $vendorId;
    }
}
