<?php

namespace Webit\Bundle\InvoiceBundle\Integration\Push;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\Bundle\InvoiceBundle\Service\InvoiceManagerInterface;

final class PersistingInvoicePusher implements InvoicePusher
{
    /** @var InvoicePusher */
    private $pusher;

    /** @var InvoiceManagerInterface */
    private $invoiceManager;

    public function __construct(InvoicePusher $pusher, InvoiceManagerInterface $invoiceManager)
    {
        $this->pusher = $pusher;
        $this->invoiceManager = $invoiceManager;
    }

    /**
     * @inheritdoc
     */
    public function push(Invoice $invoice, Vendor $vendor)
    {
        $vendorId = $this->pusher->push($invoice, $vendor);
        $invoice->setVendorId($vendorId);
        $this->invoiceManager->updateInvoice($invoice);

        return $vendorId;
    }
}
