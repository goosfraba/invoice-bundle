<?php

namespace Webit\Bundle\InvoiceBundle\Integration;

final class Vendor
{
    /** @var string */
    private $vendor;

    /**
     * @param string $vendor
     */
    private function __construct($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @param string $vendor
     * @return Vendor
     */
    public static function fromString($vendor)
    {
        return new self($vendor);
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return $this->vendor;
    }
}
