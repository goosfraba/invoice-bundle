<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma\Command;

use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Integration\WFirma\TypeLinker;
use Webit\Bundle\InvoiceBundle\Integration\WFirma\WFirma;

class LinkTypeCommand extends ContainerAwareCommand
{
    /** @var TypeLinker */
    private $typeLinker;

    /** @var EntityRepository */
    private $typeRepository;

    protected function configure()
    {
        $this->setName(
            'webit-invoice:wfirma:link-type'
        );

        $this->addArgument(
            'type',
            InputArgument::REQUIRED, 'Type ID to be linked'
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->typeLinker = $this->getContainer()->get('webit_invoice.wfirma.type_linker');
        $this->typeRepository = $this->getContainer()->get('webit_invoice.type_repository');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $typeId = $input->getArgument('type');

        /** @var Type $type */
        $type = $this->typeRepository->find($input->getArgument('type'));
        if (!$type) {
            throw new \OutOfBoundsException(
                sprintf('Could not find Type of ID "%s"', $typeId)
            );
        }

        $vendorId = $type->vendorId(WFirma::vendor());
        if ($vendorId) {
            throw new \RuntimeException(
                sprintf(
                    'The Type of ID "%s" is already linked to the wFirma series of ID "%s"',
                    $typeId,
                    $vendorId->id()
                )
            );
        }

        $vendorId = $this->typeLinker->link($type);
        $output->writeln(
            sprintf(
                'The Type of ID <info>"%s"</info> has been linked to the wFirma series of ID <info>"%s"</info>.',
                $typeId,
                $vendorId->id()
            )
        );
    }
}