<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Contractor;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\VatNo;
use Webit\EDeklaracje\Countries\Directory;
use Webit\EDeklaracje\Countries\DirectoryFactory;
use Webit\WFirmaSDK\Contractors\InvoiceAddress;
use Webit\WFirmaSDK\Contractors\TaxIdType;

class ContractorMapper
{
    /** @var Directory */
    private $countryDirectory;

    /**
     * @param Directory $countryDirectory
     */
    public function __construct(Directory $countryDirectory = null)
    {
        $this->countryDirectory = $countryDirectory ?: $this->createDirectory();
    }

    /**
     * @param Contractor $contractor
     * @return \Webit\WFirmaSDK\Contractors\Contractor
     */
    public function map(Contractor $contractor)
    {
        list($nip, $taxTypeId) = $this->taxId($contractor->vatNo());

        return new \Webit\WFirmaSDK\Contractors\Contractor(
            $contractor->name(),
            null,
            $nip,
            null,
            new InvoiceAddress(
                $contractor->address()->address(),
                $contractor->address()->postCode(),
                $contractor->address()->post(),
                $this->countryDirectory->lookup($contractor->address()->country())
            ),
            $contactAddress = null,
            null,
            null,
            true,
            false,
            null,
            null,
            $taxTypeId
        );
    }

    /**
     * @return \Webit\EDeklaracje\Countries\ByNameDirectory
     */
    private function createDirectory()
    {
        $factory = new DirectoryFactory();
        return $factory->createByName();
    }

    private function taxId(VatNo $vatNo = null)
    {
        if ($vatNo == null || strtoupper($vatNo->canonical()) == 'PRYWATNY') {
            return array(null, null);
        }

        if ($vatNo->countryCode() != 'PL') {
            return array(
              $vatNo->canonical(),
              TaxIdType::vat()
            );
        }

        return array($vatNo->number(), TaxIdType::nip());
    }
}
