<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma\Exception;

class InvoiceNumberMismatchException extends \RuntimeException
{
    /**
     * @param string $expectedInvoiceNumber
     * @param string $mappedInvoiceNumber
     * @return InvoiceNumberMismatchException
     */
    public static function create($expectedInvoiceNumber, $mappedInvoiceNumber)
    {
        return new self(
            sprintf(
                'Pushed invoice number "%s" does not match the original one ("%s")',
                $mappedInvoiceNumber,
                $expectedInvoiceNumber
            )
        );
    }
}