<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem;
use Webit\WFirmaSDK\Invoices\Discount;
use Webit\WFirmaSDK\Invoices\InvoicesContent;

class InvoiceContentMapper
{
    /**
     * @param InvoiceItem $item
     * @return InvoicesContent
     */
    public function map(InvoiceItem $item)
    {
        $discount = $item->getPrice()->getValue() - $item->getDiscountPrice()->getValue();


        return InvoicesContent::fromName(
            $this->mapItemDescription($item),
            $item->getUnit()->getSymbol(),
            $item->getQuantity(),
            $item->getPrice()->getValue(),
            $item->getPrice()->getVatValue() * 100,
            $discount > 0 ? new Discount(null, $discount) : null
        );
    }

    /**
     * @param InvoiceItem $item
     * @return string
     */
    private function mapItemDescription(InvoiceItem $item)
    {
        if ($description = $item->getDescription()) {
            return sprintf('%s (%s)', $item->getName(), $description);
        }

        return $item->getName();
    }
}
