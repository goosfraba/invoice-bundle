<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Integration\WFirma\Series\SeriesIdResolver;
use Webit\WFirmaSDK\Invoices\Disposal;

class InvoiceMapper
{
    /** @var ContractorMapper */
    private $contractorMapper;

    /** @var PaymentMapper */
    private $paymentMapper;

    /** @var InvoiceTypeMapper */
    private $invoiceTypeMapper;

    /** @var SeriesIdResolver */
    private $seriesIdResolver;

    /** @var InvoiceContentMapper */
    private $invoiceContentMapper;

    /**
     * @param ContractorMapper $contractorMapper
     * @param PaymentMapper $paymentMapper
     * @param InvoiceTypeMapper $invoiceTypeMapper
     * @param SeriesIdResolver $seriesIdResolver
     * @param InvoiceContentMapper $invoiceItemMapper
     */
    public function __construct(
        ContractorMapper $contractorMapper,
        PaymentMapper $paymentMapper,
        InvoiceTypeMapper $invoiceTypeMapper,
        SeriesIdResolver $seriesIdResolver,
        InvoiceContentMapper $invoiceItemMapper
    ) {
        $this->contractorMapper = $contractorMapper;
        $this->paymentMapper = $paymentMapper;
        $this->invoiceTypeMapper = $invoiceTypeMapper;
        $this->seriesIdResolver = $seriesIdResolver;
        $this->invoiceContentMapper = $invoiceItemMapper;
    }

    /**
     * @return InvoiceMapper
     */
    public static function create()
    {
        return new self(
            new ContractorMapper(),
            new PaymentMapper(),
            new InvoiceTypeMapper(),
            new SeriesIdResolver(),
            new InvoiceContentMapper()
        );
    }

    /**
     * @param Invoice $invoice
     * @param \Webit\WFirmaSDK\Invoices\Invoice|null $wFirmaInvoice
     * @return \Webit\WFirmaSDK\Invoices\Invoice
     */
    public function map(Invoice $invoice, \Webit\WFirmaSDK\Invoices\Invoice $wFirmaInvoice = null)
    {
        if ($wFirmaInvoice) {
            $wFirmaInvoice->changePayment(
                $this->paymentMapper->map($invoice)
            );

            $wFirmaInvoice->changeContractor(
                $this->contractorMapper->map($invoice->getClient())
            );

            $wFirmaInvoice->changeIssueDate($invoice->getIssueDate());
            $wFirmaInvoice->changeDisposal(
                Disposal::withDate($this->resolveSaleDate($invoice))
            );

            $contents = $wFirmaInvoice->invoiceContents();
            foreach ($contents as $content) {
                $wFirmaInvoice->removeInvoiceContent($content);
            }

            foreach ($invoice->getItems() as $item) {
                $wFirmaInvoice->addInvoiceContent($this->invoiceContentMapper->map($item));
            }

            return $wFirmaInvoice;
        }

        $wFirmaInvoice = \Webit\WFirmaSDK\Invoices\Invoice::forContractor(
            $this->contractorMapper->map($invoice->getClient()),
            $this->paymentMapper->map($invoice),
            $this->invoiceTypeMapper->map($invoice->getType()),
            $this->seriesIdResolver->resolve($invoice->getType()),
            $invoice->getIssueDate(),
            Disposal::withDate($invoice->getIssueDate())
        );

        foreach ($invoice->getItems() as $item) {
            $wFirmaInvoice->addInvoiceContent($this->invoiceContentMapper->map($item));
        }

        return $wFirmaInvoice;
    }

    private function resolveSaleDate(Invoice $invoice)
    {
        return $invoice->getLastPaymentDate() ?: $invoice->getSaleDate();
    }
}
