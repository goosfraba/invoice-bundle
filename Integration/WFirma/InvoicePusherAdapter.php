<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Integration\InvoiceId;
use Webit\Bundle\InvoiceBundle\Integration\Push\InvoicePusherAdapter as InvoicePusherAdapterInterface;
use Webit\Bundle\InvoiceBundle\Integration\WFirma\Exception\InvoiceNumberMismatchException;
use Webit\WFirmaSDK\Entity\Exception\NotFoundException;
use Webit\WFirmaSDK\Invoices\InvoicesApi;

final class InvoicePusherAdapter implements InvoicePusherAdapterInterface
{
    /** @var InvoicesApi */
    private $invoicesApi;

    /** @var InvoiceMapper */
    private $invoiceMapper;

    public function __construct(InvoicesApi $invoiceApi, InvoiceMapper $invoiceMapper = null)
    {
        $this->invoicesApi = $invoiceApi;
        $this->invoiceMapper = $invoiceMapper ?: InvoiceMapper::create();
    }

    /**
     * @inheritdoc
     */
    public function push(Invoice $invoice)
    {
        $vendorId = $invoice->vendorId(WFirma::vendor());

        $mappedInvoice = null;
        if ($vendorId) {
            try {
                $mappedInvoice = $this->invoicesApi->get(new \Webit\WFirmaSDK\Invoices\InvoiceId($vendorId->id()));
            } catch (NotFoundException $e) {
                $mappedInvoice = null;
            }
        }

        $mappedInvoice = $this->invoiceMapper->map($invoice, $mappedInvoice);

        $mappedInvoice = $mappedInvoice->id() ? $this->invoicesApi->edit($mappedInvoice) : $this->invoicesApi->add($mappedInvoice);

        $expectedInvoiceNumber = $invoice->getNumber();
        $mappedInvoiceNumber = $mappedInvoice->number()->fullNumber();

        if ($expectedInvoiceNumber != $mappedInvoiceNumber) {
            $this->invoicesApi->delete($mappedInvoice->id());
            throw InvoiceNumberMismatchException::create($expectedInvoiceNumber, $mappedInvoiceNumber);
        }

        return new InvoiceId(WFirma::vendor(), $mappedInvoice->id()->id());
    }
}
