<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;

class InvoiceTypeMapper
{
    /**
     * @param Type $type
     * @return \Webit\WFirmaSDK\Invoices\Type
     */
    public function map(Type $type)
    {
        switch($type->getMasterType()) {
            case 'proforma':
                return \Webit\WFirmaSDK\Invoices\Type::proformaVat();
        }

        return \Webit\WFirmaSDK\Invoices\Type::vat();
    }
}