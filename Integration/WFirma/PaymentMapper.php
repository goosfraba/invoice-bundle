<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod;
use Webit\WFirmaSDK\Invoices\Payment;

class PaymentMapper
{
    /**
     * @param Invoice $invoice
     * @return Payment
     */
    public function map(Invoice $invoice)
    {
        return Payment::create(
            $this->mapPaymentMethod($invoice->getPaymentMethod()),
            $invoice->getDueDate(),
            $this->resolvePaidAmount($invoice)
        );
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @return \Webit\WFirmaSDK\Invoices\PaymentMethod
     */
    private function mapPaymentMethod(PaymentMethod $paymentMethod)
    {
        switch ($paymentMethod->getCode()) {
            case 'cash':
                return \Webit\WFirmaSDK\Invoices\PaymentMethod::cash();
            case 'cod':
                return \Webit\WFirmaSDK\Invoices\PaymentMethod::cod();
        }

        return \Webit\WFirmaSDK\Invoices\PaymentMethod::transfer();
    }

    /**
     * @param Invoice $invoice
     * @return float
     */
    private function resolvePaidAmount(Invoice $invoice)
    {
        $paymentMethod = $invoice->getPaymentMethod();

        $amount = $invoice->getTaxedValue()->getValue();
        if ($paymentMethod->getDueDateOffset() > 0) {
            $amount = $invoice->prePaidAmount();
        }

        return round($amount, 2);
    }
}
