<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Integration\Push\InvoicePusherAdapter;
use Webit\WFirmaSDK\Entity\Exception\ApiCallExecutionException;

final class RetryingPusherAdapter implements InvoicePusherAdapter
{
    /** @var int */
    private static $attempt = 0;

    /** @var InvoicePusherAdapter */
    private $innerPusher;

    /** @var int */
    private $maxAttempts = 3;

    /** @var int */
    private $sleepInSeconds = 2;

    public function __construct(InvoicePusherAdapter $innerPusher, $maxAttempts = 3, $sleepInSeconds = 2)
    {
        $this->innerPusher = $innerPusher;
        $this->maxAttempts = $maxAttempts;
        $this->sleepInSeconds = $sleepInSeconds;
    }

    /**
     * @inheritdoc
     */
    public function push(Invoice $invoice)
    {
        return $this->tryPush($invoice);
    }

    private function tryPush(Invoice $invoice)
    {
        try {
            $result = $this->innerPusher->push($invoice);
            self::$attempt = 0;

            return $result;
        } catch (ApiCallExecutionException $e) {
            self::$attempt++;
            if (self::$attemt >= $this->maxAttempts) {
                self::$attempt = 0;
                throw $e;
            }

            sleep($this->wait);
            $this->tryPush($invoice);
        }
    }
}