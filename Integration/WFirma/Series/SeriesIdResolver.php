<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma\Series;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Integration\WFirma\WFirma;
use Webit\WFirmaSDK\Series\SeriesId;

class SeriesIdResolver
{
    /**
     * @param Type $type
     * @return SeriesId
     */
    public function resolve(Type $type)
    {
        $vendorId = $type->vendorId(WFirma::vendor());
        if ($vendorId) {
            return SeriesId::create((int)$vendorId->id());
        }

        return null;
    }
}
