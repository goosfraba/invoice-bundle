<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma\Series;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\WFirmaSDK\Series\ResetMode;
use Webit\WFirmaSDK\Series\Series;
use Webit\WFirmaSDK\Series\TemplateTokens;

class SeriesMapper
{
    /**
     * @param Type $type
     * @return Series
     */
    public function map(Type $type)
    {
        return new Series(
            $type->getLabel(),
            sprintf(
                '%s%s/%s',
                $type->getPrefix(),
                TemplateTokens::year(),
                TemplateTokens::number($type->getNumberLength())
            ),
            \Webit\WFirmaSDK\Invoices\Type::vat(),
            1,
            ResetMode::yearly()
        );
    }
}
