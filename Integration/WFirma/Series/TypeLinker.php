<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma\Series;

use Doctrine\ORM\EntityManagerInterface;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Integration\TypeId;
use Webit\Bundle\InvoiceBundle\Integration\WFirma\WFirma;
use Webit\WFirmaSDK\Series\SeriesApi;

class TypeLinker
{
    /** @var SeriesApi */
    private $seriesApi;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var SeriesMapper */
    private $mapper;

    public function __construct(
        SeriesApi $seriesApi,
        EntityManagerInterface $entityManager,
        SeriesMapper $mapper = null
    ) {
        $this->seriesApi = $seriesApi;
        $this->entityManager = $entityManager;
        $this->mapper = $mapper ?: new SeriesMapper();
    }

    /**
     * @param Type $type
     * @return TypeId
     */
    public function link(Type $type)
    {
        $series = $this->mapper->map($type);
        $series = $this->seriesApi->add($series);

        $type->setVendorId($typeId = new TypeId(WFirma::vendor(), $series->id()->id()));
        $this->entityManager->flush();

        return $typeId;
    }
}
