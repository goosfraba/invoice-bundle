<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\WFirmaSDK\Series\SeriesId;

class SeriesMapper
{
    /**
     * @param Type $type
     * @return SeriesId
     */
    public function map(Type $type)
    {
        $vendorId = $type->vendorId(WFirma::vendor());
        if ($vendorId) {
            return SeriesId::create((int)$vendorId);
        }

        return null;
    }
}
