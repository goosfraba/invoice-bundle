<?php

namespace Webit\Bundle\InvoiceBundle\Integration\WFirma;

use Webit\Bundle\InvoiceBundle\Integration\Vendor;

final class WFirma
{
    /**
     * @return Vendor
     */
    public static function vendor()
    {
        return Vendor::fromString('wFirma');
    }
}
