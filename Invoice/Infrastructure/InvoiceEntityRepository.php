<?php

namespace Webit\Bundle\InvoiceBundle\Invoice\Infrastructure;

use Doctrine\ORM\EntityRepository;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\Bundle\InvoiceBundle\Invoice\InvoiceRepository;
use Webit\Bundle\InvoiceBundle\Invoice\Month;

final class InvoiceEntityRepository extends EntityRepository implements InvoiceRepository
{
    /**
     * @inheritdoc
     */
    public function invoicesForVendorAndMonth(Vendor $vendor, Month $month)
    {
        $qb = $this->createQueryBuilder('i');
        $qb
            ->innerJoin('i.type', 't')
            ->innerJoin(
                't.vendorIds',
                'tv',
                'WITH',
                $qb->expr()->eq('tv.vendor', ':vendor')
            )
            ->where($qb->expr()->gte('i.issueDate', ':month_start'))
            ->andWhere($qb->expr()->lt('i.issueDate', ':month_end'));


        $qb->setParameter('vendor', (string)$vendor);
        $qb->setParameter('month_start', $month->start());
        $qb->setParameter('month_end', $month->end(true));

        $invoices = $qb->getQuery()->execute();

        return $invoices;
    }

    /**
     * @inheritdoc
     */
    public function invoiceOfId($id)
    {
        return $this->find($id);
    }
}
