<?php

namespace Webit\Bundle\InvoiceBundle\Invoice\Infrastructure;

use Doctrine\ORM\EntityRepository;
use Webit\Bundle\InvoiceBundle\Invoice\TypeRepository;

final class TypeEntityRepository extends EntityRepository implements TypeRepository
{
    /**
     * @inheritdoc
     */
    public function typeOfId($id)
    {
        return $this->find($id);
    }
}