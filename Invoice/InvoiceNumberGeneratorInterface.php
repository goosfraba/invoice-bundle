<?php
namespace Webit\Bundle\InvoiceBundle\Invoice;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

interface InvoiceNumberGeneratorInterface
{

    /**
     *
     * @param Invoice $invoice
     * @return string
     */
    public function generateInvoiceNumber(Invoice $invoice);
}
