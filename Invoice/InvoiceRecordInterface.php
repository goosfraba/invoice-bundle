<?php
namespace Webit\Bundle\InvoiceBundle\Invoice;

use Webit\Tools\Data\FilterCollection;
use Webit\Tools\Data\SorterCollection;
use Webit\Bundle\InvoiceBundle\Model\InvoiceInterafce;

interface InvoiceRecordInterface
{

    /**
     * @return string
     */
    public function getName();

    /**
     * @return InvoiceInterafce
     */
    public function getInvoice($id);

    /**
     *
     * @param FilterCollection $filters
     * @return InvoiceInterafce
     */
    public function findInvoice(FilterCollection $filters);

    /**
     *
     * @param FilterCollection $filters
     * @param SorterCollection $sorters
     * @param string $limit
     * @param string $offset
     * @return array
     */
    public function getInvoices(
        FilterCollection $filters = null,
        SorterCollection $sorters = null,
        $limit = null,
        $offset = null
    );

    /**
     *
     * @param InvoiceInterafce $invoice
     */
    public function updateInvoice(InvoiceInterafce $invoice);

    /**
     *
     * @param InvoiceInterafce $invoice
     */
    public function deleteInvoice(InvoiceInterafce $invoice);
}
