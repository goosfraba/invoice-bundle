<?php

namespace Webit\Bundle\InvoiceBundle\Invoice;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;

interface InvoiceRepository
{
    /**
     * @param Vendor $vendor
     * @param Month $month
     * @return Invoice[]
     */
    public function invoicesForVendorAndMonth(Vendor $vendor, Month $month);

    /**
     * @param int $id
     * @return Invoice
     */
    public function invoiceOfId($id);
}
