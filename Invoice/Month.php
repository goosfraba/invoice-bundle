<?php

namespace Webit\Bundle\InvoiceBundle\Invoice;

final class Month
{
    /** @var \DateTime */
    private $start;

    /** @var \DateTime */
    private $end;

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     */
    private function __construct(\DateTime $start, \DateTime $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return \DateTime
     */
    public function start()
    {
        return clone $this->start;
    }

    /**
     * @param bool $exclusive
     * @return \DateTime
     * @throws \Exception
     */
    public function end($exclusive = false)
    {
        if ($exclusive) {
            return clone $this->end;
        }

        $end = clone $this->end;
        $end->sub(new \DateInterval('P1D'));
        $end->setTime(23, 59, 59);

        return $end;
    }

    /**
     * @return Month
     */
    public function next()
    {
        return self::create($this->end(true));
    }

    /**
     * @return Month
     */
    public function previous()
    {
        return self::create($this->start()->sub(new \DateInterval('P1D')));
    }

    /**
     * @param \DateTime $month
     * @return Month
     */
    public static function create(\DateTime $month)
    {
        $monthStart = clone ($month);
        $monthStart->setTime(0, 0, 0);
        $monthStart->setDate(
            $monthStart->format('Y'),
            $monthStart->format('n'),
            1
        );

        $monthEnd = clone $monthStart;
        try {
            $monthEnd = $monthEnd->add(new \DateInterval('P1M'));
        } catch (\Exception $e) {
            throw new \RuntimeException('Invalid DateInterval format', 0, $e);
        }

        return new self($monthStart, $monthEnd);
    }
}