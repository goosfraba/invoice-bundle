<?php

namespace Webit\Bundle\InvoiceBundle\Invoice;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;

interface TypeRepository
{
    /**
     * @param mixed $id
     * @return Type
     */
    public function typeOfId($id);
}