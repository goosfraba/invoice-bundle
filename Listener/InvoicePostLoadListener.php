<?php
namespace Webit\Bundle\InvoiceBundle\Listener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Service\InvoiceUrlProviderInterface;

class InvoicePostLoadListener implements EventSubscriber
{
    /** @var InvoiceUrlProviderInterface */
    private $urlProvider;

    public function __construct(InvoiceUrlProviderInterface $urlProvider)
    {
        $this->urlProvider = $urlProvider;
    }

    public function getSubscribedEvents()
    {
        return array(Events::postLoad => 'onPostLoad');
    }

    public function onPostLoad(LifecycleEventArgs $event)
    {
        $invoice = $event->getEntity();
        if ($invoice instanceof Invoice) {
            $url = $this->urlProvider->getUrl($invoice);
            $invoice->setUrl($url);
        }
    }
}
