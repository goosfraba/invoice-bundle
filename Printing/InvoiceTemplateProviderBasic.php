<?php
namespace Webit\Bundle\InvoiceBundle\Printing;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

class InvoiceTemplateProviderBasic implements InvoiceTemplateProviderInterface
{
    public function getTemplate(Invoice $invoice)
    {
        $code = $invoice->getType()->getCode();

        return '::' . $code . 'tex.twig';
    }
}
