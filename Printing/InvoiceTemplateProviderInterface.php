<?php
namespace Webit\Bundle\InvoiceBundle\Printing;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

interface InvoiceTemplateProviderInterface
{
    public function getTemplate(Invoice $invoice);
}
