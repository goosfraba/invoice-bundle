<?php
namespace Webit\Bundle\InvoiceBundle\Printing;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Knp\Snappy\GeneratorInterface;
use Symfony\Component\Templating\EngineInterface as Templating;
use Doctrine\Common\Collections\ArrayCollection;
use Webit\PHPgs\Input;
use Webit\PHPgs\Output;
use Webit\PHPgs\Pdf\Merger;

class PrintManager implements PrintManagerInterface
{
    /** @var string*/
    private $printPath;

    /** @var InvoiceTemplateProviderInterface */
    private $tplProvider;

    /** @var Templating */
    private $templating;

    /** @var GeneratorInterface */
    private $printer;

    /** @var Merger */
    private $merger;

    public function __construct(
        $printPath,
        InvoiceTemplateProviderInterface $tplProvider,
        Templating $templating,
        GeneratorInterface $printer,
        Merger $merger
    ) {
        $this->printPath = $printPath;
        $this->tplProvider = $tplProvider;
        $this->templating = $templating;
        $this->printer = $printer;
        $this->merger = $merger;
    }

    /**
     * @param Invoice $invoice
     * @param bool $forceReprint
     * @return \SplFileInfo
     */
    public function printInvoice(Invoice $invoice, $forceReprint = false, $duplicate = false)
    {
        $forceReprint = $forceReprint || $invoice->getType()->getPrintOnFly();
        if ($forceReprint || $duplicate) {
            return $this->generatePrint($invoice, $duplicate);
        }

        return $this->findPrint($invoice) ?: $this->generatePrint($invoice);
    }

    /**
     * @param ArrayCollection $invoices
     * @param bool $forceReprint
     * @return \SplFileInfo
     */
    public function printInvoices(ArrayCollection $invoices, $forceReprint = false)
    {
        $prints = new ArrayCollection(array());
        foreach ($invoices as $invoice) {
            $print = $this->printInvoice($invoice, $forceReprint);
            $prints->add($print);
        }

        $print = $this->mergePdf($prints);

        return $print;
    }

    private function mergePdf(ArrayCollection $prints)
    {
        $dir = sys_get_temp_dir();

        $baseName = substr(md5(microtime() . mt_rand(0, 1000)), 5, 10);
        $filename = $baseName . '.pdf';

        $outputFile = $dir . '/' . $filename;

        $this->merger->merge(Input::multipleFiles($prints->toArray()), Output::create($outputFile));

        return new \SplFileInfo($outputFile);
    }

    private function findPrint(Invoice $invoice)
    {
        if (!$invoice->getPrintedAt()) {
            return null;
        }

        $file = $this->getPrintPathname($invoice);
        if (is_file($file->getPathname())) {
            return $file;
        }

        return null;
    }

    /**
     *
     * @param Invoice $invoice
     * @param bool $duplicate
     * @return \SplFileInfo
     */
    private function generatePrint(Invoice $invoice, $duplicate = false)
    {
        $tpl = $this->tplProvider->getTemplate($invoice);
        $latex = $this->templating->render(
            $tpl,
            array(
                'invoice' => $invoice,
                'duplicate' => $duplicate,
                'printDate' => new \DateTime()
            )
        );

        $output = $this->printer->getOutputFromHtml($latex, array('generate-twice' => true));
        $print = $this->persistPrint($invoice, $output, $duplicate);

        return $print;
    }

    private function persistPrint(Invoice $invoice, $output, $duplicate = false)
    {
        $file = $this->getPrintPathname($invoice, $duplicate);
        file_put_contents($file->getPathname(), $output);
        if (!$duplicate) {
            $invoice->setPrintFilename($file->getFilename());
        }

        return $file;
    }

    /**
     *
     * @param Invoice $invoice
     * @return \SplFileInfo
     */
    private function getPrintPathname(Invoice $invoice, $duplicate = false)
    {
        $dir = $this->printPath . '/' . $invoice->getType()->getCode();
        if (is_dir($dir) == false) {
            @mkdir($dir, 0755, true);
        }

        $filename = $invoice->getPrintFilename();
        if (!$filename || $duplicate) {
            $filename = md5(mt_rand(0, 300) . microtime()) . '.pdf';
        }

        $pathname = $dir . '/' . $filename;

        return new \SplFileInfo($pathname);
    }
}

