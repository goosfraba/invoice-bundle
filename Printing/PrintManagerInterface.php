<?php
namespace Webit\Bundle\InvoiceBundle\Printing;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Doctrine\Common\Collections\ArrayCollection;

interface PrintManagerInterface
{
    /**
     *
     * @param Invoice $invoice
     * @param bool $forceReprint
     * @param bool $duplicate
     * @return \SplFileInfo
     */
    public function printInvoice(Invoice $invoice, $forceReprint = false, $duplicate = false);

    /**
     *
     * @param ArrayCollection $invoices
     * @param boolean $forceReprint
     * @return \SplFileInfo
     */
    public function printInvoices(ArrayCollection $invoices, $forceReprint = false);
}
