Ext.Loader.loadScript(Routing.generate('webit_invoice_static_data'));
Ext.Loader.setPath('Ext.ux', '/bundles/webitextjs/extjs-4.1.1/examples/ux');
Ext.Loader.setPath('Webit', '/bundles/webitextjs/js/ext-webit');

Ext.require([
	'Webit.data.proxy.StoreUrlSelector',
	'Webit.data.proxy.Rest'
]);

Ext.require(['Ext.form.Basic']);
Ext.application({
    name: 'WebitInvoice',
    appFolder: '/bundles/webitinvoice/js/app',
    autoCreateViewport : true,
    models : [
    	'Contractor.Contractor',
    	'Contractor.User',
		'Invoice.Contractor.Address',
		'Invoice.Contractor.BankAccount',
		'Invoice.Contractor.VatNo',
    	'Invoice.Contractor.Contractor',
		'Invoice.Contractor.Supplier',
		'Invoice.Contractor.Customer'
    ],
    stores : [],
    controllers : [
    	'Contractor.ContractorTab',
    	'Contractor.ContractorTab.ContractorGrid',
    	'Contractor.ContractorTab.ContractorPanel',
    	'Product.ProductTab',
    	'Product.ProductTab.ProductGrid',
    	'Product.ProductTab.ProductPanel',
    	'Invoice.InvoiceTab',
    	'Invoice.InvoiceTab.InvoiceGrid',
    	'Invoice.InvoiceTab.InvoicePanel'
    ],
    views : ['WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceItemGrid.ProductPicker'],
    launch : function() {
    	Ext.util.Format.decimalSeparator = ',';
			Ext.util.Format.thousandSeparator = ' ';
    }
});
