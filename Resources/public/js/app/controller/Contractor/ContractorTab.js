Ext.define('WebitInvoice.controller.Contractor.ContractorTab',{
	extend : 'Ext.app.Controller',
	init : function() {
		this.control({
			'webit_invoice_contractor_contractor_tab_contractor_grid[mode="grid"]' : {
				selectionchange : this.onContractorGridSelectionChange
			},
			'webit_invoice_contractor_contractor_tab_contractor_grid[mode="grid"] button[itemId="add"]' : {
				click : this.onContractorGridButtonAddClick
			},
			'webit_invoice_contractor_contractor_tab_contractor_grid[mode="grid"] button[itemId="del"]' : {
				click : this.onContractorGridButtonDelClick
			},
			'webit_invoice_contractor_contractor_tab_contractor_panel' : {
				contractorSave : this.onContractorSave
			}
		});
	},
	onContractorGridSelectionChange : function(sm, selected, eopt) {
		var grid = this.getActiveGrid();
		console.info(grid.mode);
		if(grid.mode == 'grid') {
			if(selected.length == 1) {
				this.getContractorPanel().bindContractor(selected[0].getId(),grid.remoteStoreName);
				grid.down('button[itemId="del"]').enable();
			} else {
				grid.down('button[itemId="del"]').disable();
				this.getContractorPanel().bindContractor(null,grid.evidence);
			}
		}
	},
	onContractorGridButtonDelClick : function(button) {
		var performDelete = function() {
			var grid = button.up('webit_invoice_contractor_contractor_tab_contractor_grid[mode="grid"]');
			var record = grid.getSelectionModel().getLastSelected();
			if(record) {
				record.destroy({
					url : Routing.generate('webit_extjs_get_items',{store : grid.remoteStoreName}),
					callback : function(record, response) {
						if(response.success) {
							grid.getStore().remove(record);
						} else {
							console.error('Nie udało się usunąć kontrahenta');
						}
					}
				});
			}
		};
		
		Ext.Msg.confirm('Usuwanie kontrahenta','Czy na pewno chcesz usunąć wybranego kontrahenta?',function(btn) {
    		if(btn == 'yes') {
    			performDelete();
    		}
    	});
		
	},
	onContractorGridButtonAddClick : function(button) {
		var grid = button.up('webit_invoice_contractor_contractor_tab_contractor_grid[mode="grid"]');
		grid.getSelectionModel().deselectAll();
		
		this.getContractorPanel().bindContractor(null,grid.evidence);
	},
	onContractorSave : function(contractor, phantom) {
		var grid = this.getGridByRemoteStoreName(this.getContractorPanel().remoteStoreName);
		if(phantom) {
			grid.getStore().insert(0,contractor);
		} else {
			var selected = grid.getSelectionModel().getLastSelected();
			// update selected with contractor
		}
	},
	refs : [{
		ref : 'contractorTab',
		selector : 'webit_invoice_contractor_contractor_tab'
	},{
		ref : 'contractorPanel',
		selector : 'webit_invoice_contractor_contractor_tab_contractor_panel'
	},{
		ref  :'contractorGrid',
		selector : 'webit_invoice_contractor_contractor_tab_contractor_grid[mode="grid"]'
	}],
	getActiveGrid : function() {
		return this.getContractorTab().child('tabpanel').getActiveTab();
	},
	getGridByRemoteStoreName : function(remoteStoreName) {
		var grid = this.getContractorTab().down('webit_invoice_contractor_contractor_tab_contractor_grid[remoteStoreName="' + remoteStoreName + '"]');
		
		return grid;
	}
});