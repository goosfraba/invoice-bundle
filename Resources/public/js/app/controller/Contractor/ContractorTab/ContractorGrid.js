Ext.define('WebitInvoice.controller.Contractor.ContractorTab.ContractorGrid',{
	extend : 'Ext.app.Controller',
	init : function() {
		this.control({
			'webit_invoice_contractor_contractor_tab_contractor_grid' : {
				render : function(grid) {
					grid.getStore().load();
				}
			}
		});
	}
});