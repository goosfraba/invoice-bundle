Ext.define('WebitInvoice.controller.Contractor.ContractorTab.ContractorPanel',{
	extend : 'Ext.app.Controller',
	init : function() {
		this.control({
			'webit_invoice_contractor_contractor_tab_contractor_panel' : {
				contractorBind : this.onPanelContractorBind,
				afterrender : function(panel) {
					panel.bindContractor(null);
				}
			},
			'webit_invoice_contractor_contractor_tab_contractor_panel_contractor_form button[itemId="save"]' : {
				click : this.onContractorFormSave
			}
		});
	},
	onPanelContractorBind : function(panel, contractor) {
		this.getContractorForm().loadRecord(contractor);
		if(contractor.phantom == true) {
			panel.setTitle('Nowy kontrahent');
			this.getContractorInvoiceGrid().disable();
		} else {
			panel.setTitle(contractor.get('name'));
			this.getContractorInvoiceGrid().enable();
		}
	},
	onContractorFormSave : function(button) {
		var me = this;
		var contractorPanel = this.getContractorPanel();
		
		var form = this.getContractorForm().getForm();
		form.updateRecord(form.getRecord());
		var phantom = form.getRecord().phantom;
		
		contractorPanel.mask('Zapisywanie danych kontrahenta...');
		form.getRecord().save({
			url : Routing.generate('webit_extjs_get_items',{store : contractorPanel.remoteStoreName}),
			success : function(contractor, action) {
				form.loadRecord(contractor);
				contractorPanel.unmask();
				contractorPanel.fireEvent('contractorSave',contractor,phantom);
			},
			failure : function() {
				contractorPanel.unmask();
				console.err('Błąd zapisu kontrahenta')
				console.err(arguments);
			}
		});
	},
	refs : [{
		ref : 'contractorPanel',
		selector : 'webit_invoice_contractor_contractor_tab_contractor_panel' 
	},{
		ref : 'contractorForm',
		selector : 'webit_invoice_contractor_contractor_tab_contractor_panel_contractor_form'
	},{
		ref : 'contractorInvoiceGrid',
		selector : 'webit_invoice_contractor_contractor_tab_contractor_panel_invoice_grid'
	}]
});