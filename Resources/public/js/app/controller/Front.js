Ext.define('WebitInvoice.controller.Front',{
	extend : 'Ext.app.Controller',
	controllers: [
		'WebitInvoice.controller.Contractor.ContractorTab',
		'WebitInvoice.controller.Contractor.ContractorTab.ContractorGrid',
		'WebitInvoice.controller.Contractor.ContractorTab.ContractorPanel',
		'WebitInvoice.controller.Product.ProductTab',
		'WebitInvoice.controller.Product.ProductTab.ProductGrid',
		'WebitInvoice.controller.Product.ProductTab.ProductPanel',
		'WebitInvoice.controller.Invoice.InvoiceTab',
		'WebitInvoice.controller.Invoice.InvoiceTab.InvoiceGrid',
		'WebitInvoice.controller.Invoice.InvoiceTab.InvoicePanel'
	],
	init : function() {
	},
	views: [
		'WebitInvoice.view.Layout'
	],
	models: [
		'WebitInvoice.model.Invoice.Invoice',
		'WebitInvoice.model.Invoice.InvoiceItem',
		'WebitInvoice.model.Invoice.Contractor',
		'WebitInvoice.model.Invoice.Unit.Unit',
		'WebitInvoice.model.Invoice.Tax.TaxRate',
		'WebitInvoice.model.Invoice.Tax.TaxValue',
		'WebitInvoice.model.Product.Product',
		'WebitInvoice.model.Contractor.Contractor',
		'WebitInvoice.model.Contractor.User'
	]
});