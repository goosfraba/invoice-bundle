Ext.define('WebitInvoice.controller.Invoice.InvoiceTab',{
	extend : 'Ext.app.Controller',
	init : function() {
		this.control({
			'webit_invoice_invoice_invoice_tab_invoice_grid' : {
				selectionchange : this.onInvoiceGridSelectionChange
			},
			'webit_invoice_invoice_invoice_tab_invoice_grid button[itemId="add"]' : {
				click : this.onInvoiceGridButtonAddClick
			},
			'webit_invoice_invoice_invoice_tab_invoice_grid button[itemId="del"]' : {
				click : this.onInvoiceGridButtonDelClick
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid' : {
				itemchange : this.onInvoiceItemChange
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel' : {
				invoiceSave : function(invoice, phantom) {
					var grid = this.getInvoiceTab().down('webit_invoice_invoice_invoice_tab_invoice_grid'); 
					if(phantom) {
						grid.getStore().insert(0,invoice);
						grid.getSelectionModel().select(grid.getStore().getAt(0));
					} else {
						var selection = grid.getSelectionModel().getSelection();
						if(selection.length > 0) {
							selection[0].set(invoice.getData());
						}
					}
				}
			}
		});
	},
	onInvoiceItemChange : function(eOpts) {
		var values = {
			value_net : eOpts.invoice.value_net,
			value_gross : eOpts.invoice.value_gross
		};
		
		var invoiceForm = this.getInvoicePanel().down('webit_invoice_invoice_invoice_tab_invoice_panel_invoice_header_form').getForm();
		invoiceForm.setValues(values);
		
		var selection = this.getInvoiceGrid().getSelectionModel().getSelection();
		if(selection.length > 0) {
			selection[0].set(values);
		}
	},
	onInvoiceGridSelectionChange : function(sm, selected, eopt) {
		var grid = this.getInvoiceGrid();
		if(selected.length == 1) {
			this.getInvoicePanel().bindInvoice(selected[0].getId());
			grid.down('button[itemId="del"]').enable();
		} else {
			grid.down('button[itemId="del"]').disable();
			this.getInvoicePanel().bindInvoice(null);
		}
	},
	onInvoiceGridButtonDelClick : function(button) {
		var performDelete = function() {
			var grid = button.up('webit_invoice_invoice_invoice_tab_invoice_grid');
			var record = grid.getSelectionModel().getLastSelected();
			
			if(record) {
				record.destroy({
					callback : function(record, response) {
						if(response.success) {
							grid.getStore().remove(record);
						} else {
							console.error('Nie udało się usunąć produktu');
						}
					}
				});
			}
		};
		
		Ext.Msg.confirm('Usuwanie faktury','Czy na pewno chcesz usunąć wybraną fakturę?',function(btn) {
    		if(btn == 'yes') {
    			performDelete();
    		}
    	});
	},
	onInvoiceGridButtonAddClick : function(button) {
		var grid = button.up('webit_invoice_invoice_invoice_tab_invoice_grid');
		grid.getSelectionModel().deselectAll();
		
		this.getInvoicePanel().bindInvoice(null);
	},
	onInvoiceSave : function(invoice, phantom) {
		var grid = this.getInvoiceGrid();
		if(phantom) {
			grid.getStore().insert(0,invoice);
		} else {
			var selected = grid.getSelectionModel().getLastSelected();
			// update selected with contractor
		}
	},
	refs : [{
		ref : 'invoiceTab',
		selector : 'webit_invoice_invoice_invoice_tab'
	},{
		ref : 'invoicePanel',
		selector : 'webit_invoice_invoice_invoice_tab_invoice_panel'
	},{
		ref  :'invoiceGrid',
		selector : 'webit_invoice_invoice_invoice_tab_invoice_grid'
	}]
});