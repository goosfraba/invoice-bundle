Ext.define('WebitInvoice.controller.Invoice.InvoiceTab.InvoiceGrid',{
	extend : 'Ext.app.Controller',
	init : function() {
		this.control({
			'webit_invoice_invoice_invoice_tab_invoice_grid' : {
				render : function(grid) {
					grid.getStore().load();
				}
			}
		});
	}
});