Ext.define('WebitInvoice.controller.Invoice.InvoiceTab.InvoicePanel',{
	extend : 'Ext.app.Controller',
	init : function() {
		this.control({
			'webit_invoice_invoice_invoice_tab_invoice_panel' : {
				invoiceBind : this.onPanelInvoiceBind,
				afterrender : function(panel) {
					panel.bindInvoice(null);
				},
				invoiceSave : this.onInvoiceSave
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_header_form button[itemId="save"]' : {
				click : this.onInvoiceFormSave
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_header_form button[itemId="contractor-select"]': {
				click : this.onContractorSelectClick
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_header_form_contractor_picker' : {
				contractorPick : this.onContractorPick
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid' : {
				selectionchange : this.onInvoiceItemGridSelectionchange
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid button[itemId="add"]' : {
				click : this.onInvoiceItemAddClick
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid button[itemId="edit"]' : {
				click : this.onInvoiceItemEditClick
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid button[itemId="del"]' : {
				click : this.onInvoiceItemDelClick
			},
			'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid_product_picker' : {
				productPick : this.onInvoiceItemPick
			}
		});
	},
	onInvoiceItemGridSelectionchange : function(sm, selected) {
		if(selected.length == 1) {
			this.getInvoiceItemGrid().down('button[itemId="edit"]').enable();
			this.getInvoiceItemGrid().down('button[itemId="del"]').enable();
		} else {
			this.getInvoiceItemGrid().down('button[itemId="edit"]').disable();
			this.getInvoiceItemGrid().down('button[itemId="del"]').disable();
		}
	},
	onPanelInvoiceBind : function(panel, invoice) {
		var invoiceForm = this.getInvoiceForm();
		var invoiceItemGrid = this.getInvoiceItemGrid();
		
		if(invoice.phantom == true) {
			panel.setActiveTab(invoiceForm);
			invoiceItemGrid.disable();
			this.getInvoiceForm().down('textfield[itemId="number"]').setReadOnly(true);
			
			this.getInvoiceForm().getForm().reset();
			this.getInvoiceForm().getForm().updateRecord(invoice);
		} else {
			invoiceItemGrid.enable();
			this.getInvoiceForm().getForm().reset();
			invoiceItemGrid.getStore().clearFilter(true);
			this.getInvoiceForm().down('textfield[itemId="number"]').setReadOnly(false);
			
			invoiceItemGrid.getStore().filter({
				property : 'invoice_id',
				value : invoice.get('id')
			});
		}
		
		this.getInvoiceForm().loadRecord(invoice);
	},
	onInvoiceFormSave : function(button) {
		var me = this;
		var invoicePanel = this.getInvoicePanel();
		
		var form = this.getInvoiceForm().getForm();
		form.updateRecord(form.getRecord());
		var phantom = form.getRecord().phantom;
		
		invoicePanel.mask('Zapisywanie faktury...');
		form.getRecord().save({
			success : function(invoice, action) {
				form.loadRecord(invoice);
				invoicePanel.unmask();
				invoicePanel.fireEvent('invoiceSave',invoice,phantom);
			},
			failure : function() {
				invoicePanel.unmask();
				console.err('Błąd zapisu kontrahenta')
				console.err(arguments);
			}
		});
	},
	onInvoiceSave : function(invoice, phantom) {
		this.getInvoiceItemGrid().enable();
		this.getInvoiceForm().down('textfield[itemId="number"]').setReadOnly(false);
	},
	onContractorSelectClick : function(button) {
		var picker = Ext.create('widget.webit_invoice_invoice_invoice_tab_invoice_panel_invoice_header_form_contractor_picker');
		picker.show();
	},
	onContractorPick : function(contractor) {
		var data = contractor.getData();
		delete data.id;
		this.getInvoiceForm().getForm().setValues({client : data});
	},
	onInvoiceItemAddClick : function(button) {
		var picker = Ext.create('widget.webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid_product_picker',{
			height : 500,
			layout : {
				type : 'vbox',
				align : 'stretch'
			}});
		picker.show();
	},
	onInvoiceItemEditClick : function(button) {
		var itemsGrid = this.getInvoiceItemGrid();
		var selection = itemsGrid.getSelectionModel().getSelection();
		if(selection.length > 0) {
			var picker = Ext.create('widget.webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid_product_picker',{
				title : 'Edytuj produkt',
				item : selection[0]
			});
			
			picker.show();	
		}
	},
	onInvoiceItemDelClick : function(button) {
		var itemsGrid = this.getInvoiceItemGrid();
		var invoice = this.getInvoicePanel().getBindedInvoice();
		
		var performDelete = function() {
			var item = itemsGrid.getSelectionModel().getLastSelected();
			
			if(item) {
				item.destroy({
					callback : function(record, operation) {
						if(operation.success) {
							var json = Ext.decode(operation.response.responseText);
							var invoiceHeader = json.misc['invoice'];
							
							itemsGrid.getStore().remove(item);
							itemsGrid.getStore().commitChanges();
							
							itemsGrid.fireEvent('itemchange',{
								action : 'remove',
								item : item,
								invoice : invoiceHeader
							});
						} else {
							console.error('Nie udało się usunąć pozycji faktury');
						}
					}
				});
			}
		};
		
		Ext.Msg.confirm('Usuwanie pozycji faktury','Czy na pewno chcesz usunąć wybraną pozycję faktury?',function(btn) {
    		if(btn == 'yes') {
    			performDelete();
    		}
    	});
	},
	onInvoiceItemPick : function(item) {
		var itemsGrid = this.getInvoiceItemGrid();
		var invoice = this.getInvoicePanel().getBindedInvoice();
		
		item.set('invoice_id',invoice.getId());
		
		var phantom = item.phantom;
		var store = this.getInvoiceItemGrid().getStore();
		item.save({
			success : function(record, operation) {
				var json = Ext.decode(operation.response.responseText);
				var invoiceHeader = json.misc['invoice'];
				if(phantom) {
					store.add(record);
					store.commitChanges(false);
				}
				
				itemsGrid.fireEvent('itemchange',{
					action : phantom ? 'add' : 'update',
					item : record,
					invoice : invoiceHeader
				});
				
				itemsGrid.getSelectionModel().deselectAll();
				itemsGrid.getSelectionModel().select(item);
			},
			failure : function() {
				
			}
		});
	},
	refs : [{
		ref : 'invoicePanel',
		selector : 'webit_invoice_invoice_invoice_tab_invoice_panel'
	},{
		ref : 'invoiceForm',
		selector : 'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_header_form'
	},{
		ref : 'invoiceItemGrid',
		selector : 'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid'
	}]
});