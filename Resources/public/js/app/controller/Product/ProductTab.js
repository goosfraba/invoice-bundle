Ext.define('WebitInvoice.controller.Product.ProductTab',{
	extend : 'Ext.app.Controller',
	init : function() {
		this.control({
			'webit_invoice_product_product_tab_product_grid[mode="grid"]' : {
				selectionchange : this.onProductGridSelectionChange
			},
			'webit_invoice_product_product_tab_product_grid[mode="grid"] button[itemId="add"]' : {
				click : this.onProductGridButtonAddClick
			},
			'webit_invoice_product_product_tab_product_grid[mode="grid"] button[itemId="del"]' : {
				click : this.onProductGridButtonDelClick
			},
			'webit_invoice_product_product_tab_product_panel' : {
				productSave : function(product, phantom) {
					var grid = this.getProductTab().down('webit_invoice_product_product_tab_product_grid[evidence="' + product.get('evidence') + '"]'); 
					if(phantom) {
						grid.getStore().insert(0,product);
					} else {
						var selected = grid.getSelectionModel().getLastSelected();
						// update selected with contractor
					}
				}
			}
		});
	},
	onProductGridSelectionChange : function(sm, selected, eopt) {
		var grid = this.getActiveGrid();
		if(selected.length == 1) {
			this.getProductPanel().bindProduct(selected[0].getId(),grid.remoteStoreName);
			grid.down('button[itemId="del"]').enable();
		} else {
			grid.down('button[itemId="del"]').disable();
			this.getProductPanel().bindProduct(null,grid.evidence);
		}
	},
	onProductGridButtonDelClick : function(button) {
		var performDelete = function() {
			var grid = button.up('webit_invoice_product_product_tab_product_grid[mode="grid"]');
			var record = grid.getSelectionModel().getLastSelected();
			if(record) {
				record.destroy({
					url : Routing.generate('webit_extjs_get_items',{store : grid.remoteStoreName}),
					callback : function(record, response) {
						if(response.success) {
							grid.getStore().remove(record);
						} else {
							console.error('Nie udało się usunąć produktu');
						}
					}
				});
			}
		};
		
		Ext.Msg.confirm('Usuwanie produktu','Czy na pewno chcesz usunąć wybrany produkt?',function(btn) {
    		if(btn == 'yes') {
    			performDelete();
    		}
    	});
	},
	onProductGridButtonAddClick : function(button) {
		var grid = button.up('webit_invoice_product_product_tab_product_grid[mode="grid"]');
		grid.getSelectionModel().deselectAll();
		
		this.getProductPanel().bindProduct(null,grid.evidence);
	},
	onProductSave : function(product, phantom) {
		var grid = this.getGridByRemoteStoreName(this.getProductPanel().remoteStoreName);
		if(phantom) {
			grid.getStore().insert(0,product);
		} else {
			var selected = grid.getSelectionModel().getLastSelected();
			// update selected with contractor
		}
	},
	refs : [{
		ref : 'productTab',
		selector : 'webit_invoice_product_product_tab'
	},{
		ref : 'productPanel',
		selector : 'webit_invoice_product_product_tab_product_panel'
	},{
		ref  :'productGrid',
		selector : 'webit_invoice_product_product_tab_product_grid[mode="grid"]'
	}],
	getActiveGrid : function() {
		return this.getProductTab().child('tabpanel').getActiveTab();
	},
	getGridByRemoteStoreName : function(remoteStoreName) {
		var grid = this.getContractorTab().down('webit_invoice_product_product_tab_product_grid[remoteStoreName="' + remoteStoreName + '"]');
		
		return grid;
	}
});