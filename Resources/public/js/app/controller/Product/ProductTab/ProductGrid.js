Ext.define('WebitInvoice.controller.Product.ProductTab.ProductGrid',{
	extend : 'Ext.app.Controller',
	init : function() {
		this.control({
			'webit_invoice_product_product_tab_product_grid' : {
				render : function(grid) {
					grid.getStore().load();
				}
			}
		});
	}
});