Ext.define('WebitInvoice.controller.Product.ProductTab.ProductPanel',{
	extend : 'Ext.app.Controller',
	init : function() {
		this.control({
			'webit_invoice_product_product_tab_product_panel' : {
				productBind : this.onPanelProductBind,
				afterrender : function(panel) {
					panel.bindProduct(null);
				}
			},
			'webit_invoice_product_product_tab_product_panel_product_form button[itemId="save"]' : {
				click : this.onProductFormSave
			}
		});
	},
	onPanelProductBind : function(panel, product) {
		// FIXME: to należy przenieść gdzieś indziej (powinno się to odbywać samo podczas loadRecord()
//		var combos = this.getProductForm().query('combo');
//		Ext.each(combos,function(combo) {
//			// FIXME: brzydki warunek, może da się napisać lepszy?
//			if(product.fields.containsKey(combo.getItemId()) && combo.getStore().indexOfId(product.get(combo.getItemId()).id) == -1) {
//				combo.getStore().insert(0,product.get(combo.getItemId()));
//			}
//		});
		
		this.getProductForm().loadRecord(product);
		if(product.phantom == true) {
			panel.setTitle('Nowy produkt');
		} else {
			panel.setTitle(product.get('name'));
		}
	},
	onProductFormSave : function(button) {
		var me = this;
		var productPanel = this.getProductPanel();
		
		var form = this.getProductForm().getForm();
		form.updateRecord(form.getRecord());
		var phantom = form.getRecord().phantom;
		
		productPanel.mask('Zapisywanie danych produktu...');

		form.getRecord().save({
			url : Routing.generate('webit_extjs_get_items',{store : productPanel.remoteStoreName}),
			success : function(product, action) {
				form.loadRecord(product);
				productPanel.unmask();
				productPanel.fireEvent('productSave',product,phantom);
			},
			failure : function() {
				productPanel.unmask();
				console.err('Błąd zapisu kontrahenta')
				console.err(arguments);
			}
		});
	},
	refs : [{
		ref : 'productPanel',
		selector : 'webit_invoice_product_product_tab_product_panel' 
	},{
		ref : 'productForm',
		selector : 'webit_invoice_product_product_tab_product_panel_product_form'
	}]
});