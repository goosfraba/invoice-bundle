Ext.define('WebitInvoice.model.Contractor.Contractor',{
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int',
		useNull : true
	},{
		name : 'name',
		type : 'string'
	},{
		name : 'vat_no',
		type : 'string'
	},{
		name : 'address',
		type : 'string'
	},{
		name : 'address_post',
		type : 'string'
	},{
		name : 'address_post_code',
		type : 'string'
	},{
		name : 'address_country',
		type : 'string'
	},{
		name : 'information',
		type : 'string'
	},{
		name : 'bank_account',
		type : 'string'
	},{
		name : 'email',
		type : 'string'
	}],
	hasMany : [{
		model : 'WebitInvoice.model.Contractor.User',
		name : 'users'
	}],
	proxy : {
		type : 'webitrest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector({store : 'webit_invoice.contractor_store'}),
		reader: {
        type: 'json',
        root: 'data',
        successProperty : 'success',
        totalProperty : 'total',
        idProperty : 'id'
    },
    writer : {
    	type : 'json',
    	writeAllFields : true,
    	allowSingle : false
    }
	}
});
