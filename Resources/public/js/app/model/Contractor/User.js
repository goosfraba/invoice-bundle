Ext.define('WebitInvoice.model.Contractor.User',{
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int'
	},{
		name : 'name',
		type : 'string'
	},{
		name : 'lastname',
		type : 'string'
	},{
		name : 'state',
		type : 'string'
	},{
		name : 'contractor_id',
		type : 'int'
	}],
	belongsTo : [{
		model : 'WebitInvoice.model.Contractor.Contractor',
		name : 'contractor'
	}]
});