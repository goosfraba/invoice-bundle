Ext.define('WebitInvoice.model.Invoice.Contractor',{
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int',
		useNull : true
	},{
		name : 'name',
		type : 'string'
	},{
		name : 'vat_no',
		type : 'string'
	},{
		name : 'address',
		type : 'string'
	},{
		name : 'address_post',
		type : 'string'
	},{
		name : 'address_post_code',
		type : 'string'
	},{
		name : 'address_country',
		type : 'string'
	},{
		name : 'information',
		type : 'string'
	},{
		name : 'bank_account',
		type : 'string'
	},{
		name : 'email',
		type : 'string'
	}]
});
