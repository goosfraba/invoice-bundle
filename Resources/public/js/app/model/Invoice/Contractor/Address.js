Ext.define('WebitInvoice.model.Invoice.Contractor.Address',{
    extend : 'Ext.data.Model',
    fields : [{
        name : 'address',
        type : 'string'
    },{
        name : 'post_code',
        type : 'string'
    },{
        name : 'post',
        type : 'string'
    },{
        name : 'country',
        type : 'string'
    }]
});
