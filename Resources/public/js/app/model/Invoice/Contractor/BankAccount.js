Ext.define('WebitInvoice.model.Invoice.Contractor.BankAccount',{
    extend : 'Ext.data.Model',
    fields : [{
        name : 'number',
        type : 'string'
    },{
        name : 'info',
        type : 'string'
    }]
});
