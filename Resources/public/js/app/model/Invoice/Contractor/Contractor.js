Ext.define('WebitInvoice.model.Invoice.Contractor.Contractor',{
    extend : 'Ext.data.Model',
    fields : [{
        name : 'name',
        type : 'string'
    },{
        name : 'address',
        model : 'WebitInvoice.model.Invoice.Contractor.Address'
    },{
        name : 'vat_no',
        model : 'WebitInvoice.model.Invoice.Contractor.VatNo'
    }]
});
