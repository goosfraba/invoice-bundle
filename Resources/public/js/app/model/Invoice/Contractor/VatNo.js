Ext.define('WebitInvoice.model.Invoice.Contractor.Supplier',{
    extend : 'WebitInvoice.model.Invoice.Contractor.Contractor',
    fields : [{
        name : 'bank_account',
        model: 'WebitInvoice.model.Invoice.Contractor.BankAccount'
    }]
});
