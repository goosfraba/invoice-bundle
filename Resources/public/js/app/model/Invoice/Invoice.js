Ext.define('WebitInvoice.model.Invoice.Invoice',{
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int',
		useNull : true
	},{
		name : 'type_id',
		type : 'int'
	},{
		name : 'type',
		persist : false
	},{
		name : 'number',
		type : 'string'
	},{
		name : 'issue_date',
		type : 'date',
		dateFormat : 'Y-m-d H:i:s'
	},{
		name : 'sale_date',
		type : 'date',
		dateFormat : 'Y-m-d H:i:s'
	},{
		name : 'payment_method',
		type : 'string'
	},{
		name : 'payment_date',
		type : 'date',
		dateFormat : 'Y-m-d H:i:s'
	},{
		name : 'paid_at',
		type : 'date',
		dateFormat : 'Y-m-d H:i:s',
		persist : false
	},{
		name : 'value_net',
		type : 'float',
		persist : false
	},{
		name : 'value_gross',
		type : 'float',
		persist : false
	},{
		name : 'information',
		type : 'string'
	},{
		name : 'revicer',
		type : 'string'
	},{
		name : 'issuer',
		type : 'string'
	},{
		name : 'client',
		model : 'WebitInvoice.model.Invoice.Contractor.Customer'
	}],
	proxy : {
		type : 'webitrest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector({store : 'webit_invoice.invoice_invoice_store'}),
		reader: {
			type: 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty : 'total',
			idProperty : 'id'
		},
		writer : {
			type : 'json',
			writeAllFields : true,
			allowSingle : false
		}
	}
});
