Ext.define('WebitInvoice.model.Invoice.InvoiceItem',{
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int',
		useNull : true
	},{
		name : 'name',
		type : 'string'
	},{
		name : 'unit',
		model : 'WebitInvoice.model.Invoice.Unit.Unit',
		persist : false
	},{
		name : 'unit_id',
		type : 'int'
	},{
		name : 'price',
		type : 'float'
	},{
		name : 'discount_price',
		type : 'float'
	},{
		name : 'tax_rate',
		model : 'WebitInvoice.model.Invoice.Tax.TaxRate',
		persist : false
	},{
		name : 'tax_rate_id',
		type : 'int'
	},{
		name : 'tax_percent',
		type : 'float'
	},{
		name : 'quantity',
		type : 'float'
	},{
		name : 'invoice',
		perist : false
	},{
		name : 'invoice_id',
		type : 'int'
	}],
	proxy : {
		type : 'webitrest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector({store : 'webit_invoice.invoice_invoice_item_store'}),
		reader: {
        type: 'json',
        root: 'data',
        successProperty : 'success',
        totalProperty : 'total',
        idProperty : 'id'
    },
    writer : {
    	xtype : 'json',
    	writeAllFields : true,
    	allowSingle : false
    }
	}
});
	