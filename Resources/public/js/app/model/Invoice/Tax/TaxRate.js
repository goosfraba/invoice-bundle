Ext.define('WebitInvoice.model.Invoice.Tax.TaxRate',{
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int',
		useNull : true
	},{
		name : 'name',
		type : 'string'
	},{
		name : 'label',
		type : 'string',
		persist : false
	},{
		name : 'value',
		type : 'float',
		persist : false
	},{
		name : 'ratio',
		type : 'float',
		perist : false
	}],
	hasMany : [{
		name : 'values',
		model : 'WebitInvoice.model.Invoice.Tax.TaxValue',
		foreignKey : 'tax_rate_id'
	}],
	proxy : {
		type : 'webitrest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector({store : 'webit_invoice.tax_tax_rate'}),
		reader: {
        type: 'json',
        root: 'data',
        successProperty : 'success',
        totalProperty : 'total',
        idProperty : 'id'
    },
    writer : {
    	xtype : 'json',
    	writeAllFields : true,
    	allowSingle : false
    }
	}
});
