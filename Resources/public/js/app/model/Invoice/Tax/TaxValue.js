Ext.define('WebitInvoice.model.Invoice.Tax.TaxValue',{
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int',
		useNull : true
	},{
		name : 'tax_rate_id',
		type : 'int'
	},{
		name : 'name',
		type : 'string'
	},{
		name : 'value',
		type : 'float'
	},{
		name : 'valid_to',
		type : 'date',
		dateFormat : 'Y-m-d H:i:s'
	}],
	proxy : {
		type : 'webitrest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector({store : 'webit_invoice.tax_tax_value'}),
		reader: {
        type: 'json',
        root: 'data',
        successProperty : 'success',
        totalProperty : 'total',
        idProperty : 'id'
    },
    writer : {
    	xtype : 'writer.json',
    	writeAllFields : true,
    	allowSingle : false,
    	encode : true,
    	root : 'data'
    }
	}
});
	