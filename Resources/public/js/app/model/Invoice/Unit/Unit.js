Ext.define('WebitInvoice.model.Invoice.Unit.Unit',{
	extend : 'Ext.data.Model',
	fields : [{
		name : 'id',
		type : 'int',
		useNull : true
	},{
		name : 'label',
		type : 'string'
	},{
		name : 'symbol',
		type : 'string'
	}],
	proxy : {
		type : 'webitrest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector({store : 'webit_invoice.unit_unit'}),
		reader: {
        type: 'json',
        root: 'data',
        successProperty : 'success',
        totalProperty : 'total',
        idProperty : 'id'
    },
    writer : {
    	type : 'json',
    	writeAllFields : true,
    	allowSingle : false
    }
	}
});
	