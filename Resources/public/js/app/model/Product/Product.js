Ext.define('WebitInvoice.model.Product.Product',{
	extend : 'Ext.data.Model',
	requires : [
		'WebitInvoice.model.Invoice.Unit.Unit',
		'WebitInvoice.model.Invoice.Tax.TaxRate'
	],
	fields : [{
		name : 'id',
		type : 'int',
		useNull : true
	},{
		name : 'name',
		type : 'string'
	},{
		name : 'price_mode',
		type : 'string'
	},{
		name : 'tax_rate',
		model : 'WebitInvoice.model.Invoice.Tax.TaxRate',
		persist : false
	},{
		name : 'tax_rate_id',
		type : 'int',
		useNull : true
	},{
		name : 'unit',
		model : 'WebitInvoice.model.Invoice.Unit.Unit',
		persist : false
	},{
		name : 'unit_id',
		type : 'int',
		useNull : true
	},{
		name : 'price',
		type : 'float'
	},{
		name : 'tax_ratio',
		type : 'float',
		perist: false
	},{
		name : 'tax_value',
		type : 'float',
		perist: false
	}],
	proxy : {
		type : 'rest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector({store : 'webit_invoice.product_product_store'}),
		reader: {
        type: 'json',
        root: 'data',
        successProperty : 'success',
        totalProperty : 'total',
        idProperty : 'id'
    },
    writer : {
    	type : 'json',
    	writeAllFields : true,
    	allowSingle : false
    }
	}
});
