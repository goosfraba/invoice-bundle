Ext.define('WebitInvoice.view.Contractor.ContractorTab',{
	extend : 'Ext.panel.Panel',
	alias : 'widget.webit_invoice_contractor_contractor_tab',
	requires : [
		'WebitInvoice.view.Contractor.ContractorTab.ContractorGrid',
		'WebitInvoice.view.Contractor.ContractorTab.ContractorPanel'
	],
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	border : false,
	title : 'Kontrahenci',
	initComponent : function() {
		this.items = [{
			xtype : 'tabpanel',
			tabPosition : 'bottom',
			flex : 1,
			items : [{
				xtype : 'webit_invoice_contractor_contractor_tab_contractor_grid',
				title : 'Pozostali',
				remoteStoreName : 'webit_invoice.contractor_store'
			},{
				xtype : 'webit_invoice_contractor_contractor_tab_contractor_grid',
				title : 'Sklep internetowy',
				remoteStoreName : 'webit_ecommerce_contractor.invoice_contractor_store'
			}]
		},{
			xtype : 'webit_invoice_contractor_contractor_tab_contractor_panel',
			width : 600
		}];
		
		this.callParent();
	}
});
