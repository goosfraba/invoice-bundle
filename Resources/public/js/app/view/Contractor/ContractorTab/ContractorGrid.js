Ext.define('WebitInvoice.view.Contractor.ContractorTab.ContractorGrid',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.webit_invoice_contractor_contractor_tab_contractor_grid',
	requires : ['WebitInvoice.model.Contractor.Contractor'],
	remoteStoreName : null,
	mode : 'grid',
	initComponent : function() {
		Ext.apply(this,{
			store : {
				xtype : 'store',
				model : 'WebitInvoice.model.Contractor.Contractor',
				pageSize : 50,
				remoteSort : true,
				remoteFilter : true,
				sorters : [{
					property : 'name',
					direction : 'ASC'
				}],
				proxy : {
					type : 'rest',
					appendId : false,
					url : Routing.generate('webit_extjs_get_items',{store : this.remoteStoreName}),
					reader: {
			        type: 'json',
			        root: 'data',
			        successProperty : 'success',
			        totalProperty : 'total',
			        idProperty : 'id'
			    }
				}
			}
		});
		
		this.columns = [{
			text : 'NIP',
			tooltip : 'Numer Identyfikacji Podatkowej',
			width : 150,
			dataIndex : 'vat_no'
		},{
			text : 'Nazwa',
			tooltip : 'Nazwa kontrahenta',
			flex : 1,
			dataIndex : 'name'
		},{
			text : 'Poczta',
			tooltip : 'Poczta',
			width : 150,
			dataIndex : 'address_post'
		},{
			text : 'Adres',
			tooltip : 'Adres kontrahenta',
			width : 200,
			dataIndex : 'address'
		}];
		
		this.dockedItems = [];
		if(this.mode == 'grid') {
			this.dockedItems.push({
				xtype: 'toolbar',
				dock: 'right',
				items: [{
					text: '+',
					itemId : 'add',
					tooltip : 'Dodaj kontrahenta'
				},{
					text : '-',
					itemId : 'del',
					disabled : true,
					tooltip : 'Usuń kontrahenta'
				}]
			});
		}
		
		this.dockedItems.push({
        xtype: 'pagingtoolbar',
        dock: 'bottom',
        displayInfo: true
    });
		
		this.callParent();

		this.down('pagingtoolbar').bind(this.getStore());
	}
});
