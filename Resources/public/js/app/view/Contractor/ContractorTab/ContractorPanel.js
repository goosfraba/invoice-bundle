Ext.define('WebitInvoice.view.Contractor.ContractorTab.ContractorPanel',{
	extend : 'Ext.tab.Panel',
	alias : 'widget.webit_invoice_contractor_contractor_tab_contractor_panel',
	requires : [
		'WebitInvoice.view.Contractor.ContractorTab.ContractorPanel.ContractorForm',
		'WebitInvoice.view.Contractor.ContractorTab.ContractorPanel.InvoiceGrid',
		'WebitInvoice.view.Contractor.ContractorTab.ContractorPanel.InfoHistoryGrid'
	],
	title : 'Kontrahent',
	contractor : null,
	remoteStoreName : null,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	initComponent : function() {
		this.addEvents('contractorBind','contractorSave');
		
		this.items = [{
			xtype : 'webit_invoice_contractor_contractor_tab_contractor_panel_contractor_form',
			title : 'Informacje',
			autoScroll: true
		},{
			xtype : 'webit_invoice_contractor_contractor_tab_contractor_panel_invoice_grid'
		}]
		this.callParent();
	},
	bindContractor : function(contractor, remoteStoreName) {
		var contractor_id;
		this.remoteStoreName = remoteStoreName || 'webit_invoice.contractor_store';
		
		if(Ext.isObject(contractor)) {
			contractor_id = contractor.getId();
		} else {
			contractor_id = contractor;
		}
		
		if(Ext.isEmpty(contractor_id) == false) {
			this.el.mask('Ładowanie danych kontrahenta');
			Ext.ModelManager.getModel('WebitInvoice.model.Contractor.Contractor').load(contractor_id,{
				callback : function(record) {
					this.contractor = record;
					this.fireEvent('contractorBind',this,this.contractor);
					this.el.unmask();
				},
				scope : this
			});	
		} else {
			this.contractor = Ext.create('WebitInvoice.model.Contractor.Contractor');
			
			this.fireEvent('contractorBind',this,this.contractor);
		}
	}
});
