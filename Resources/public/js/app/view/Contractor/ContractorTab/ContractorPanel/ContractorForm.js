Ext.define('WebitInvoice.view.Contractor.ContractorTab.ContractorPanel.ContractorForm',{
	extend : 'Ext.form.Panel',
	alias : 'widget.webit_invoice_contractor_contractor_tab_contractor_panel_contractor_form',
	frame : true,
	border : false,
	initComponent : function() {
		this.items = [{
			xtype : 'hidden',
			name : 'id'
		},{
			xtype : 'fieldset',
			title : 'Dane podstawowe',
			defaults : {
				anchor : '100%'
			},
			items : [{
				xtype : 'textfield',
				fieldLabel : 'Nazwa',
				name : 'name',
				allowBlank : false
			},{
				xtype : 'textfield',
				fieldLabel : 'NIP',
				name : 'vat_no',
				allowBlank : false,
				anchor : '',
				width : 250
			},{
				xtype : 'textfield',
				fieldLabel : 'Nr rachunku',
				name : 'bank_account'
			}]
		},{
			xtype : 'fieldset',
			defaults : {
				anchor : '100%'
			},
			title : 'Dane adresowe',
			items : [{
				xtype : 'textfield',
				fieldLabel : 'Adres',
				name : 'address',
				allowBlank : false
			},{
				xtype : 'textfield',
				fieldLabel : 'Kod pocztowy',
				name : 'address_post_code',
				anchor : '',
				width : 200,
				allowBlank : false
			},{
				xtype : 'textfield',
				fieldLabel : 'Poczta',
				name : 'address_post',
				anchor : '',
				width : 300,
				allowBlank : false
			},{
				xtype : 'textfield',
				fieldLabel : 'E-mail',
				name : 'email',
				anchor : '',
				width : 300
			}]
		},{
			xtype : 'fieldset',
			title : 'Informacje dodatkowe',
			items : [{
				xtype : 'textareafield',
				name : 'information',
				height : 100,
				anchor : '100%'
			}]
		}];
		
		this.bbar = ['->',
		{
			text : 'Zapisz',
			itemId : 'save'
		},{
			text : 'Anuluj',
			itemId : 'cancel'
		}];
		
		this.callParent();
	}
});
