Ext.define('WebitInvoice.view.Contractor.ContractorTab.ContractorPanel.InfoHistoryGrid',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.webit_invoice_contractor_contractor_tab_contractor_panel_info_history_grid',
	title : 'Historia zmian danych kontrahenta',
	border : false,
	initComponent : function() {
		this.columns = [{
			text : 'Data',
			tooltip : 'Data wpisu',
			width : 100
		},{
			text : 'Nazwa',
			tooltip : 'Nazwa kontrahenta',
			flex : 1
		},{
			text : 'NIP',
			tooltip : 'Numer Identyfikacji Podatkowej',
			width : 150
		}];
		
		this.callParent();
	}
});
