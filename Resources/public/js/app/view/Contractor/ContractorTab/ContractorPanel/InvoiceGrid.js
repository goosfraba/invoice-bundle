Ext.define('WebitInvoice.view.Contractor.ContractorTab.ContractorPanel.InvoiceGrid',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.webit_invoice_contractor_contractor_tab_contractor_panel_invoice_grid',
	title : 'Faktruy',
	border : false,
	initComponent : function() {
		this.columns = [{
			text : 'Data',
			tooltip : 'Data wystawienia',
			width : 100
		},{
			text : 'Typ',
			tooltip : 'Typ faktury',
			width : 50
		},{
			text : 'Numer',
			tooltip : 'Numer faktury',
			flex : 1
		},{
			text : 'Netto',
			tooltip : 'Wartość netto',
			width : 100
		},{
			text : 'Brutto',
			tooltip : 'Wartość brutto',
			width : 100
		}];
		
		this.dockedItems = [{
			xtype: 'toolbar',
			dock: 'right',
			items: [{
				text: 'p',
				itemId : 'show',
				tooltip : 'Wyświetl fakturę'
			}]
		}];
		
		this.callParent();
	}
});
