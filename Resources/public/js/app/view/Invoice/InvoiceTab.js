Ext.define('WebitInvoice.view.Invoice.InvoiceTab',{
	extend : 'Ext.panel.Panel',
	alias : 'widget.webit_invoice_invoice_invoice_tab',
	requires : [
		'WebitInvoice.view.Invoice.InvoiceTab.InvoiceGrid',
		'WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel'
	],
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	title : 'Faktury',
	initComponent : function() {
		this.items = [{
			xtype : 'webit_invoice_invoice_invoice_tab_invoice_grid',
			flex : 1
		},{
			xtype : 'webit_invoice_invoice_invoice_tab_invoice_panel',
			width : 700
		}];
		
		this.callParent();
	}
});
