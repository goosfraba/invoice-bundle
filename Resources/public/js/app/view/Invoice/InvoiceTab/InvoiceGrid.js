Ext.define('WebitInvoice.view.Invoice.InvoiceTab.InvoiceGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.webit_invoice_invoice_invoice_tab_invoice_grid',
    requires: ['WebitInvoice.model.Invoice.Invoice'],
    initComponent: function () {
        console.info('grid');
        Ext.apply(this, {
            store: {
                xtype: 'store',
                model: 'WebitInvoice.model.Invoice.Invoice',
                pageSize: 50,
                remoteSort: true,
                remoteFilter: true,
                sorters: [{
                    property: 'issue_date',
                    direction: 'DESC'
                }]
            },
            features: [{
                ftype: 'filters',
                encode: true,
                filters: [{
                    type: 'string',
                    dataIndex: 'number'
                }, {
                    type: 'string',
                    dataIndex: 'client'
                }, {
                    type: 'datetime',
                    dataIndex: 'issue_date'
                }]
            }]
        });

        this.columns = [{
            text: 'Data',
            tooltip: 'Data wystawienia',
            dataIndex: 'issue_date',
            width: 100,
            renderer: Ext.util.Format.dateRenderer('Y-m-d')
        }, {
            text: 'Rodzaj',
            tooltip: 'Rodzaj faktury',
            dataIndex: 'type_id',
            renderer: function (v, meta, r) {
                return r.get('type').symbol;
            },
            width: 60
        }, {
            text: 'Numer',
            tooltip: 'Numer faktury',
            dataIndex: 'number',
            width: 120
        }, {
            text: 'Kontrahent',
            tooltip: 'Kontrahent',
            dataIndex: 'client',
            flex: 1,
            renderer: function (v, meta, r) {
                return v.name;
            }
        }, {
            text: 'Netto',
            tooltip: 'Wartość netto',
            dataIndex: 'value_net',
            width: 90,
            renderer: Ext.util.Format.numberRenderer('0.0,0')
        }, {
            text: 'Brutto',
            tooltip: 'Wartość brutto',
            dataIndex: 'value_gross',
            width: 90,
            renderer: Ext.util.Format.numberRenderer('0.0,0')
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            dock: 'right',
            items: [{
                text: '+',
                itemId: 'add',
                tooltip: 'Dodaj fakturę'
            }, {
                text: '-',
                itemId: 'del',
                disabled: true,
                tooltip: 'Usuń fakturę'
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true
        }];

        this.callParent();

        this.down('pagingtoolbar').bind(this.getStore());
    }
});
