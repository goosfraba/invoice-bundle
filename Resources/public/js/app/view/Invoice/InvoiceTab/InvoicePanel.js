Ext.define('WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel',{
	extend : 'Ext.tab.Panel',
	alias : 'widget.webit_invoice_invoice_invoice_tab_invoice_panel',
	requires : [
		'WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceHeaderForm',
		'WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceItemGrid'
	],
	autoScroll : true,
	initComponent : function() {
		this.addEvents('invoiceBind','invoiceSave');
		
		this.items = [{
			xtype : 'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_header_form',
			title : 'Faktura',
			autoScroll: true
		},{
			xtype : 'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid',
			title : 'Produkty'
		}];
		
		this.callParent();
	},
	getBindedInvoice : function() {
		return this.invoice;
	},
	bindInvoice : function(invoice) {
		var invoice_id;
		
		if(Ext.isObject(invoice)) {
			invoice_id = invoice.getId();
		} else {
			invoice_id = invoice;
		}
		
		if(Ext.isEmpty(invoice_id) == false) {
			this.el.mask('Ładowanie danych faktury');
			Ext.ModelManager.getModel('WebitInvoice.model.Invoice.Invoice').load(invoice_id,{
				callback : function(record) {
					this.invoice = record;
					this.fireEvent('invoiceBind',this,this.invoice);
					this.el.unmask();
				},
				scope : this
			});	
		} else {
			this.invoice = Ext.create('WebitInvoice.model.Invoice.Invoice');
			
			this.fireEvent('invoiceBind',this,this.invoice);
		}
	}
});
