Ext.define('WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceHeaderForm',{
	extend : 'Ext.form.Panel',
	requires: [
		'WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceHeaderForm.ContractorPicker'
	],
	alias : 'widget.webit_invoice_invoice_invoice_tab_invoice_panel_invoice_header_form',
	frame : true,
	border : false,
	initComponent : function() {
		this.items = [{
			xtype : 'hidden',
			name : 'id'
		},{
			xtype : 'fieldset',
			title : 'Dane podstawowe',
			defaults : {
				frame : false,
				border : false
			},
			items : [{
				layout : 'hbox',
				defaults : {
					frame : true,
					border : false
				},
				items : [{
					flex : 5,
					layout : 'anchor',
					items : [{
						xtype : 'textfield',
						itemId : 'number',
						fieldLabel : 'Faktura numer',
						readOnly : true,
						name : 'number',
						anchor : '-10'
					}]
				},{
					flex : 4,
					layout : 'anchor',
					items : [{
						xtype : 'combo',
						queryMode : 'local',
						name : 'type_id',
						fieldLabel : 'Rodzaj faktury',
						labelAlign : 'right',
						anchor : '-5',
						store : {
							xtype : 'store',
							fields : [{
								name : 'id',
								type : 'int'
							},{
								name : 'label',
								type : 'string'
							}],
							data : WebitInvoice.data.Invoice.Type
						},
						value : 1,
						displayField : 'label',
						valueField : 'id',
						editable : false
					}]
				}]
			},{
				layout : 'hbox',
				defaults : {
					frame : true,
					border : false,
					flex : 1,
					layout : 'anchor'
				},
				items : [{
					items : [{
						xtype : 'datefield',
						name : 'issue_date',
						fieldLabel : 'Data wystawienia',
						anchor : '-5',
						format : 'Y-m-d',
						value : new Date()
					}]
				},{
					items : [{
						xtype : 'datefield',
						name : 'sale_date',
						labelAlign : 'right',
						fieldLabel : 'Data sprzedaży',
						format : 'Y-m-d',
						anchor : '-5',
						value : new Date()
					}]
				},{
					items : [{
						xtype : 'datefield',
						name : 'payment_date',
						labelAlign : 'right',
						fieldLabel : 'Termin płatności',
						format : 'Y-m-d',
						value : new Date(),
						anchor : '-5'
					}]
				}]
			},{
				layout : 'hbox',
				defaults : {
					frame : true,
					border : false,
					flex : 1,
					layout : 'anchor'
				},
				items : [{
						items : [{
							xtype : 'combo',
							queryMode : 'local',
							fieldLabel : 'Metoda płatności',
							name : 'payment_method',
							anchor : '-5',
							store : {
								xtype : 'store',
								fields : [{
									name : 'id',
									type : 'string'
								},{
									name : 'label',
									type : 'string'
								}],
								data : [{
									label : 'gotówka',
									id : 'cache'
								},{
									label : 'przelew',
									id : 'transfer'
								},{
									label : 'karta płatnicza',
									id : 'card'
								}]
							},
							value : 'cache',
							displayField : 'label',
							valueField : 'id',
							editable : false
						}]
				},{
					items : [{
						xtype : 'combo',
						queryMode : 'local',
						fieldLabel : 'Wystawił',
						labelAlign : 'right',
						name : 'issuer',
						itemId : 'issuer',
						allowBlank : false,
						anchor : '-5',
						store : {
							xtype : 'store',
							fields : [{
								name : 'id',
								type : 'int'
							},{
								name : 'label',
								type : 'string'
							}],
							data : [{
								id : 1,
								label : 'Paweł Gwizdała'
							}]
						},
						valueField : 'label',
						displayField : 'label',
						value : 'Paweł Gwizdała',
						editable : true,
						forceSelection : false
					}]
				}]
			},{
				layout : 'hbox',
				defaults : {
					frame : true,
					border : false,
					flex : 1
				},
				items : [{
					items : [{
						xtype : 'displayfield',
						name : 'value_net',
						fieldLabel : 'Wartość netto',
						renderer : Ext.util.Format.numberRenderer('0.0,0')
					}]
				},{
					items : [{
						xtype : 'displayfield',
						name : 'value_gross',
						fieldLabel : 'Wartość brutto',
						renderer : Ext.util.Format.numberRenderer('0.0,0')
					}]
				}]
			},{
				xtype : 'fieldset',
				title : 'Notatka',
				border : true,
				collapsible : true,
				collapsed : true,
				items : [{
					xtype : 'textarea',
					name : 'information',
					height : 100,
					anchor : '100%'
				}]
			}]
		},{
			xtype : 'fieldset',
			title : 'Kontrahent',
			defaults : {
				anchor : '100%'
			},
			items : [{
				xtype : 'button',
				text : 'Wybierz...',
				itemId : 'contractor-select'
			},{
				xtype : 'hidden',
				name : 'client_id'
			},{
				xtype : 'hidden',
				name : 'client.id'
			},{
				xtype : 'textfield',
				fieldLabel : 'Nazwa',
				name : 'client.name',
				allowBlank : false
			},{
				xtype : 'textfield',
				fieldLabel : 'NIP',
				name : 'client.vat_no',
				anchor : '',
				width : 250
			},{
				xtype : 'textfield',
				fieldLabel : 'Adres',
				name : 'client.address',
				allowBlank : false
			},{
				xtype : 'textfield',
				fieldLabel : 'Kod pocztowy',
				name : 'client.address_post_code',
				anchor : '',
				width : 200,
				allowBlank : false
			},{
				xtype : 'textfield',
				fieldLabel : 'Poczta',
				name : 'client.address_post',
				anchor : '',
				width : 300,
				allowBlank : false
			},{
				xtype : 'textfield',
				fieldLabel : 'Odebrał',
				width : 300
			}]
		}];
		
		this.bbar = [{
			text : 'Drukuj',
			itemId : 'print',
			handler : function() {
				Ext.Msg.alert('Drukuj','Tutaj będzie drukowanie faktury');
			}
		},{
			text : 'Wyślij',
			itemId : 'send',
			handler : function() {
				Ext.Msg.alert('Wyślij','Tutaj będzie wysyłanie faktury');
			}
		},{
			text : 'Duplikat',
			itemId : 'copy',
			handler : function() {
				Ext.Msg.alert('Duplikat','Tutaj będzie drukowanie duplikatu');
			}
		},{
			text : 'Korekta',
			itemId : 'correct',
			handler : function() {
				Ext.Msg.alert('Korekta','Tutaj będzie wystawianie korekty');
			}
		},'->',
		{
			text : 'Zapisz',
			itemId : 'save'
		},{
			text : 'Anuluj',
			itemId : 'cancel'
		}];
		
		this.callParent();
	}
});
