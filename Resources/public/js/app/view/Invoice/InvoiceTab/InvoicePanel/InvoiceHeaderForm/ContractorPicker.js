Ext.define('WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceHeaderForm.ContractorPicker',{
	extend : 'Ext.window.Window',
	alias : 'widget.webit_invoice_invoice_invoice_tab_invoice_panel_invoice_header_form_contractor_picker',
	frame : true,
	border : false,
	title : 'Wybierz kontrahenta',
	width : 800,
	height : 350,
	modal : true,
	layout : 'fit',
	initComponent : function() {
		this.addEvents('contractorPick');
		
		this.items = [{
			xtype : 'tabpanel',
			tabPosition : 'bottom',
			items : [{
				xtype : 'webit_invoice_contractor_contractor_tab_contractor_grid',
				title : 'Pozostali',
				remoteStoreName : 'webit_invoice.contractor_store',
				mode : 'picker'
			},{
				xtype : 'webit_invoice_contractor_contractor_tab_contractor_grid',
				title : 'Pozostali',
				remoteStoreName : 'webit_ecommerce_contractor.invoice_contractor_store',
				mode : 'picker'
			}]
		}];
		
		this.bbar = ['->',{
			text : 'Wybierz',
			itemId : 'select',
			scope : this,
			handler : function(btn) {
				var grid = this.down('tabpanel').getActiveTab();
				var contractor = grid.getSelectionModel().getLastSelected();
				if(contractor) {
					this.fireEvent('contractorPick',contractor);
					this.close();
				}
			}
		},{
			text : 'Anuluj',
			itemId : 'cancel',
			scope : this,
			handler : function() {
				this.close();
			}
		}];
		
		this.callParent();
	}
});
