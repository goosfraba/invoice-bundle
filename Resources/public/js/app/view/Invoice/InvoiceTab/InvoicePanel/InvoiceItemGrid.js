Ext.define('WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceItemGrid',{
	extend : 'Ext.grid.Panel',
	requires : [
		'WebitInvoice.model.Invoice.InvoiceItem',
		'WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceItemGrid.ProductPicker'
	],
	alias : 'widget.webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid',
	title : 'Produkty',
	border : false,
	initComponent : function() {
		this.addEvents('itemchange');
		
		this.columns = [{
			header : 'Nazwa',
			tooltip : 'Data wystawienia',
			dataIndex : 'name',
			flex : 1
		},{
			header : 'Jed.',
			tooltip : 'Jednostka sprzedaży',
			width : 50,
			dataIndex : 'unit',
			renderer : function(v, meta, r) {
				return v.symbol;
			}
		},{
			header : 'Ilość',
			tooltip : 'Ilość',
			dataIndex : 'quantity',
			width : 50,
			align : 'right'
		},{
			header : 'Cena jednostkowa netto',
			columns : [{
				header : 'Regularna',
				tooltip : 'Cena netto regularna',
				dataIndex : 'price',
				width : 70,
				align : 'right',
				renderer : Ext.util.Format.numberRenderer('0.0,0')
			},{
				header : 'Po rabacie',
				tooltip : 'Cena jednostkowa netto po rabacie',
				dataIndex : 'discount_price',
				width : 70,
				align : 'right',
				renderer : Ext.util.Format.numberRenderer('0.0,0')
			}]
		},{
			header : 'Wartość',
			columns : [{
				header : 'Netto',
				tooltip : 'Wartość netto',
				width : 70,
				align : 'right',
				renderer : function(v, meta, r) {
					var result = Math.round((r.get('discount_price') * r.get('quantity')) * Math.pow(10, 2)) / Math.pow(10, 2);
					return Ext.util.Format.number(result,'0.0,0');
				}
			},{
				header : 'Brutto',
				tooltip : 'Wartość brutto',
				width : 70,
				align : 'right',
				renderer : function(v, meta, r) {
					var netto = r.get('discount_price') * r.get('quantity');
					var vat = netto * (r.get('tax_percent') / 100)
					var result = Math.round((netto + vat) * Math.pow(10, 2)) / Math.pow(10, 2);
					return Ext.util.Format.number(result,'0.0,0');
				}
			}]
		},{
			header : 'VAT',
			columns : [{
				header : '%',
				tooltip : 'Stawka VAT',
				dataIndex : 'tax_percent',
				width : 50,
				align : 'right',
				renderer : Ext.util.Format.numberRenderer('0.0,0')
			},{
				header : 'Wartość',
				tooltip : 'Wartość VAT',
				width : 60,
				align : 'right',
				renderer : function(v, meta, r) {
					var netto = r.get('discount_price') * r.get('quantity');
					
					var result = Math.round((netto * (r.get('tax_percent') / 100)) * Math.pow(10, 2)) / Math.pow(10, 2);
					return Ext.util.Format.number(result,'0.0,0');
				}
			}]
		}];
		
		this.store = {
			model : 'WebitInvoice.model.Invoice.InvoiceItem',
			remoteSort : true,
			remoteFilter : true,
			sorters : [{
				property : 'id',
				direction : 'ASC'
			}],
			pageSize : 50
		};
		
		this.dockedItems = [{
			xtype: 'toolbar',
			dock: 'right',
			items: [{
				text: '+',
				itemId : 'add',
				tooltip : 'Dodaj produkt'
			},{
				text : 'e',
				itemId : 'edit',
				tooltip : 'Edytuj produkt'
			},{
				text : '-',
				itemId : 'del',
				disabled : true,
				tooltip : 'Usuń produkt'
			}]
		},{
			xtype : 'pagingtoolbar',
			dock : 'bottom'
		}];
		
		this.callParent();
		this.down('pagingtoolbar').bindStore(this.getStore());
	}
});
