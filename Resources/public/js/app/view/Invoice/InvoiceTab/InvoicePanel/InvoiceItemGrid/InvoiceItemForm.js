Ext.define('WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceItemGrid.InvoiceItemForm',{
	extend : 'Ext.form.Panel',
	alias : 'widget.webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid_invoice_item_form',
	frame : true,
	border : false,
	initComponent : function() {
		this.items = [{
			xtype : 'hidden',
			name : 'id'
		},{
			xtype : 'fieldcontainer',
			layout : 'hbox',
			defaults : {
				flex : 1
			},
			items : [{
				xtype : 'textfield',
				fieldLabel : 'Nazwa',
				name : 'name',
				allowBlank : false,
				flex : 3
			},{
				xtype : 'combo',
				labelAlign : 'right',
				queryMode : 'local',
				fieldLabel : 'Jednostka',
				labelWidth : 70,
				name : 'unit_id',
				itemId : 'unit',
				allowBlank : false,
				flex : 1,
				store : {
					xtype : 'store',
					fields : [{
						name : 'id',
						type : 'int'
					},{
						name : 'symbol',
						type : 'string'
					},{
						name : 'label',
						type : 'float'
					}],
					data : WebitInvoice.data.Invoice.Unit,
					proxy : {
						type : 'ajax',
						url : Routing.generate('webit_extjs_get_items',{store : 'webit_invoice.unit_unit_combo_store'}),
						reader : {
							type : 'json',
							root : 'data',
							idProperty : 'id',
							successProperty : 'success'
						}
					}
				},
				displayField : 'symbol',
				valueField : 'id'
			}]
		},{
			xtype : 'fieldcontainer',
			layout : 'hbox',
			defaults : {
				flex : 1
			},
			items : [{
				xtype : 'numberfield',
				name : 'price',
				itemId : 'price',
				fieldLabel : 'Cena',
				hideTrigger : true,
				listeners : {
					scope : this,
					blur : function(field) {
						var discountpriceNumberfield = this.down('field[itemId="discount_price"]');
						if(Ext.isEmpty(discountpriceNumberfield.getValue())) {
							discountpriceNumberfield.setValue(field.getValue());
						}
					}
				}
			},{
				xtype : 'numberfield',
				labelAlign : 'right',
				name : 'discount_price',
				itemId : 'discount_price',
				fieldLabel : 'Cena po rabacie',
				hideTrigger : true,
				listeners : {
					scope : this,
					change : function(field, newValue, oldValue) {
						this.calculateValues();
					}
				}
			},{
				xtype : 'combo',
				queryMode : 'local',
				labelAlign : 'right',
				fieldLabel : 'Stawka VAT',
				name : 'tax_rate_id',
				itemId : 'tax_rate',
				allowBlank : false,
				store : {
					xtype : 'store',
					fields : [{
						name : 'id',
						type : 'int'
					},{
						name : 'label',
						type : 'string'
					},{
						name : 'value',
						type : 'float'
					},{
						name : 'ratio',
						type : 'float'
					}],
					data : WebitInvoice.data.Invoice.Tax
				},
				displayField : 'label',
				valueField : 'id',
				editable : false,
				listeners : {
					scope : this,
					change : function(combo, newValue, oldValue) {
						this.calculateValues();
					}
				}
			},{
				xtype : 'numberfield',
				name : 'quantity',
				itemId : 'quantity',
				fieldLabel : 'Ilość',
				labelWidth : 70,
				labelAlign : 'right',
				hideTrigger : true,
				value : 1,
				listeners : {
					scope : this,
					change : function(field, newValue, oldValue) {
						this.calculateValues();
					}
				}
			}]
		},{
			xtype : 'fieldcontainer',
			layout : 'hbox',
			defaults : {
				flex : 1
			},
			items : [{
				xtype : 'displayfield',
				fieldLabel : 'Wartość',
				itemId : 'value_net'
			},{
				xtype : 'displayfield',
				fieldLabel : 'Wartość brutto',
				itemId : 'value_gross',
				labelAlign : 'right'
			},{
				xtype : 'displayfield',
				fieldLabel : 'Wartość VAT',
				itemId : 'vat_value',
				labelAlign : 'right',
				flex : 2
			}]
		}];
		
		this.callParent();
	},
	calculateValues : function() {
		var vatCombo = this.down('field[itemId="tax_rate"]');
		var vatValueField = this.down('field[itemId="vat_value"]');
		var valuenetField = this.down('field[itemId="value_net"]');
		var valuegrossField = this.down('field[itemId="value_gross"]');
		var quantityNumberfield = this.down('field[itemId="quantity"]');
		
		var priceNumberfield = this.down('field[itemId="price"]');
		var discountpriceNumberfield = this.down('field[itemId="discount_price"]');

		// obliczanie wartości netto
		var price = discountpriceNumberfield.getValue(); // do obliczeń biorę cenę po rabacie
		var value;
		
		var valuenet = 0;
		if(price > 0 && quantityNumberfield.getValue() > 0) {
			valuenet = price * quantityNumberfield.getValue();
		} else {
			valuenet = 0;
		}
		
		valuenet = Math.round(valuenet * Math.pow(10, 2)) / Math.pow(10, 2);
		valuenetField.setValue(Ext.util.Format.number(valuenet,'0.0,0'));

		// obliczanie wartości podatku
		var vat = vatCombo.getStore().getById(vatCombo.getValue());
		var	vatRatio = vat ? vat.get('value') / 100 : 1;
		var vatValue = valuenet * vatRatio;
		
		vatValue = Math.round(vatValue * Math.pow(10, 2)) / Math.pow(10, 2);
		vatValueField.setValue(Ext.util.Format.number(vatValue,'0.0,0'));
		
		// obliczanie wartości brutto
		var valueGross = Math.round((valuenet + vatValue) * Math.pow(10, 2)) / Math.pow(10, 2);
		valuegrossField.setValue(Ext.util.Format.number(valueGross,'0.0,0'));
	},
	getInvoiceItem : function() {
		var form = this.getForm();
		
		var vatCombo = this.down('combo[itemId="tax_rate"]');

		var values = form.getValues();
		values.tax_percent = vatCombo.findRecordByValue(values.tax_rate_id).get('value');
		
		var item = form.getRecord() ? form.getRecord() : Ext.create('WebitInvoice.model.Invoice.InvoiceItem');
		item.set(values);
		
		return item;
	}
});
