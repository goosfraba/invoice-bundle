Ext.define('WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceItemGrid.ProductPicker',{
	extend : 'Ext.window.Window',
	alias : 'widget.webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid_product_picker',
	requires : [
		'WebitInvoice.view.Invoice.InvoiceTab.InvoicePanel.InvoiceItemGrid.InvoiceItemForm'
	],
	frame : true,
	border : false,
	modal : true,
	title : 'Dodaj produkt',
	width : 800,
	item : null,
	initComponent : function() {
		this.addEvents('productPick');
		this.items = [];
		if(Ext.isEmpty(this.item)) {
			this.items.push({
				xtype : 'tabpanel',
				flex : 1,
				title : 'Wybierz produkt',
				tabPosition : 'bottom',
				items : [{
					xtype : 'webit_invoice_product_product_tab_product_grid',
					title : 'Pozostałe',
					remoteStoreName : 'webit_invoice.product_store',
					mode : 'picker'
				},{
					xtype : 'webit_invoice_product_product_tab_product_grid',
					title : 'Sklep internetowy',
					remoteStoreName : 'webit_ecommerce_contractor.invoice_product_store',
					mode : 'picker'
				}]
			});
		}
		
		this.items.push({
			xtype : 'webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid_invoice_item_form'
		});
		
		this.bbar = ['->',{
			text : 'Dodaj',
			itemId : 'add',
			hidden : Ext.isEmpty(this.item) == false,
			scope : this,
			handler : function() {
				this.performPick();
			}
		},{
			text : 'Dodaj i zamknij',
			itemId : 'add-exit',
			scope : this,
			hidden : Ext.isEmpty(this.item) == false,
			handler : function(button) {
				this.performPick();
				this.close();
			}
		},{
			text : 'Zapisz',
			itemId : 'save',
			scope : this,
			hidden : Ext.isEmpty(this.item),
			handler : function() {
				this.performPick();
				this.close();
			}
		},{
			text : 'Anuluj',
			itemId : 'cancel',
			scope : this,
			handler : function() {
				this.close();
			}
		}];
		
		this.callParent();
		
		var formPanel = this.down('webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid_invoice_item_form');
		if(Ext.isEmpty(this.item)) {
			var grids = this.query('webit_invoice_product_product_tab_product_grid');
			Ext.each(grids,function(grid) {
				grid.on('selectionchange',function(sm) {
					var selected = sm.getLastSelected();
					if(selected) {
						var data = selected.data;
						data.discount_price = data.price;
						delete data.id;
						
						formPanel.getForm().reset();
						var values = selected.getData();
						values.discount_price = values.price;
						formPanel.getForm().setValues(values);
					}
				});
			});
		} else {
			formPanel.getForm().loadRecord(this.item);
		}
	},
	performPick : function() {
		var formPanel = this.down('webit_invoice_invoice_invoice_tab_invoice_panel_invoice_item_grid_invoice_item_form');
		var item = formPanel.getInvoiceItem();
		
		this.fireEvent('productPick',item);
	}
});
