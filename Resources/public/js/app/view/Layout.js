Ext.define('WebitInvoice.view.Layout',{
	extend : 'Ext.tab.Panel',
	alias : 'widget.webit_invoice_layout',
	requires : [
		'WebitInvoice.view.Invoice.InvoiceTab',
		'WebitInvoice.view.Contractor.ContractorTab',
		'WebitInvoice.view.Product.ProductTab',
		'WebitInvoice.view.Config.ConfigTab'
	],
	title : 'Moduł faktur',
	initComponent : function() {
		this.items = [{
			xtype : 'webit_invoice_invoice_invoice_tab'
		},{
			xtype : 'webit_invoice_contractor_contractor_tab'
		},{
			xtype : 'webit_invoice_product_product_tab'
		},{
			xtype : 'webit_invoice_config_config_tab'
		}];
		
		this.callParent();
	}
});
