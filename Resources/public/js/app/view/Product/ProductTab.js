Ext.define('WebitInvoice.view.ProductTab',{
	extend : 'Ext.panel.Panel',
	alias : 'widget.webit_invoice_product_product_tab',
	requires : [
		'WebitInvoice.view.Product.ProductTab.ProductGrid',
		'WebitInvoice.view.Product.ProductTab.ProductPanel'
	],
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	title : 'Produkty',
	initComponent : function() {
		this.items = [{
			xtype : 'tabpanel',
			tabPosition : 'bottom',
			flex : 1,
			items : [{
				xtype : 'webit_invoice_product_product_tab_product_grid',
				title : 'Pozostałe',
				remoteStoreName : 'webit_invoice.product_store'
			},{
				xtype : 'webit_invoice_product_product_tab_product_grid',
				title : 'Sklep internetowy',
				remoteStoreName : 'webit_ecommerce_catalog.invoice_product_store'
			}]
		},{
			xtype : 'webit_invoice_product_product_tab_product_panel',
			width : 600
		}];
		
		this.callParent();
	}
});
