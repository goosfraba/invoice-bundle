Ext.define('WebitInvoice.view.Product.ProductTab.ProductGrid',{
	extend : 'Ext.grid.Panel',
	requires : ['WebitInvoice.model.Product.Product'],
	alias : 'widget.webit_invoice_product_product_tab_product_grid',
	remoteStoreName : null,
	mode : 'grid',
	initComponent : function() {
		Ext.apply(this,{
			store : {
				xtype : 'store',
				model : 'WebitInvoice.model.Product.Product',
				pageSize : 50,
				remoteSort : true,
				remoteFilter : true,
				sorters : [{
					property : 'name',
					direction : 'ASC'
				}],
				proxy : {
					type : 'rest',
					appendId : false,
					url : Routing.generate('webit_extjs_get_items',{store : this.remoteStoreName }),
					reader: {
			        type: 'json',
			        root: 'data',
			        successProperty : 'success',
			        totalProperty : 'total',
			        idProperty : 'id'
			    }
				}
			}
		});
		
		this.columns = [{
			text : 'Nazwa',
			tooltip : 'Nazwa produktu',
			flex : 1,
			dataIndex : 'name'
		},{
			text : 'Jednostka',
			tooltip : 'Jednostka sprzedaży',
			width : 80,
			dataIndex : 'unit',
			renderer : function(v, meta, r) {
				return r.get('unit').symbol;
			}
		},{
			text : 'Netto',
			tooltip : 'Cena netto',
			width : 120,
			dataIndex : 'price',
			renderer : function(v, meta, r) {
				if(r.get('price_mode') == 'net') {
					return Ext.util.Format.number(v,'0.0,0')
				} else {
					if(r.get('tax_ratio') != 0) {
						v /= r.get('tax_ratio');
						v = Math.round(v * Math.pow(10, 2)) / Math.pow(10, 2);
					}
					
					return Ext.util.Format.number(v,'0.0,0');
				}
			}
		},{
			text : 'Brutto',
			tooltip : 'Cena brutto',
			width : 120,
			dataIndex : 'price',
			renderer : function(v, meta, r) {
				if(r.get('price_mode') == 'gross') {
					return Ext.util.Format.number(v,'0.0,0')
				} else {
					if(r.get('tax_ratio') != 0) {
						v *= r.get('tax_ratio');
						v = Math.round(v * Math.pow(10, 2)) / Math.pow(10, 2);
					}
					
					return Ext.util.Format.number(v,'0.0,0');
				}
			}
		},{
			text : 'VAT',
			tooltip : 'Stawka VAT',
			width : 80,
			renderer : function(v, meta, r) {
				return r.get('tax_value') + '%';
			}
		}];
		this.dockedItems = [];
		if(this.mode == 'grid') {
			this.dockedItems.push({
				xtype: 'toolbar',
				dock: 'right',
				items: [{
					text: '+',
					itemId : 'add',
					tooltip : 'Dodaj produkt'
				},{
					text : '-',
					itemId : 'del',
					disabled : true,
					tooltip : 'Usuń produkt'
				}]
			});
		};
		
		this.dockedItems.push({
        xtype: 'pagingtoolbar',
        dock: 'bottom',
        displayInfo: true
    });
		
		this.callParent();
		
		this.down('pagingtoolbar').bind(this.getStore());
	}
});
