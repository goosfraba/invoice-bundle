Ext.define('WebitInvoice.view.Product.ProductTab.ProductPanel',{
	extend : 'Ext.panel.Panel',
	requires : [
		'WebitInvoice.view.Product.ProductTab.ProductPanel.ProductForm'
	],
	alias : 'widget.webit_invoice_product_product_tab_product_panel',
	title : 'Produkt',
	product : null,
	initComponent : function() {
		this.addEvents('productBind','productSave');
		
		this.items = [{
			xtype : 'webit_invoice_product_product_tab_product_panel_product_form',
			autoScroll: true
		}];
		this.callParent();
	},
	bindProduct : function(product, remoteStoreName) {
		var product_id;
		this.remoteStoreName = remoteStoreName || 'webit_invoice.product_store';
		if(Ext.isObject(product)) {
			product_id = product.getId();
		} else {
			product_id = product;
		}
		
		if(Ext.isEmpty(product_id) == false) {
			this.el.mask('Ładowanie danych produktu');
			Ext.ModelManager.getModel('WebitInvoice.model.Product.Product').load(product_id,{
				callback : function(record) {
					this.product = record;
					this.fireEvent('productBind',this,this.product);
					this.el.unmask();
				},
				scope : this
			});	
		} else {
			this.product = Ext.create('WebitInvoice.model.Product.Product',{
				price_mode : 'net',
				// FIXME: pobieranie domyślnej wartości
				tax_rate_id : 1,
				tax_rate : {
					id : 1,
					label : 'A (23%)',
					value : 23,
					ratio : 1.23
				},
				// FIXME: skąd wiesz, że takie coś istnieje?
				unit_id : 1,
				unit : {
					id : 1,
					label : 'Sztuka',
					symbol : 'szt.'
				}
			});

			this.fireEvent('productBind',this,this.product);
		}
	}
});
