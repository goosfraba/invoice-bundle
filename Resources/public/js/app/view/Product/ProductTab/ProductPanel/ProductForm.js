Ext.define('WebitInvoice.view.Product.ProductTab.ProductPanel.ProductForm',{
	extend : 'Ext.form.Panel',
	alias : 'widget.webit_invoice_product_product_tab_product_panel_product_form',
	frame : true,
	border : false,
	initComponent : function() {
		this.items = [{
			xtype : 'hidden',
			name : 'id'
		},{
			xtype : 'fieldset',
			title : 'Dane podstawowe',
			items : [{
				xtype : 'textfield',
				fieldLabel : 'Nazwa',
				name : 'name',
				allowBlank : false,
				anchor : '100%'
			},{
				xtype : 'combo',
				queryMode : 'local',
				fieldLabel : 'Jednostka sprz.',
				name : 'unit_id',
				itemId : 'unit',
				allowBlank : false,
				width : 250,
				store : {
					xtype : 'store',
					fields : [{
						name : 'id',
						type : 'int'
					},{
						name : 'symbol',
						type : 'string'
					},{
						name : 'label',
						type : 'float'
					}],
//					data : WebitInvoice.data.Invoice.Unit,
					proxy : {
						type : 'ajax',
						url : Routing.generate('webit_extjs_get_items',{store : 'webit_invoice.unit_unit_combo_store'}),
						reader : {
							type : 'json',
							root : 'data',
							idProperty : 'id',
							successProperty : 'success'
						}
					}
				},
				displayField : 'symbol',
				valueField : 'id'
			},{
				xtype : 'combo',
				queryMode : 'local',
				fieldLabel : 'Stawka VAT',
				name : 'tax_rate_id',
				itemId : 'tax_rate',
				allowBlank : false,
				width : 250,
				store : {
					xtype : 'store',
					fields : [{
						name : 'id',
						type : 'int'
					},{
						name : 'name',
						type : 'string'
					},{
						name : 'label',
						type : 'string'
					},{
						name : 'value',
						type : 'float'
					},{
						name : 'ratio',
						type : 'float'
					}]
//					data : WebitInvoice.data.Invoice.Tax
				},
				displayField : 'label',
				valueField : 'id',
				editable : false,
				listeners : {
					scope : this,
					change : function(combo, newValue, oldValue) {
						this.calculatePrice();
					}
				}
			},{
				xtype : 'combo',
				queryMode : 'local',
				fieldLabel : 'Ustaw cenę',
				name : 'price_mode',
				itemId : 'price_mode',
				allowBlank : false,
				width : 200,
				store : {
					xtype : 'store',
					fields : [{
						name : 'label',
						type : 'string'
					},{
						name : 'value',
						type : 'string'
					}],
					data : [{
						label : 'Netto',
						value : 'net'
					},{
						label : 'Brutto',
						value : 'gross'
					}]
				},
				displayField : 'label',
				valueField : 'value',
				editable : false,
				value : 'net',
				listeners : {
					scope : this,
					change : function(combo, newValue, oldValue) {
						this.calculatePrice();
						this.togglePriceLabel();
					}
				}
			},{
				xtype : 'numberfield',
				allowBlank : false,
				fieldLabel : 'Cena netto',
				hideTrigger : true,
				name : 'price',
				itemId : 'price',
				width : 250,
				decimalSeparator : ',',
				listeners : {
					scope : this,
					change : function(field, newValue, oldValue) {
						this.calculatePrice();
					}
				}
			},{
				xtype : 'displayfield',
				fieldLabel : 'Cena brutto',
				itemId : 'displayPrice',
				value : '0,00'
			}]
		}];
		
		this.bbar = ['->',
		{
			text : 'Zapisz',
			itemId : 'save'
		},{
			text : 'Anuluj',
			itemId : 'cancel'
		}];
		
		this.callParent();
	},
	calculatePrice : function() {
		var vatCombo = this.down('field[itemId="tax_rate"]');
		var priceModeField = this.down('field[itemId="price_mode"]');
		var priceNumberfield = this.down('field[itemId="price"]');
		var displayPriceDisplayfield = this.down('field[itemId="displayPrice"]');
		
		var price = Ext.isEmpty(priceNumberfield.getValue()) ? 0 : priceNumberfield.getValue();
		var resultPrice, vatValue;
		var vat = vatCombo.getStore().getById(vatCombo.getValue());
		
		if(vat) { 
			vatRatio = vat.get('ratio');
			
			if(priceModeField.getValue() == 'net') {
				resultPrice = price * vatRatio;	
			} else {
				resultPrice = price / vatRatio;
			}
			
			resultPrice = Math.round(resultPrice * Math.pow(10, 2)) / Math.pow(10, 2);		
			displayPriceDisplayfield.setValue(Ext.util.Format.number(resultPrice,'0.0,0'));
		}
	},
	togglePriceLabel : function() {
		var priceNumberfield = this.down('field[itemId="price"]');
		var displayPriceDisplayfield = this.down('field[itemId="displayPrice"]');
		
		var priceLabel, displayPriceLabel;
		priceLabel = priceNumberfield.getFieldLabel();
		displayPriceLabel = displayPriceDisplayfield.getFieldLabel();
		
		priceNumberfield.setFieldLabel(displayPriceLabel);
		displayPriceDisplayfield.setFieldLabel(priceLabel);
	}
});
