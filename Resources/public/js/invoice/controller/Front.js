Ext.define('WebitInvoice.controller.Front',{
	extend: 'Ext.app.Controller',
	requires: [
		'WebitInvoice.util.Serializer'
	],
	models: [
		'WebitInvoice.model.contractor.Contractor',
		'WebitInvoice.model.contractor.ContractorCurrent',
		'WebitInvoice.model.contractor.ContractorVersion',
		'WebitInvoice.model.contractor.User',
		'WebitInvoice.model.invoice.Invoice',
		'WebitInvoice.model.invoice.InvoiceDataPrice',
		'WebitInvoice.model.invoice.InvoiceItem',
		'WebitInvoice.model.invoice.InvoicePrice',
		'WebitInvoice.model.invoice.JoinedInvoice',
		'WebitInvoice.model.invoice.PaymentMethod',
		'WebitInvoice.model.invoice.Type',
		'WebitInvoice.model.product.Product',
		'WebitInvoice.model.product.ProductPrice'
	],
	views: [
		'WebitInvoice.view.invoice.InvoiceGrid'
	],
	controllers: [
		'WebitInvoice.controller.InvoiceGrid'
	],
	registerTaxRateStore: function(data) {
		Ext.create('Ext.data.Store',{
			model: 'WebitAccountingCommon.model.Vat.VatRate',
			data: data,
			storeId: 'vat_rates'
		});
	},
	registerUnitStore: function(data) {
		Ext.create('Ext.data.Store',{
			model: 'WebitCommonUnit.model.Unit',
			data: data,
			storeId: 'units'
		});
	}
});
