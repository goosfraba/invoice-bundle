Ext.define('WebitInvoice.controller.InvoiceGrid',{
	extend: 'Ext.app.Controller',
	init: function() {
		this.control({
			'webit_invoice_invoice_grid': {
				render: this.onInvoiceGridRender,
				selectionchange: this.onInvoiceGridSelectionChange
			}
		});
	},
	onInvoiceGridRender: function(grid) {
		this.registerButtonHandlers(grid);
		grid.getStore().load();
	},
	onInvoiceGridSelectionChange: function(sm, sel) {
		var grid = sm.view.up('grid');
		this.toggleGridButtons(grid, sel);
	},
	registerButtonHandlers: function(grid) {
		var buttons = grid.down('toolbar[itemId="right"]').query('button');
		
		Ext.each(buttons, function(button, i) {
			var fncName = button.getItemId()+'ButtonHandler';
			button.setHandler(this[fncName]);
		},this);
	},
	toggleGridButtons: function(grid, sel) {
		// var delButton = grid.down('toolbar[itemId="right"]').down('button[itemId="delete"]');
		// delButton.setDisabled(sel.length == 0);

		var printButton = grid.down('toolbar[itemId="right"]').down('button[itemId="print"]');
		printButton.setDisabled(sel.length == 0);
		
		// var sendButton = grid.down('toolbar[itemId="right"]').down('button[itemId="send"]');
		// sendButton.setDisabled(sel.length == 0);

        var printDuplicateButton = grid.down('toolbar[itemId="right"]').down('button[itemId="printDuplicate"]');
        printDuplicateButton.setDisabled(sel.length != 1);
	},
	addButtonHandler: function(btn) {
		Ext.Msg.alert('Dodawanie faktur','Funkcja niedostępna.');
	},
	deleteButtonHandler: function(btn) {
		Ext.Msg.alert('Usuwanie faktur','Funkcja niedostępna.');
	},
	printButtonHandler: function(btn) {
		var sel = btn.up('grid').getSelectionModel().getSelection();
		var invoices = [];
		Ext.each(sel, function(invoice, i) {
			invoices.push({id: invoice.get('id')});
		});
		
		var url = Routing.generate('webit_invoice_admin_print',{invoices: Ext.encode(invoices)});
		window.open(url);
	},
    printDuplicateButtonHandler: function(btn) {
        var sel = btn.up('grid').getSelectionModel().getSelection();
        var id = sel[0].get('id');

        var url = Routing.generate('webit_invoice_admin_print_duplicate', {id: id});
        window.open(url);
    },
	sendButtonHandler: function(btn) {
		Ext.Msg.alert('Wysyłanie faktur','Funkcja niedostępna.');
	}
});
