Ext.define('WebitInvoice.model.contractor.Contractor',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'integer',
		useNull: true
	},{
		name: 'name',
		type: 'string'
	},{
		name: 'vat_no',
		type: 'string'
	},{
		name: 'vat_no_canonical',
		type: 'string',
		persist: false
	},{
		name: 'address',
		type: 'string'
	},{
		name: 'address_post',
		type: 'string'
	},{
		name: 'address_post_code',
		type: 'string'
	},{
		name: 'address_country',
		type: 'string'
	},{
		name: 'information',
		type: 'string'
	},{
		name: 'bank_account',
		type: 'string'
	},{
		name: 'bank_account_canonical',
		type: 'string'
	},{
		name: 'bank_account_info',
		type: 'string'
	},{
		name: 'email',
		type: 'string'
	},{
		name: 'phone_no',
		type: 'string'
	},{
		name: 'supplier',
		type: 'bool',
		defaultValue: false
	},{
		name: 'updated_at',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s',
		persist: false
	},{
		name: 'users',
		model: 'WebitInvoice.model.contractor.User',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer(),
		persist: false
	},{
		name: 'default_user',
		model: 'WebitInvoice.model.contractor.User',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'buy_invoices',
		model: 'WebitInvoice.model.invoice.Invoice',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer(),
		persist: false
	},{
		name: 'sell_invoices',
		model: 'WebitInvoice.model.invoice.Invoice',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer(),
		persist: false
	}]
});
