Ext.define('WebitInvoice.model.contractor.ContractorCurrent',{
	extend: 'WebitInvoice.model.contractor.Contractor',
	fields: [{
		name: 'contractor_versions',
		model: 'WebitInvoice.model.contractor.ContractorVersion',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer(),
		persist: false
	}]
});
