Ext.define('WebitInvoice.model.contractor.ContractorVersion',{
	extend: 'WebitInvoice.model.contractor.Contractor',
	fields: [{
		name: 'contractor_current',
		model: 'WebitInvoice.model.contractor.ContractorCurrent',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer(),
		persist: false
	}]
});
