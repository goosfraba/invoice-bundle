Ext.define('WebitInvoice.model.contractor.User',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'integer',
		useNull: true
	},{
		name: 'name',
		type: 'string'
	},{
		name: 'lastname',
		type: 'string'
	},{
		name: 'state',
		type: 'string'
	},{
		name: 'contractor',
		model: 'WebitInvoice.model.contractor.Contractor',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	}]
});
