Ext.define('WebitInvoice.model.invoice.Invoice',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'integer',
		useNull: true
	},{
		name: 'type',
		model: 'WebitInvoice.model.invoice.Type',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'value',
		model: 'WebitInvoice.model.invoice.InvoicePrice',
		serialize: WebitInvoice.util.Serializer.modelSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'taxed_value',
		model: 'WebitInvoice.model.invoice.InvoicePrice',
		serialize: WebitInvoice.util.Serializer.modelSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'number',
		type: 'string'
	},{
		name: 'issue_date',
		type: 'date',
		dateFromat: 'Y-m-d H:i:s'
	},{
		name: 'sale_date',
		type: 'date',
		dateFromat: 'Y-m-d H:i:s'
	},{
		name: 'payment_method',
		model: 'WebitInvoice.model.invoice.PaymentMethod',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'due_date',
		type: 'date',
		dateFromat: 'Y-m-d H:i:s'
	},{
		name: 'paid_at',
		type: 'date',
		dateFromat: 'Y-m-d H:i:s'
	},{
		name: 'client',
		model: 'WebitInvoice.model.contractor.Contractor',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'seller',
		serialize: '',
		model: 'WebitInvoice.model.contractor.Contractor',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'issuer',
		type: 'string'
	},{
		name: 'reciver',
		type: 'string'
	},{
		name: 'joined_invoices',
		model: 'WebitInvoice.model.invoice.JoinedInvoice',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'note',
		type: 'string'
	},{
		name: 'internal_note',
		type: 'string'
	},{
		name: 'token',
		type: 'string'
	},{
		name: 'items',
		type: 'WebitInvoice.model.invoice.InvoiceItem',
//		serialize: WebitInvoice.util.Serializer.idSerializer(),
//		convert: WebitInvoice.util.Serializer.createItemDeserializer(),
		persist: false
	},{
		name: 'payment_instructions',
		model: 'WebitPaymentJmsCore.model.PaymentInstruction',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer(),
		persist: false
	},{
		name: 'created_at',
		type: 'date',
		dateFromat: 'Y-m-d H:i:s'
	},{
		name: 'updated_at',
		type: 'date',
		dateFromat: 'Y-m-d H:i:s'
	},{
		name: 'printed_at',
		type: 'date',
		dateFromat: 'Y-m-d H:i:s'
	},{
		name: 'print_filename',
		type: 'string'
	},{
		name: 'url',
		type: 'string'
	},{
        name: 'wfirma_id',
        type: 'string',
		readonly: true
    }],
	proxy: {
		type: 'webitrest',
		appendId: false,
		urlSelector: Webit.data.proxy.StoreUrlSelector('webit_invoice.invoice_store'),
		reader: {
			type: 'json',
			root: 'data',
			idProperty: 'id'
		},
		writer: {
			type: 'json',
			writeAllFields: true,
			allowSingle: false
		}
	}
});
