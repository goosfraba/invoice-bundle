Ext.define('WebitInvoice.model.invoice.InvoiceDataPrice',{
	extend: 'WebitAccountingCommon.model.Price.TaxedPrice',
	fields: [{
		name: 'vat_value',
		type: 'float',
		useNull: true
	}]
});
