Ext.define('WebitInvoice.model.invoice.InvoiceItem',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'integer',
		useNull: true
	},{
		name: 'name',
		type: 'string'
	},{
		name: 'description',
		type: 'string'
	},{
		name: 'unit_id',
		type: 'string'
	},{
		name: 'unit',
		model: 'WebitCommonUnit.model.Unit',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.storeItemDeserializer(['units'],'id',false)
	},{
		name: 'price',
		model: 'WebitInvoice.model.invoice.InvoiceDataPrice',
		serialize: WebitInvoice.util.Serializer.modelSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'discount_price',
		model: 'WebitInvoice.model.invoice.InvoiceDataPrice',
		serialize: WebitInvoice.util.Serializer.modelSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'quantity',
		type: 'float',
		useNull: true,
		defaultValue: 1
	},{
		name: 'invoice',
		model: 'WebitInvoice.model.invoice.Invoice',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'source_item_id',
		type: 'string'
	},{
		name: 'source_item_provider',
		type: 'string'
	}]
});
