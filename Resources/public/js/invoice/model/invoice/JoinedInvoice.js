Ext.define('WebitInvoice.model.invoice.JoinedInvoice',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'integer',
		useNull: true
	},{
		name: 'master_invoice',
		model: 'WebitInvoice.model.invoice.Invoice',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'dest_invoice',
		model: 'WebitInvoice.model.invoice.Invoice',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer()
	},{
		name: 'type',
		type: 'string'
	}]
});
