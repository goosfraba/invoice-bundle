Ext.define('WebitInvoice.model.invoice.PaymentMethod',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'integer',
		useNull: true
	},{
		name: 'code',
		type: 'string'
	},{
		name: 'label',
		type: 'string'
	},{
		name: 'due_date_offset',
		type: 'integer',
		defaultValue: 0
	}]
});
