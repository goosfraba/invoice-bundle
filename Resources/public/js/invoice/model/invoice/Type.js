Ext.define('WebitInvoice.model.invoice.Type',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'integer',
		useNull: true
	},{
		name: 'label',
		type: 'string'
	},{
		name: 'code',
		type: 'string'
	},{
		name: 'symbol',
		type: 'string'
	},{
		name: 'prefix',
		type: 'string'
	},{
		name: 'number_length',
		type: 'integer',
		useNull: true
	},{
		name: 'print_on_fly',
		type: 'boolean',
		defaultValue: false
	},{
		name: 'invoices',
		model: 'WebitInvoice.model.invoice.Invoice',
		serialize: WebitInvoice.util.Serializer.idSerializer(),
		convert: WebitInvoice.util.Serializer.createItemDeserializer(),
		persist: false
	}]
});
