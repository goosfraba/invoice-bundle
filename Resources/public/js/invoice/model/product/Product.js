Ext.define('WebitInvoice.model.product.Product',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id',
		type: 'integer',
		useNull: true
	},{
		name: 'name',
		type: 'string'
	},{
		name: 'unit_id',
		type: 'string'
	},{
		name: 'unit',
		model: 'WebitCommonUnit.model.Unit',
		serialize: '',
		convert: ''
	},{
		name: 'price',
		model: ''
	}]
});
