Ext.define('WebitInvoice.model.product.ProductPrice',{
	extend: 'WebitAccountingCommon.model.Price.TaxedPrice',
	fields: [{
		name: 'vat_value',
		type: 'float',
		useNull: true
	}]
});
