Ext.define('WebitInvoice.view.invoice.InvoiceGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.webit_invoice_invoice_grid',
    initComponent: function () {
        Ext.apply(this, {
            store: Ext.create('Ext.data.Store', {
                model: 'WebitInvoice.model.invoice.Invoice',
                remoteSort: true,
                pageSize: 50,
                sorters: [{
                    property: 'issue_date',
                    direction: 'DESC'
                }]
            }),
            features: [{
                ftype: 'filters',
                encode: true,
                filters: [{
                    type: 'string',
                    dataIndex: 'number'
                }, {
                    type: 'string',
                    dataIndex: 'client'
                }, {
                    type: 'date',
                    dataIndex: 'issue_date',
                    dateFormat: 'Y-m-d'
                }]
            }],
            selModel: {
                mode: 'MULTI'
            },
            columns: [{
                header: 'Data',
                dataIndex: 'issue_date',
                renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s'),
                width: 120
            }, {
                header: 'Typ',
                dataIndex: 'type',
                renderer: function (v, meta, r) {
                    return r.get('type').get('symbol');
                },
                width: 50
            }, {
                header: 'Numer',
                dataIndex: 'number',
                width: 130
            }, {
                header: 'Klient',
                dataIndex: 'client',
                renderer: function (v, meta, r) {
                    return v ? v.get('name') : '';
                },
                minWidth: 150,
                flex: 1
            }, {
                header: 'Netto',
                dataIndex: 'value',
                width: 120,
                renderer: function (v, meta, r) {
                    return Ext.util.Format.number(v.get('value'), '0,00/i')
                }
            }, {
                header: 'Brutto',
                dataIndex: 'taxed_value',
                width: 120,
                renderer: function (v, meta, r) {
                    return Ext.util.Format.number(v.get('value'), '0,00/i')
                }
            }, {
                header: 'wFirma',
                dataIndex: 'wfirma_id',
                width: 80,
                sortable: false
            }],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'right',
                itemId: 'right',
                items: [{
                    iconCls: 'fam-silk-add',
                    itemId: 'add',
                    disabled: true,
                    tooltip: 'Nowa faktura'
                }, {
                    iconCls: 'fam-silk-delete',
                    itemId: 'delete',
                    disabled: true,
                    tooltip: 'Usuń wybrane faktury'
                }, {
                    iconCls: 'fam-silk-printer',
                    itemId: 'print',
                    disabled: true,
                    tooltip: 'Drukuj wybrane faktury'
                }, {
                    iconCls: 'fam-silk-printer_add',
                    itemId: 'printDuplicate',
                    disabled: true,
                    tooltip: 'Drukuj duplikat'
                }, {
                    iconCls: 'fam-silk-email_go',
                    itemId: 'send',
                    disabled: true,
                    tooltip: 'Wyślij wybrane faktury'
                }]
            }],
            bbar: {
                xtype: 'pagingtoolbar'
            }
        });

        this.callParent();

        this.down('pagingtoolbar').bindStore(this.getStore());
    }
});