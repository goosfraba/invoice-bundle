<?php
namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity;
use Doctrine\ORM\EntityManager;
use Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorVersion;
use Webit\Tools\Object\ObjectUpdater;

class ContractorManager implements ContractorManagerInterface
{
    /**
     *
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     *
     * @param mixed $id ID or VAT Number
     * @return Contractor
     */
    public function getContractor($id, \DateTime $date = null)
    {
        if ($this->isVatNo($id)) {
            $contractor = $this->em->getRepository(
                'Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent'
            )->findOneBy(array('vatNoCanonical' => $id));
        } else {
            $contractor = $this->em->getRepository(
                'Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent'
            )->find($id);
        }

        return $contractor;
    }

    private function isVatNo($id)
    {
        return preg_match('/^[A-Z]{2}/', $id);
    }

    public function getContractors()
    {
        $contractors = $this->em->getRepository(
            'Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent'
        )->findAll();

        return $contractors;
    }

    public function updateContractor(AbstractContractorEntity $contractor)
    {
        $contractor->getName(); // lazy-load
        $this->em->persist($contractor);

        $version = new ContractorVersion();
        $ou = new ObjectUpdater();
        $ou->fromObject($contractor, $version, array('id', 'currentContractor', 'contratorVersions', 'updatedAt'));
        $version->setContractorCurrent($contractor);
        $this->em->persist($version);
        $this->em->flush($version);
    }

    public function correctContractor(AbstractContractorEntity $contractor)
    {
        $this->em->persist($contractor);
        $this->em->flush($contractor);
    }

    public function getSupplier()
    {
        $seller = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent')->findOneBy(
            array('supplier' => true)
        );

        return $seller;
    }
}
