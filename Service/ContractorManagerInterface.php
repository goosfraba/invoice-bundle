<?php
namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity;

interface ContractorManagerInterface
{

    /**
     *
     * @param mixed $id ID or VAT Number
     * @return AbstractContractorEntity
     */
    public function getContractor($id);

    public function getContractors();

    public function correctContractor(AbstractContractorEntity $contractor);

    public function updateContractor(AbstractContractorEntity $contractor);

    public function getSupplier();
}
