<?php

namespace Webit\Bundle\InvoiceBundle\Service;

use Doctrine\ORM\EntityRepository;

final class DefaultSupplierFirstSupplierProvider implements DefaultSupplierProvider
{
    /** @var EntityRepository */
    private $repository;

    public function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function getDefaultSupplier()
    {
        return $this->repository->findOneBy(['supplier' => true], ['id' => 'ASC']);
    }
}