<?php

namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent;

interface DefaultSupplierProvider
{
    /**
     * @return ContractorCurrent
     */
    public function getDefaultSupplier();
}
