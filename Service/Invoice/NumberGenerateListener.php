<?php
namespace Webit\Bundle\InvoiceBundle\Service\Invoice;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Doctrine\ORM\Event\LifecycleEventArgs;

class NumberGenerateListener implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Invoice) {
            $number = $entity->getNumber();
            if (empty($number)) {
                $number = $this->container->get('webit_invoice.invoice_number_generator')->generateNumber($entity);
                $entity->setNumber($number);
            }
        }
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
