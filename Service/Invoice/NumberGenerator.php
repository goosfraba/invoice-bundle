<?php
namespace Webit\Bundle\InvoiceBundle\Service\Invoice;

use Doctrine\ORM\Query;
use Doctrine\ORM\EntityManager;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

class NumberGenerator
{
    /** @var EntityManager */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Invoice $invoice
     * @return string
     */
    public function generateNumber(Invoice $invoice)
    {
        $qb = $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice')->createQueryBuilder('i');
        $qb->select('COUNT(i.id) + 1 as num')
            ->where($qb->expr()->eq('i.type', ':type'))
            ->andWhere($qb->expr()->gte('i.issueDate', ':from'))
            ->andWhere($qb->expr()->lt('i.issueDate', ':to'));

        $year = $invoice->getIssueDate()->format('Y');
        list($from, $to) = $this->calculateFromAndTo($year);

        $qb->setParameter('type', $invoice->getType());
        $qb->setParameter('from', $from);
        $qb->setParameter('to', $to);

        $num = $qb->getQuery()->execute(array(), Query::HYDRATE_SINGLE_SCALAR);

        $number = sprintf(
            '%s%d/%0' . $invoice->getType()->getNumberLength() . 'd',
            $invoice->getType()->getPrefix(),
            $year,
            $num
        );

        return $number;
    }

    /**
     * @param string $year
     * @return array
     */
    private function calculateFromAndTo($year)
    {
        return [
            \DateTime::createFromFormat('Y-m-d H:i:s', ($year . '-01-01 00:00:00')),
            \DateTime::createFromFormat('Y-m-d H:i:s', (((int)$year + 1) . '-01-01 00:00:00'))
        ];
    }
}
