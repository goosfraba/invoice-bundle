<?php
namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Address;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\BankAccount;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Seller;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\SellerFactory;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\VatNo;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod;
use Webit\Bundle\InvoiceBundle\Entity\Contractor\AbstractContractorEntity;
use Webit\Bundle\InvoiceBundle\Service\InvoiceMapper\InvoiceMapperInterface;

class InvoiceCreator implements InvoiceCreatorInterface
{
    /**
     *
     * @var ContractorManagerInterface
     */
    protected $cm;

    /**
     *
     * @var PaymentMethodManagerInterface
     */
    protected $pmm;

    /**
     *
     * @var array
     */
    private $mappers = array();

    /**
     * @var SellerFactory
     */
    private $sellerFactory;

    public function __construct(
        ContractorManagerInterface $cm,
        PaymentMethodManagerInterface $pmm,
        SellerFactory $sellerFactory
    ) {
        $this->cm = $cm;
        $this->pmm = $pmm;
        $this->sellerFactory = $sellerFactory;
    }

    public function createInvoice(Type $type, Invoice $sourceInvoice = null)
    {
        if ($sourceInvoice == null) {
            $invoice = new Invoice();
            $invoice->setType($type);

            $contractorSeller = $this->getDefaultSeller();

            if ($contractorSeller) {
                $seller = $this->sellerFactory->create($contractorSeller);
                $invoice->setSeller($seller);
            }

            if ($contractorSeller && ($user = $contractorSeller->getDefaultUser())) {
                $invoice->setIssuer($user->getFullname());
            }

            $paymentMethod = $this->getDefaultPaymentMethod();
            if ($paymentMethod) {
                $invoice->setPaymentMethod($paymentMethod);
            }

            return $invoice;
        }

        $mapper = $this->findMapper($sourceInvoice->getType(), $type);
        if (!$mapper) {
            throw new \Exception(
                sprintf(
                    'Mapper "%s" to "%s" could not be found.',
                    $sourceInvoice->getType()->getCode(),
                    $type->getCode()
                )
            );
        }

        $invoice = $mapper->mapInvoice($sourceInvoice, $type);


        return $invoice;
    }

    /**
     *
     * @param Type $sourceType
     * @param Type $destType
     * @return InvoiceMapperInterface
     */
    private function findMapper(Type $sourceType, Type $destType)
    {
        $arKeys = array();
        $arKeys[] = $sourceType->getCode() . '-' . $destType->getCode();
        $arKeys[] = $sourceType->getCode() . '-' . $destType->getMasterType();
        $arKeys[] = $sourceType->getMasterType() . '-' . $destType->getCode();
        $arKeys[] = $sourceType->getMasterType() . '-' . $destType->getMasterType();
        foreach ($arKeys as $key) {
            if ($this->hasMapper($key)) {
                return $this->mappers[$key];
            }
        }

        return null;
    }


    /**
     * @return AbstractContractorEntity
     */
    private function getDefaultSeller()
    {
        return $this->cm->getSupplier();
    }

    /**
     *
     * @return PaymentMethod
     */
    private function getDefaultPaymentMethod()
    {
        $paymentMethod = $this->pmm->getPaymentMethod('cash');

        return $paymentMethod;
    }

    private function hasMapper($key)
    {
        return key_exists($key, $this->mappers);
    }

    public function registerMapper($sourceType, $destType, InvoiceMapperInterface $mapper)
    {
        $this->mappers[$sourceType . '-' . $destType] = $mapper;
    }
}
