<?php
namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

interface InvoiceCreatorInterface
{
    public function createInvoice(Type $type, Invoice $sourceInvoice = null);
}