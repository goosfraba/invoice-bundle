<?php
namespace Webit\Bundle\InvoiceBundle\Service;

use Doctrine\ORM\EntityManager;
use Webit\Bundle\ExtJsBundle\Store\ORM\QueryBuilderDecorator;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Invoice\InvoiceRepository;

class InvoiceManager implements InvoiceManagerInterface
{
    /**
     *
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     *
     * @param int $id
     * @return Invoice
     */
    public function getInvoice($id)
    {
        $invoice = $this->em->getRepository('')->find($id);

        return $invoice;
    }

    /**
     * @return array
     */
    public function getInvoices(FilterCollectionInterface $filters, SorterCollectionInterface $sorters)
    {
        $qb = $this->getRepository()->createQueryBuilder('i');
        $qb->select('i, c, t, s');
        $qb->leftJoin('i.client', 'c');
        $qb->leftJoin('i.type', 't');
        $qb->leftJoin('i.seller', 's');
        $qbDec = new QueryBuilderDecorator($qb);
        $qbDec->applyFilters($filters);
        $qbDec->applySorters($sorters);

        $arInvoices = $qbDec->getQuery()->execute();

        return $arInvoices;
    }

    /**
     * @param Invoice $invoice
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateInvoice(Invoice $invoice)
    {
        if ($this->em->contains($invoice) == false) {
            $this->em->persist($invoice);
        }

        $this->em->flush();
    }

    /**
     *
     * @return \Doctrine\ORM\EntityRepository|InvoiceRepository
     */
    private function getRepository()
    {
        return $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice');
    }
}