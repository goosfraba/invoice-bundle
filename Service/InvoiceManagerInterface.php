<?php
namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;

interface InvoiceManagerInterface
{
    /**
     *
     * @param int $id
     * @return Invoice
     */
    public function getInvoice($id);

    /**
     * @return array
     */
    public function getInvoices(FilterCollectionInterface $filters, SorterCollectionInterface $sorters);

    /**
     *
     * @param Invoice $invoice
     */
    public function updateInvoice(Invoice $invoice);
}