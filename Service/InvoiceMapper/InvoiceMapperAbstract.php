<?php
namespace Webit\Bundle\InvoiceBundle\Service\InvoiceMapper;

use Webit\Bundle\InvoiceBundle\Entity\Contractor\ContractorCurrent;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\SellerFactory;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Service\ContractorManagerInterface;
use Webit\Bundle\InvoiceBundle\Service\PaymentMethodManagerInterface;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\JoinedInvoice;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem;

abstract class InvoiceMapperAbstract implements InvoiceMapperInterface
{
    /**
     *
     * @var ContractorManagerInterface
     */
    protected $cm;

    /**
     *
     * @var PaymentMethodManagerInterface
     */
    protected $pmm;

    /**
     * @var SellerFactory
     */
    private $sellerFactory;

    public function __construct(
        ContractorManagerInterface $cm,
        PaymentMethodManagerInterface $pmm,
        SellerFactory $sellerFactory
    ) {
        $this->cm = $cm;
        $this->pmm = $pmm;
        $this->sellerFactory = $sellerFactory;
    }

    public function mapInvoice(Invoice $sourceInvoice, Type $type)
    {
        $invoice = new Invoice();
        $invoice->setType($type);
        $this->joinInvoices($sourceInvoice, $invoice);
        $this->copyParticipants($sourceInvoice, $invoice);
        $this->copyPaymentMethod($sourceInvoice, $invoice);

        $this->mapDates($sourceInvoice, $invoice);
        $this->mapItems($sourceInvoice, $invoice);

        return $invoice;
    }

    abstract protected function mapDates(Invoice $sourceInvoice, Invoice $invoice);

    abstract protected function mapItems(Invoice $sourceInvoice, Invoice $invoice);

    protected function joinInvoices(Invoice $sourceInvoice, Invoice $invoice)
    {
        $joined = new JoinedInvoice();
        $joined->setMasterInvoice($invoice);
        $joined->setDestInvoice($sourceInvoice);
        $joined->setType($sourceInvoice->getType()->getMasterType());
        $invoice->getJoinedInvoices()->add($joined);

        $joined = new JoinedInvoice();
        $joined->setMasterInvoice($sourceInvoice);
        $joined->setDestInvoice($invoice);
        $joined->setType($invoice->getType()->getMasterType());
        $sourceInvoice->getJoinedInvoices()->add($joined);
    }

    protected function copyParticipants(Invoice $sourceInvoice, Invoice $invoice)
    {
        if ($client = $sourceInvoice->getClient()) {
            $invoice->setClient($client);
        }

        if ($receiver = $sourceInvoice->getReceiver()) {
            $invoice->setReceiver($receiver);
        }

        $seller = $sourceInvoice->getSeller();
        $issuer = $sourceInvoice->getIssuer();

        $sellerContractor = null;

        if (! $seller) {
            $sellerContractor = $this->getDefaultSeller();
            $seller = $this->sellerFactory->create($sellerContractor);

            $user = $sellerContractor->getDefaultUser();
            $issuer = $user ? $user->getFullname() : null;
        }

        $invoice->setSeller($seller);
        $invoice->setIssuer($issuer);
    }

    protected function copyPaymentMethod(Invoice $sourceInvoice, Invoice $invoice)
    {
        if ($paymentMethod = $sourceInvoice->getPaymentMethod()) {
            $invoice->setPaymentMethod($paymentMethod);
        }
    }

    protected function cloneItem(InvoiceItem $sourceItem, Invoice $invoice)
    {
        $item = new InvoiceItem();
        $item->setInvoice($invoice);
        $item->setQuantity($sourceItem->getQuantity());
        $item->setUnit($sourceItem->getUnit());
        $item->setName($sourceItem->getName());
        $item->setDescription($sourceItem->setDescription());

        $sourceItem->getDiscountPrice()->getCurrency();
        $sourceItem->getDiscountPrice()->getVatRate();
        $discountPrice = clone($sourceItem->getDiscountPrice());
        $item->setDiscountPrice($discountPrice);

        $sourceItem->getPrice()->getCurrency();
        $sourceItem->getPrice()->getVatRate();
        $price = clone($sourceItem->getPrice());
        $item->setPrice($price);

        $item->setSourceItemProvider($sourceItem->getSourceItemProvider());
        $item->setSourceItemId($sourceItem->getSourceItemId());

        return $item;
    }

    /**
     * @return ContractorCurrent
     */
    protected function getDefaultSeller()
    {
        return $this->cm->getSupplier();
    }

    /**
     *
     * @return PaymentMethod
     */
    protected function getDefaultPaymentMethod()
    {
        $paymentMethod = $this->pmm->getPaymentMethod('cash');

        return $paymentMethod;
    }
}