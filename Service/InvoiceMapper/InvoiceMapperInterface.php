<?php
namespace Webit\Bundle\InvoiceBundle\Service\InvoiceMapper;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

interface InvoiceMapperInterface
{
    public function mapInvoice(Invoice $sourceInvoice, Type $type);
}