<?php
namespace Webit\Bundle\InvoiceBundle\Service\InvoiceMapper;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

class ProformaToVatMapper extends InvoiceMapperAbstract
{

    protected function mapDates(Invoice $sourceInvoice, Invoice $invoice)
    {
        $invoice->setSaleDate(new \DateTime());
        $invoice->setDueDate($sourceInvoice->getDueDate());
    }

    protected function mapItems(Invoice $sourceInvoice, Invoice $invoice)
    {
        foreach ($sourceInvoice->getItems() as $sourceItem) {
            $item = $this->cloneItem($sourceItem, $invoice);
            $invoice->getItems()->add($item);
        }
    }
}