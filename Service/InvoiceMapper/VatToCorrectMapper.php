<?php
namespace Webit\Bundle\InvoiceBundle\Service\InvoiceMapper;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

class VatToCorrectMapper extends InvoiceMapperAbstract
{

    protected function mapDates(Invoice $sourceInvoice, Invoice $invoice)
    {
        $invoice->setSaleDate(new \DateTime());
        $invoice->setDueDate($sourceInvoice->getDueDate());
    }

    protected function mapItems(Invoice $sourceInvoice, Invoice $invoice)
    {
        foreach ($invoice->getItems() as $sourceItem) {
            $item = $this->cloneItem($sourceItem, $invoice);
            $invoice->getItems()->add($item);
            // TODO: ustawianie poprzedniego itemu
        }
    }
}
