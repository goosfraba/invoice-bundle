<?php
namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Symfony\Component\Routing\RouterInterface;

class InvoiceUrlProvider implements InvoiceUrlProviderInterface
{
    /**
     *
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function getUrl(Invoice $invoice, $mode = 'download', $absolute = false)
    {
        $url = $this->router->generate(
            'webit_invoice_print',
            array('id' => $invoice->getId(), 'mode' => $mode, 'token' => $invoice->getToken()),
            $absolute
        );

        return $url;
    }
}
