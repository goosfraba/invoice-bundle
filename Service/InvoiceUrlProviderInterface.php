<?php
namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;

interface InvoiceUrlProviderInterface
{
    /**
     * @param Invoice $invoice
     * @param string $mode
     * @param bool $absolute
     * @return mixed
     */
    public function getUrl(Invoice $invoice, $mode = 'download', $absolute = false);
}