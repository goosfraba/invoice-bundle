<?php

namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod;
use Doctrine\ORM\EntityManager;

final class PaymentMethodManager implements PaymentMethodManagerInterface
{
    /**
     *
     * @var EntityManager
     */
    private $em;

    /**
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    public function getPaymentMethod($code)
    {
        return $this->getRepository()->findOneBy(['code' => $code]);
    }

    /**
     * @inheritDoc
     */
    public function getPaymentMethods()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @inheritDoc
     */
    public function updatePaymentMethod(PaymentMethod $method)
    {
        if ($this->em->contains($method) == false) {
            $this->em->persist($method);
        }

        $this->em->flush($method);
    }

    /**
     * @inheritDoc
     */
    public function removePaymentMethod(PaymentMethod $method)
    {
        if ($this->em->contains($method)) {
            $this->em->remove($method);
            $this->em->flush($method);
        }
    }

    /**
     * @return EntityRepository
     */
    private function getRepository()
    {
        return $this->em->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod');
    }
}
