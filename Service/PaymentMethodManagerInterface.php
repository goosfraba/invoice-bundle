<?php
namespace Webit\Bundle\InvoiceBundle\Service;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod;

interface PaymentMethodManagerInterface
{
    /**
     * @param string $code
     * @return PaymentMethod
     */
    public function getPaymentMethod($code);

    /**
     * @return PaymentMethod[]
     */
    public function getPaymentMethods();

    /**
     * @param PaymentMethod $method
     */
    public function updatePaymentMethod(PaymentMethod $method);

    /**
     * @param PaymentMethod $method
     */
    public function removePaymentMethod(PaymentMethod $method);
}