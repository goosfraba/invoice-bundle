<?php
/**
 * File VatNoTest.php
 * Created at: 2016-09-03 15-54
 *
 * @author Daniel Bojdo <daniel.bojdo@web-it.eu>
 */

namespace Webit\Bundle\InvoiceBundle\Tests\Entity\Invoice\Contractor;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\VatNo;

class VatNoTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider numbers
     */
    public function shouldParseTheNumber($number, $countryCode, $canonical)
    {
        $vatNo = new VatNo($number, $countryCode);

        $this->assertEquals($countryCode, $vatNo->countryCode());
        $this->assertEquals($canonical, $vatNo->canonical());
    }

    /**
     * @return array
     */
    public function numbers()
    {
        return array(
            array('972-12 -32- 135  ', null, '9721232135'),
            array('9721232135', 'PL', 'PL9721232135'),
            array('972-123-21-35', null, '9721232135'),
            array('857245739B01', 'NL', 'NL857245739B01'),
            array('857245739b01', 'NL', 'NL857245739B01')
        );
    }
}
