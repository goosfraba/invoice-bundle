<?php

namespace Webit\Bundle\InvoiceBundle\Tests\Functional;

use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;
use Webit\Accounting\CommonBundle\Entity\Vat\VatRate;
use Webit\Bundle\InvoiceBundle\DataFixtures\ORM\LoadCurrency;
use Webit\Bundle\InvoiceBundle\DataFixtures\ORM\LoadPaymentMethod;
use Webit\Bundle\InvoiceBundle\DataFixtures\ORM\LoadType;
use Webit\Bundle\InvoiceBundle\DataFixtures\ORM\LoadVatRates;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Address;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Customer;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceDataPrice;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoicePrice;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\VatNo;
use Webit\Bundle\InvoiceBundle\Tests\Functional\Bootstrap\AppKernel;
use Webit\Common\CurrencyBundle\Entity\Currency;
use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryProviderInterface;
use Webit\Common\UnitBundle\Entity\Unit;
use Webit\Tests\Doctrine\SchemaEnsureTrait;

abstract class AbstractFunctionalTest extends TestCase
{
    use SchemaEnsureTrait;

    /** @var Generator */
    private $faker;

    /**
     * @return Generator
     */
    protected function faker()
    {
        if (!$this->faker) {
            $this->faker = Factory::create('pl_PL');
        }

        return $this->faker;
    }

    protected function createKernel($hash = null)
    {
        return new AppKernel($hash);
    }

    protected function setUp()
    {
        $this->prepareSchema('default');
        $this->loadFixtures();
    }

    /**
     * @param string|null $rate
     * @return VatRate|object
     */
    protected function vatRate($rate = null)
    {
        /** @var DictionaryProviderInterface $dp */
        $dp = $this->service('webit_common_dictionary.dictionary_provider');

        $dictionary = $dp->getDictionary('vat');

        return $dictionary->getItem($rate ?: 'A');
    }

    /**
     * @return Currency|object
     */
    protected function currency()
    {
        /** @var DictionaryProviderInterface $dp */
        $dp = $this->service('webit_common_dictionary.dictionary_provider');

        $dictionary = $dp->getDictionary('currency');

        return $dictionary->getItem('PLN');
    }

    /**
     * @return Unit|object
     */
    protected function unit()
    {
        /** @var DictionaryProviderInterface $dp */
        $dp = $this->service('webit_common_dictionary.dictionary_provider');

        $dictionary = $dp->getDictionary('unit');

        return $dictionary->getItem('quantity:szt.');
    }

    /**
     * @return Type|object
     */
    protected function invoiceType($code = null)
    {
        $repo = $this->defaultEntityManager()->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\Type');
        $code = $code ?: 'vat-ec';

        return $repo->findOneBy(array('code' => $code));
    }

    /**
     * @return PaymentMethod|object
     */
    protected function paymentMethod($code = null)
    {
        $repo = $this->defaultEntityManager()->getRepository('Webit\Bundle\InvoiceBundle\Entity\Invoice\PaymentMethod');
        $code = $code ?: 'on-line';

        return $repo->findOneBy(array('code' => $code));
    }

    private function loadFixtures()
    {
        /** @var DictionaryProviderInterface $dp */
        $dp = $this->service('webit_common_dictionary.dictionary_provider');
        $this->registerCurrency($dp);
        $this->registerUnits($dp);
        $this->registerVatRates();
        $this->registerPaymentMethods();
        $this->registerInvoiceTypes();
    }

    private function registerCurrency(DictionaryProviderInterface $dp)
    {

        $fixtures = new LoadCurrency();
        $fixtures->setContainer($this->container());
        $fixtures->load($this->defaultEntityManager());
    }

    private function registerUnits(DictionaryProviderInterface $dp)
    {
        $dictionary = $dp->getDictionary('unit');

        $unit = new Unit('quantity');
        $unit->setSymbol('szt.');
        $unit->setLabel('Sztuk');
        $dictionary->updateItem($unit);

        $dictionary->commitChanges();
    }

    private function registerPaymentMethods()
    {
        $fixtures = new LoadPaymentMethod();
        $fixtures->load($this->defaultEntityManager());
    }

    private function registerInvoiceTypes()
    {
        $fixtures = new LoadType();
        $fixtures->load($this->defaultEntityManager());
    }

    private function registerVatRates()
    {
        $fixtures = new LoadVatRates();
        $fixtures->setContainer($this->container());
        $fixtures->load($this->defaultEntityManager());
    }

    /**
     * @return EntityManagerInterface
     */
    protected function defaultEntityManager()
    {
        return $this->container()->get('doctrine')->getManager('default');
    }

    protected function flushAndClear()
    {
        $this->defaultEntityManager()->flush();
        $this->defaultEntityManager()->clear();
    }

    protected function tearDown()
    {
        $this->kernel = null;
        $this->kernel()->clear();
    }

    protected function invoice(Type $type = null, \DateTime $issueDate = null)
    {
        $invoice = new Invoice();
        $price = new InvoicePrice();
        $price->setCurrency($this->currency());
        $invoice->setTaxedValue($price);

        $price = new InvoicePrice();
        $price->setCurrency($this->currency());
        $invoice->setValue($price);

        $invoice->setClient(
            new Customer(
                $this->faker()->company,
                new VatNo('123456785'),
                new Address(
                    $this->faker()->streetAddress,
                    $this->faker()->postcode,
                    $this->faker()->city,
                    'Polska'
                )
            )
        );

        $invoice->setIssueDate($issueDate ?: new \DateTime());
        $invoice->setDueDate(date_create('now + 7 days'));
        $invoice->setReceiver($this->faker()->name);
        $invoice->setIssuer($this->faker()->name);
        $invoice->setInternalNote($this->faker()->colorName);
        $invoice->setNote($this->faker()->colorName);
        $invoice->setSaleDate(date_create('now'));

        $invoice->setPaymentMethod($this->paymentMethod());
        $invoice->setType($type ?: $this->invoiceType());

        $itemsNo = mt_rand(1, 4);

        for ($i = 0; $i < $itemsNo; $i++) {
            $invoice->getItems()->add($this->invoiceItem());
        }

        return $invoice;
    }

    /**
     * @return InvoiceItem
     */
    protected function invoiceItem()
    {
        $item = new InvoiceItem();
        $item->setName($this->faker()->colorName);
        $item->setDescription($this->faker()->creditCardNumber);
        $item->setQuantity(mt_rand(1, 5));

        $price = new InvoiceDataPrice();
        $price->setValue(mt_rand(10000, 100000) / 100);
        $price->setCurrency($this->currency());
        $price->setVatRate($this->vatRate());

        $item->setPrice($price);

        return $item;
    }
}
