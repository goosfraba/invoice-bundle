<?php

namespace Webit\Bundle\InvoiceBundle\Tests\Functional\Bootstrap;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use FOS\RestBundle\FOSRestBundle;
use JMS\Payment\CoreBundle\JMSPaymentCoreBundle;
use JMS\SerializerBundle\JMSSerializerBundle;
use Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Webit\Accounting\CommonBundle\WebitAccountingCommonBundle;
use Webit\Bundle\ExtJsBundle\WebitExtJsBundle;
use Webit\Bundle\InvoiceBundle\WebitInvoiceBundle;
use Webit\Bundle\PdfLatexBundle\WebitPdfLatexBundle;
use Webit\Common\CurrencyBundle\WebitCommonCurrencyBundle;
use Webit\Common\DictionaryBundle\WebitCommonDictionaryBundle;
use Webit\Common\UnitBundle\WebitCommonUnitBundle;
use Webit\Tests\Kernel\ConfigurableKernel;

class AppKernel extends ConfigurableKernel
{

    /**
     * @inheritdoc
     */
    public function registerBundles()
    {
        return array(
            new FrameworkBundle(),
            new StofDoctrineExtensionsBundle(),
            new TwigBundle(),
            new JMSSerializerBundle(),
            new JMSPaymentCoreBundle(),
            new FOSRestBundle(),
            new WebitCommonDictionaryBundle(),
            new WebitCommonUnitBundle(),
            new WebitCommonCurrencyBundle(),
            new WebitAccountingCommonBundle(),
            new WebitExtJsBundle(),
            new WebitPdfLatexBundle(),
            new WebitInvoiceBundle(),
            new DoctrineBundle()
        );
    }
}
