<?php

namespace Webit\Bundle\InvoiceBundle\Tests\Functional\Doctrine;

use Doctrine\ORM\EntityRepository;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Address;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\Customer;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Contractor\VatNo;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceDataPrice;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoiceItem;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\InvoicePrice;
use Webit\Bundle\InvoiceBundle\Integration\InvoiceId;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\Bundle\InvoiceBundle\Invoice\InvoiceRepository;
use Webit\Bundle\InvoiceBundle\Tests\Functional\AbstractFunctionalTest;

class InvoiceVendorIdMappingTest extends AbstractFunctionalTest
{
    /**
     * @test
     */
    public function it_persists_vendor_id()
    {
        $invoiceId = $this->newInvoiceId();
        $this->flushAndClear();

        $invoice = $this->invoiceRepository()->invoiceOfId($invoiceId);

        $invoice->setVendorId(
            $expectedVendorId = new InvoiceId(
                $vendor = Vendor::fromString('my-vendor'),
            123
            )
        );
        $this->flushAndClear();

        $invoice = $this->invoiceRepository()->invoiceOfId($invoiceId);
        $vendorId = $invoice->vendorId($vendor);

        $this->assertEquals($expectedVendorId, $vendorId);
    }

    /**
     * @test
     */
    public function it_removes_vendor_id()
    {
        $invoiceId = $this->newInvoiceId();
        $invoice = $this->invoiceRepository()->invoiceOfId($invoiceId);

        $invoice->setVendorId(
            $expectedVendorId = new InvoiceId(
                $vendor = Vendor::fromString('my-vendor'),
                123
            )
        );

        $this->flushAndClear();

        $invoice = $this->invoiceRepository()->invoiceOfId($invoiceId);
        $invoice->unsetVendorId($vendor);
        $this->flushAndClear();

        $invoice = $this->invoiceRepository()->invoiceOfId($invoiceId);
        $this->assertNull($invoice->vendorId($vendor));
    }

    private function newInvoiceId()
    {
        $invoice = $this->invoice();
        $this->service('webit_invoice.invoice_manager')->updateInvoice($invoice);

        return $invoice->getId();
    }

    /**
     * @return InvoiceRepository|EntityRepository|object
     */
    private function invoiceRepository()
    {
        return $this->service('webit_invoice.invoice_repository');
    }
}
