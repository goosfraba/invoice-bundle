<?php

namespace Webit\Bundle\InvoiceBundle\Tests\Functional\Doctrine;

use Doctrine\ORM\EntityRepository;
use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Integration\TypeId;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\Bundle\InvoiceBundle\Invoice\TypeRepository;
use Webit\Bundle\InvoiceBundle\Tests\Functional\AbstractFunctionalTest;

class TypeVendorIdMappingTest extends AbstractFunctionalTest
{
    /**
     * @test
     */
    public function it_persists_vendor_id()
    {
        $typeId = $this->newTypeId();
        $this->flushAndClear();

        $type = $this->typeRepository()->typeOfId($typeId);

        $type->setVendorId(
            $expectedVendorId = new TypeId(
                $vendor = Vendor::fromString('my-vendor'),
                123
            )
        );
        $this->flushAndClear();

        $type = $this->typeRepository()->typeOfId($typeId);
        $vendorId = $type->vendorId($vendor);

        $this->assertEquals($expectedVendorId, $vendorId);
    }

    /**
     * @test
     */
    public function it_removes_vendor_id()
    {
        $typeId = $this->newTypeId();
        $type = $this->typeRepository()->typeOfId($typeId);

        $type->setVendorId(
            $expectedVendorId = new TypeId(
                $vendor = Vendor::fromString('my-vendor'),
                123
            )
        );

        $this->flushAndClear();

        $type = $this->typeRepository()->typeOfId($typeId);
        $type->unsetVendorId($vendor);
        $this->flushAndClear();

        $type = $this->typeRepository()->typeOfId($typeId);
        $this->assertNull($type->vendorId($vendor));
    }

    private function type()
    {
        $type = new Type();
        $type->setCode($this->faker()->randomAscii);
        $type->setSymbol($this->faker()->randomAscii);
        $type->setLabel('Label ' . $this->faker()->randomAscii);
        $type->setPrefix('X/'.$this->faker()->randomAscii);
        $type->setNumberLength(mt_rand(0, 5));

        return $type;
    }

    private function newTypeId()
    {
        $type = $this->type();
        $em = $this->defaultEntityManager();
        $em->persist($type);
        $em->flush();

        return $type->getId();
    }

    /**
     * @return TypeRepository|EntityRepository|object
     */
    private function typeRepository()
    {
        return $this->service('webit_invoice.type_repository');
    }
}
