<?php

namespace Webit\Bundle\InvoiceBundle\Tests\Functional\Integration\WFirma;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Webit\Bundle\InvoiceBundle\Tests\Functional\AbstractFunctionalTest;

class BundleConfigurationTest extends AbstractFunctionalTest
{
    protected function setUp()
    {

    }

    /**
     * @test
     */
    public function it_loads_wfirma_services()
    {
        $kernel = $this->kernel(false);
        $kernel->appendConfig(
            <<<YML
webit_invoice:
    wfirma:
        username: test
        password: test
YML
        );

        $this->assertInstanceOf(
            'Webit\Bundle\InvoiceBundle\Integration\WFirma\Series\TypeLinker',
            $this->service('webit_invoice.wfirma.type_linker')
        );

        $this->assertInstanceOf(
            'Webit\Bundle\InvoiceBundle\Integration\WFirma\InvoicePusherAdapter',
            $this->service('webit_invoice.wfirma.invoice_pusher_adapter')
        );

        $app = new Application($this->kernel());
        $this->assertInstanceOf(
            'Webit\Bundle\InvoiceBundle\Integration\WFirma\Command\LinkTypeCommand',
            $app->get('webit-invoice:wfirma:link-type')
        );
    }
}