<?php

namespace Webit\Bundle\InvoiceBundle\Tests\Functional\Invoice\Infrastructure;

use Webit\Bundle\InvoiceBundle\Entity\Invoice\Type;
use Webit\Bundle\InvoiceBundle\Integration\TypeId;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\Bundle\InvoiceBundle\Invoice\Infrastructure\InvoiceEntityRepository;
use Webit\Bundle\InvoiceBundle\Invoice\Month;
use Webit\Bundle\InvoiceBundle\Tests\Functional\AbstractFunctionalTest;

class InvoiceEntityRepositoryTest extends AbstractFunctionalTest
{
    /** @var InvoiceEntityRepository */
    private $repo;

    protected function setUp()
    {
        parent::setUp();
        $this->repo = $this->service('webit_invoice.invoice_repository');
    }

    /**
     * @test
     */
    public function it_finds_invoices_for_vendor_and_month()
    {
        $currentMonth = Month::create(new \DateTime());
        $previousMonth = $currentMonth->previous();
        $nextMonth = $currentMonth->next();

        $vendor = Vendor::fromString('vendor-1');
        $vendor2 = Vendor::fromString('vendor-2');

        $type1 = $this->newInvoiceType();
        $type2 = $this->newInvoiceType($vendor);
        $type3 = $this->newInvoiceType($vendor2);

        // expected invoices (vendor-1, current month)
        $expectedInvoices = [
            $this->newInvoice($type2, $currentMonth->start())->getId(),
            $this->newInvoice($type2, $currentMonth->start()->add(new \DateInterval('P10D')))->getId(),
            $this->newInvoice($type2, $currentMonth->end())->getId(),
        ];

        // invoices of vendor-1
        $this->newInvoice($type2, $previousMonth->end());
        $this->newInvoice($type2, $nextMonth->start());

        // invoices of unmapped type
        $this->newInvoice($type1, $currentMonth->start());
        $this->newInvoice($type1, $currentMonth->end());
        $this->newInvoice($type1, $previousMonth->end());
        $this->newInvoice($type1, $nextMonth->start());

        // invoices of vendor=2
        $this->newInvoice($type3, $currentMonth->start());
        $this->newInvoice($type3, $currentMonth->end());
        $this->newInvoice($type3, $previousMonth->end());
        $this->newInvoice($type3, $nextMonth->start());

        $invoices = $this->repo->invoicesForVendorAndMonth($vendor, $currentMonth);
        $this->assertEquals(count($expectedInvoices), count($invoices));
        foreach ($invoices as $invoice) {
            $this->assertContains($invoice->getId(), $expectedInvoices);
        }
    }

    private function newInvoiceType(Vendor $vendor = null)
    {
        $type = new Type();

        $type->setSymbol($this->faker()->randomAscii);
        $type->setCode($type->getSymbol());
        $type->setLabel('Type ' . $type->getSymbol());
        $type->setNumberLength(5);
        $type->setPrefix($type->getSymbol().'/');

        if ($vendor) {
            $type->setVendorId(new TypeId($vendor, mt_rand(1, 1000)));
        }

        $this->defaultEntityManager()->persist($type);
        $this->defaultEntityManager()->flush();

        return $type;
    }

    /**
     * @param Type $type
     * @param \DateTime $issueDate
     * @return \Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice
     */
    private function newInvoice(Type $type, \DateTime $issueDate)
    {
        $invoice = $this->invoice($type, $issueDate);

        $this->defaultEntityManager()->persist($invoice);
        $this->defaultEntityManager()->flush();

        return $invoice;
    }
}
