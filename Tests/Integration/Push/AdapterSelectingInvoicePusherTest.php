<?php

namespace Webit\Bundle\InvoiceBundle\Tests\Integration\Push;

use PHPUnit\Framework\TestCase;
use Webit\Bundle\InvoiceBundle\Integration\InvoiceId;
use Webit\Bundle\InvoiceBundle\Integration\Push\AdapterSelectingInvoicePusher;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;

class AdapterSelectingInvoicePusherTest extends TestCase
{
    /**
     * @test
     */
    public function it_selects_adapter_by_vendor()
    {
        $p1 = $this->prophesize('Webit\Bundle\InvoiceBundle\Integration\Push\InvoicePusherAdapter');
        $p2 = $this->prophesize('Webit\Bundle\InvoiceBundle\Integration\Push\InvoicePusherAdapter');

        $pusher = new AdapterSelectingInvoicePusher(
            array(
                'vendor-1' => $p1->reveal(),
                'vendor-2' => $p2->reveal()
            )
        );

        $invoice = $this->prophesize('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice')->reveal();
        $vendor = Vendor::fromString('vendor-2');

        $p1->push($invoice)->shouldNotBeCalled();
        $p2->push($invoice)->willReturn($id = new InvoiceId($vendor, '123'));

        $this->assertSame($id, $pusher->push($invoice, Vendor::fromString('vendor-2')));
    }

    /**
     * @test
     * @expectedException \Webit\Bundle\InvoiceBundle\Integration\Exception\UnsupportedVendorException
     */
    public function it_throws_exception_when_no_matching_adapter()
    {
        $p1 = $this->prophesize('Webit\Bundle\InvoiceBundle\Integration\Push\InvoicePusherAdapter');

        $pusher = new AdapterSelectingInvoicePusher(
            array(
                'vendor-1' => $p1->reveal(),
            )
        );

        $invoice = $this->prophesize('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice')->reveal();
        $vendor = Vendor::fromString('vendor-2');

        $pusher->push($invoice, $vendor);
    }
}
