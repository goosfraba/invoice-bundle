<?php

namespace Webit\Bundle\InvoiceBundle\Tests\Integration\Push;

use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Webit\Bundle\InvoiceBundle\Integration\InvoiceId;
use Webit\Bundle\InvoiceBundle\Integration\Push\InvoicePusher;
use Webit\Bundle\InvoiceBundle\Integration\Push\PersistingInvoicePusher;
use Webit\Bundle\InvoiceBundle\Integration\Vendor;
use Webit\Bundle\InvoiceBundle\Service\InvoiceManagerInterface;

class PersistingInvoicePusherTest extends TestCase
{
    /** @var InvoicePusher|ObjectProphecy */
    private $innerPusher;

    /** @var InvoiceManagerInterface|ObjectProphecy */
    private $invoiceManager;

    /** @var PersistingInvoicePusher */
    private $pusher;

    protected function setUp()
    {
        $this->innerPusher = $this->prophesize('Webit\Bundle\InvoiceBundle\Integration\Push\InvoicePusher');
        $this->invoiceManager = $this->prophesize('Webit\Bundle\InvoiceBundle\Service\InvoiceManagerInterface');
        $this->pusher = new PersistingInvoicePusher($this->innerPusher->reveal(), $this->invoiceManager->reveal());
    }

    /**
     * @test
     */
    public function it_persists_vendor_id_of_pushed_invoice()
    {
        $invoice = $this->prophesize('Webit\Bundle\InvoiceBundle\Entity\Invoice\Invoice');
        $vendor = Vendor::fromString('vendor-1');

        $vendorId = new InvoiceId($vendor, '123');
        $this->innerPusher->push($invoice, $vendor)->willReturn($vendorId);
        $invoice->setVendorId($vendorId)->shouldBeCalled();
        $this->invoiceManager->updateInvoice($invoice)->shouldBeCalled();

        $this->assertSame($vendorId, $this->pusher->push($invoice->reveal(), $vendor));
    }
}
