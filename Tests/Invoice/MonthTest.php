<?php

namespace Webit\Bundle\InvoiceBundle\Tests\Invoice;

use PHPUnit\Framework\TestCase;
use Webit\Bundle\InvoiceBundle\Invoice\Month;

class MonthTest extends TestCase
{
    /**
     * @test
     * @dataProvider months
     * @param \DateTime $anchorDate
     * @param \DateTime $expectedFrom
     * @param \DateTime $expectedTo
     * @param \DateTime $expectedToExclusive
     * @throws \Exception
     */
    public function it_is_created_from_date_time(
        \DateTime $anchorDate,
        \DateTime $expectedFrom,
        \DateTime $expectedTo,
        \DateTime $expectedToExclusive
    ) {
        $month = Month::create($anchorDate);

        $this->assertEquals($expectedFrom, $month->start());
        $this->assertEquals($expectedTo, $month->end());
        $this->assertEquals($expectedToExclusive, $month->end(true));
    }

    public function months()
    {
        return [
            [
                date_create('2018-01-05 12:43:21'),
                date_create('2018-01-01 00:00:00'),
                date_create('2018-01-31 23:59:59'),
                date_create('2018-02-01 00:00:00'),
            ],
            [
                date_create('2018-02-28 12:43:21'),
                date_create('2018-02-01 00:00:00'),
                date_create('2018-02-28 23:59:59'),
                date_create('2018-03-01 00:00:00'),
            ]
        ];
    }

    /**
     * @test
     */
    public function it_creates_next_month()
    {
        $current = date_create('2018-01-05 12:43:21');
        $month = Month::create($current);
        $nextMonth = $month->next();

        $this->assertEquals(
            date_create('2018-02-01 00:00:00'),
            $nextMonth->start()
        );

        $this->assertEquals(
            date_create('2018-02-28 23:59:59'),
            $nextMonth->end()
        );
    }

    /**
     * @test
     */
    public function it_creates_previous_month()
    {
        $current = date_create('2018-01-05 12:43:21');
        $month = Month::create($current);
        $previousMonth = $month->previous();

        $this->assertEquals(
            date_create('2017-12-01 00:00:00'),
            $previousMonth->start()
        );

        $this->assertEquals(
            date_create('2017-12-31 23:59:59'),
            $previousMonth->end()
        );
    }
}
