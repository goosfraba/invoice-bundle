<?php

namespace Webit\Bundle\InvoiceBundle\Twig;

use JMS\Payment\CoreBundle\Model\FinancialTransactionInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Webit\Bundle\InvoiceBundle\Util\PaymentDateHelper;
use Webit\Bundle\InvoiceBundle\Util\Util;

class PaymentSystemNameExtension extends AbstractExtension
{

    public function getFilters()
    {
        return array(
            'payment_system_name' => new TwigFilter('payment_system_name', [$this, 'getPaymentSystemName']),
            'trim_country_code' => new TwigFilter('trim_country_code', [$this, 'trimCountryCode'])
        );
    }

    public function getFunctions()
    {
        return array(
            'get_payment_date' => new TwigFunction('get_payment_date', [$this, 'getPaymentDate'])
        );
    }

    public function getName()
    {
        return 'webit_invoice_extension';
    }

    public function getPaymentSystemName($value)
    {
        switch ($value) {
            case 'cashbill_direct':
                return 'On-line';
                break;
            case 'simple_bank_transfer':
                return 'Przelew';
                break;
            case 'simple_cash':
                return 'Gotówka';
                break;
            case 'simple_cod':
                return 'Pobranie';
                break;

        }

        return $value;
    }

    public function trimCountryCode($vatNo)
    {
        $countryCode = Util::getCoutryCode($vatNo);
        if ($countryCode) {
            return substr($vatNo, 2);
        }

        return $vatNo;
    }

    /**
     *
     * @param FinancialTransactionInterface $transaction
     * @return \DateTime
     */
    public function getPaymentDate(FinancialTransactionInterface $transaction)
    {
        return PaymentDateHelper::getPaymentDate($transaction);
    }
}
