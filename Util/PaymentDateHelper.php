<?php
namespace Webit\Bundle\InvoiceBundle\Util;

use JMS\Payment\CoreBundle\Entity\FinancialTransaction;
use JMS\Payment\CoreBundle\Entity\Payment;

class PaymentDateHelper
{
    /**
     * @var array
     */
    private static $createdAtPaymentSystems = array(
        'simple_bank_transfer',
        'simple_cash',
        'simple_cod'
    );

    /**
     *
     * @param FinancialTransaction $transaction
     * @return \DateTime
     */
    public static function getPaymentDate(FinancialTransaction $transaction)
    {
        /** @var Payment $payment */
        $payment = $transaction->getPayment();

        $pi = $payment->getPaymentInstruction();
        if (in_array($pi->getPaymentSystemName(), self::$createdAtPaymentSystems)) {
            return $transaction->getCreatedAt();
        }

        return $transaction->getUpdatedAt();
    }
}
