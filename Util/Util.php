<?php
namespace Webit\Bundle\InvoiceBundle\Util;

class Util
{

    /**
     *
     * @param string $vatNo
     * @param string $countryCode
     * @return string
     */
    public static function vatNoCanonize($vatNo, $countryCode = null)
    {
        $vatNo = trim($vatNo);
        $vatNo = preg_replace('/[^A-Z0-9]/', '', $vatNo);

        $vatNo = self::prependCountryCode($vatNo, $countryCode);

        return $vatNo;
    }

    /**
     *
     * @param string $vatNo
     * @param string $countryCode
     * @return string
     */
    public static function prependCountryCode($vatNo, $countryCode)
    {
        if (empty($vatNo)) {
            return $vatNo;
        }

        if (preg_match('/^[A-Z]{2}/', $vatNo) == false) {
            $vatNo = $countryCode . $vatNo;
        }

        return $vatNo;
    }

    /**
     *
     * @param string $vatNo
     * @return string
     */
    public static function getCoutryCode($vatNo)
    {
        if (preg_match('/^[A-Za-z]{2}/', $vatNo)) {
            $code = substr($vatNo, 0, 2);
            $code = strtoupper($code);

            return $code;
        }

        return null;
    }
}
