<?php

namespace Webit\Bundle\InvoiceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Webit\Bundle\InvoiceBundle\DependencyInjection\Compiler\InvoiceMapperPass;
use Webit\Bundle\InvoiceBundle\DependencyInjection\Compiler\InvoicePusherAdapterPass;

class WebitInvoiceBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new InvoiceMapperPass());
        $container->addCompilerPass(new InvoicePusherAdapterPass());
    }
}
